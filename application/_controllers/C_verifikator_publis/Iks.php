<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Iks extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_verifikator_publis/Model_iks', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Keynote Speaker',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vks' => 'active',
            'v' => $this->ma->getIks(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/iks/v_iks', $data);
        $this->load->view('layouts/footer');
    }


    public function history()
    {
        $email = $this->uri->segment(3);
        $data = array(
            'title' => 'History Usulan Insentif Keynote Speaker',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_viks' => 'active',
            'v' => $this->ma->getIks(),
            'h' => $this->ma->getHistory($email),
            'akun' => $this->mu->getUser(),
        );

        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/iks/h_iks', $data);
        $this->load->view('layouts/footer');
    }

    public function tolak($id)
    {

        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Penolakan Usulan Insentif Keynote Speaker ',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vks' => 'active',
            'v' => $this->ma->getIks(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/iks/t_iks', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $sts = 6;
        $date_validator = date('Y-m-d H:i:s');
        $insentif = 0;
        $insentif_disetujui = 0;
        $this->db->set('insentif', $insentif);
        $this->db->set('insentif_disetujui', $insentif_disetujui);
        $this->db->set('date_validator', $date_validator);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_ks');

        $log = [
            'log' => "Menolak Usulan Insentif Keynote Speaker Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vks');
    }

    public function setujui($id)
    {

        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Persetujuan Usulan Insentif Keynote Speaker',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vks' => 'active',
            'v' => $this->ma->getIks(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/iks/s_iks', $data);
        $this->load->view('layouts/footer');
    }


    public function setujuiGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $insentif = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('insentif', true)));
        $sts = 2;
        $date_validator = date('Y-m-d H:i:s');

        $this->db->set('date_validator', $date_validator);
        $this->db->set('insentif', $insentif);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_ks');

        $log = [
            'log' => "Mengusul Usulan Insentif Keynote Speaker Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('viks');
    }

    public function detailIks($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Insentif Keynote speaker ',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vks' => 'active',
            'v' => $this->ma->getIks(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/iks/d_iks', $data);
        $this->load->view('layouts/footer');
    }
}
