<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AJi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_verifikator_publis/Model_aji', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vaji' => 'active',
            'aji' => $this->ma->getAji(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/aji/v_aji', $data);
        $this->load->view('layouts/footer');
    }


    public function detailAji($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Insentif Artikel Dijurnal',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vaji' => 'active',
            'aji' => $this->ma->getAji(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/aji/d_aji', $data);
        $this->load->view('layouts/footer');
    }

    public function history()
    {
        $email = $this->uri->segment(3);
        $data = array(
            'title' => 'History Usulan Insentif Artikel Dijurnal',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vaji' => 'active',
            'aji' => $this->ma->getAji(),
            'h' => $this->ma->getHistory($email),
            'akun' => $this->mu->getUser()
        );

        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/aji/h_aji', $data);
        $this->load->view('layouts/footer');
    }


    public function setujui()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Persetujuan Usulan Insentif Publikasi Jurnal',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vaji' => 'active',
            'aji' => $this->ma->getAji(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/aji/s_aji', $data);
        $this->load->view('layouts/footer');
    }

    public function setujuiGo()
    {

        $id = $this->input->post('id', true);
        $pagu = $this->input->post('pagu', true);
        $ket = $this->input->post('ket', true);
        $insentif = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('insentif', true)));
        $pagu = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('pagu', true)));
        $sts = 2;
        $date_validator = date('Y-m-d H:i:s');

        if ($insentif > $pagu) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Insentif Anda Melebihi Biaya Publikasi yang sudah di tentukan, Silahkan Baca Panduan Terbaru!!</div>');
            redirect('vaji');
        }

        $this->db->set('date_validator', $date_validator);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('insentif', $insentif);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_aji');

        $log = [
            'log' => "Mengusul Insentif Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vaji');
    }

    public function tolak()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Penolakan Usulan Insentif Publikasi Jurnal',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vaji' => 'active',
            'aji' => $this->ma->getAji(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/aji/t_aji', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $sts = 6;
        $date_validator = date('Y-m-d H:i:s');
        $insentif = 0;
        $insentif_disetujui = 0;
        $this->db->set('insentif', $insentif);
        $this->db->set('insentif_disetujui', $insentif_disetujui);
        $this->db->set('date_validator', $date_validator);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_aji');

        $log = [
            'log' => "Menolak Insentif Jurnal Ilmiah Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vaji');
    }
}
