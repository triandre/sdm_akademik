<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ctendik extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('ModelDashboard', 'mds');
        $this->load->model('m_cuti/Model_cuti', 'mc');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_pcuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/tendik/v_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function createCuti()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_pcuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/tendik/c_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function createUmum()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_pcuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );

        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('cutiDiambil', 'Lamanya Cuti', 'required|trim');
        $this->form_validation->set_rules('tglCuti', 'Tanggal Mulai Cuti', 'required|trim');
        $this->form_validation->set_rules('tglSelesaiCuti', 'Tanggal Selesai Cuti', 'required|trim');
        $this->form_validation->set_rules('tglMasuk', 'Tanggal Masuk', 'required|trim');
        $this->form_validation->set_rules('alasan', 'Alasan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('cuti/tendik/c_umum', $data);
            $this->load->view('layouts/footer');
        } else {

            $cutiDiambil = $this->input->post('cutiDiambil');
            $stokCuti = $this->input->post('stokCuti');
            $sisaCuti = ($stokCuti - $cutiDiambil);

            if ($sisaCuti < 0) {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('tendik/cuti/umum');
            } else {
                $email = $this->input->post('email');
                $date = date("Y-m-d H:i:s");

                // cek jika ada gambar yang akan diupload
                $upload_image = $_FILES['scan']['name'];

                if ($upload_image) {
                    $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                    $config['max_size']      = '10048';
                    $config['upload_path'] = './assets/scan/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('scan')) {
                        $old_image = $data['occ_permohonan_cuti']['scan'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'assets/scan/' . $old_image);
                        }
                        $new_image = $this->upload->data('file_name');
                    } else {
                        echo $this->upload->display_errors();
                    }
                }


                $data = [
                    'email' => $email,
                    'jenisCuti' => "Cuti Umum",
                    'cutiDiambil' => $this->input->post('cutiDiambil'),
                    'alasan' => $this->input->post('alasan'),
                    'sts' => 1,
                    'tglCuti' => $this->input->post('tglCuti'),
                    'tglSelesaiCuti' => $this->input->post('tglSelesaiCuti'),
                    'tglMasuk' => $this->input->post('tglMasuk'),
                    'tglPengajuan' => $date,
                    'created_at' => $date,
                    'scan' => $new_image

                ];


                $this->db->insert('occ_permohonan_cuti', $data);

                $this->db->set('cutiUmum', $sisaCuti);
                $this->db->where('email', $email);
                $this->db->update('mm_hak_cuti');
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('tendik/cuti');
            }
        }
    }

    public function createUmroh()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_pcuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );

        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('cutiDiambil', 'Lamanya Cuti', 'required|trim');
        $this->form_validation->set_rules('tglCuti', 'Tanggal Mulai Cuti', 'required|trim');
        $this->form_validation->set_rules('tglSelesaiCuti', 'Tanggal Selesai Cuti', 'required|trim');
        $this->form_validation->set_rules('tglMasuk', 'Tanggal Masuk', 'required|trim');
        $this->form_validation->set_rules('alasan', 'Alasan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('cuti/tendik/c_umroh', $data);
            $this->load->view('layouts/footer');
        } else {

            $cutiDiambil = $this->input->post('cutiDiambil');
            $stokCuti = $this->input->post('stokCuti');
            $sisaCuti = ($stokCuti - $cutiDiambil);

            if ($sisaCuti < 0) {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('tendik/cuti/umroh');
            } else {
                $email = $this->input->post('email');
                $date = date("Y-m-d H:i:s");

                $upload_image = $_FILES['scan']['name'];

                if ($upload_image) {
                    $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                    $config['max_size']      = '10048';
                    $config['upload_path'] = './assets/scan/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('scan')) {
                        $old_image = $data['occ_permohonan_cuti']['scan'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'assets/scan/' . $old_image);
                        }
                        $new_image = $this->upload->data('file_name');
                    } else {
                        echo $this->upload->display_errors();
                    }
                }


                $data = [
                    'email' => $email,
                    'jenisCuti' => "Cuti Umroh",
                    'cutiDiambil' => $this->input->post('cutiDiambil'),
                    'alasan' => $this->input->post('alasan'),
                    'sts' => 1,
                    'tglCuti' => $this->input->post('tglCuti'),
                    'tglSelesaiCuti' => $this->input->post('tglSelesaiCuti'),
                    'tglMasuk' => $this->input->post('tglMasuk'),
                    'tglPengajuan' => $date,
                    'created_at' => $date,
                    'scan' => $new_image

                ];


                $this->db->insert('occ_permohonan_cuti', $data);

                $this->db->set('cutiUmroh', $sisaCuti);
                $this->db->where('email', $email);
                $this->db->update('mm_hak_cuti');
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('tendik/cuti');
            }
        }
    }

    public function createHamil()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_pcuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );

        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('cutiDiambil', 'Lamanya Cuti', 'required|trim');
        $this->form_validation->set_rules('tglCuti', 'Tanggal Mulai Cuti', 'required|trim');
        $this->form_validation->set_rules('tglSelesaiCuti', 'Tanggal Selesai Cuti', 'required|trim');
        $this->form_validation->set_rules('tglMasuk', 'Tanggal Masuk', 'required|trim');
        $this->form_validation->set_rules('alasan', 'Alasan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('cuti/tendik/c_hamil', $data);
            $this->load->view('layouts/footer');
        } else {

            $cutiDiambil = $this->input->post('cutiDiambil');
            $stokCuti = $this->input->post('stokCuti');
            $sisaCuti = ($stokCuti - $cutiDiambil);

            if ($sisaCuti < 0) {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('tendik/cuti/umroh');
            } else {
                $email = $this->input->post('email');
                $date = date("Y-m-d H:i:s");

                // cek jika ada gambar yang akan diupload
                $upload_image = $_FILES['scan']['name'];

                if ($upload_image) {
                    $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                    $config['max_size']      = '10048';
                    $config['upload_path'] = './assets/scan/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('scan')) {
                        $old_image = $data['occ_permohonan_cuti']['scan'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'assets/scan/' . $old_image);
                        }
                        $new_image = $this->upload->data('file_name');
                    } else {
                        echo $this->upload->display_errors();
                    }
                }


                $data = [
                    'email' => $email,
                    'jenisCuti' => "Cuti Hamil",
                    'cutiDiambil' => $this->input->post('cutiDiambil'),
                    'alasan' => $this->input->post('alasan'),
                    'sts' => 1,
                    'tglCuti' => $this->input->post('tglCuti'),
                    'tglSelesaiCuti' => $this->input->post('tglSelesaiCuti'),
                    'tglMasuk' => $this->input->post('tglMasuk'),
                    'tglPengajuan' => $date,
                    'created_at' => $date,
                    'scan' => $new_image

                ];


                $this->db->insert('occ_permohonan_cuti', $data);

                $this->db->set('cutiHamil', $sisaCuti);
                $this->db->where('email', $email);
                $this->db->update('mm_hak_cuti');
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('tendik/cuti');
            }
        }
    }

    public function data_pribadi()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'kdd' => $this->md->getDetailKdd(),
            'klg' => $this->md->getDetailKlg(),
            'ktam' => $this->md->getDetailKtam(),
            'bid' => $this->md->getDetailBid(),
            'alm' => $this->md->getDetailAlm(),
            'kpg' => $this->md->getDetaiKpg(),
            'lln' => $this->md->getDetailLln(),
            'ss' => $this->md->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('tendik/profil', $data);
        $this->load->view('layouts/footer');
    }

    public function detail()
    {
        $id_cuti = $this->uri->segment(4);
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_cuti' => 'active',
            'd' => $this->mc->detailCuti($id_cuti)

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/tendik/d_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function cetak()
    {
        $id_cuti = $this->uri->segment(4);
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_cuti' => 'active',
            'd' => $this->mc->detailCuti($id_cuti)

        );

        $this->load->view('cuti/tendik/cetak', $data);
    }

    public function vizinsakit()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_pizinsakit' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'isku' => $this->mc->getView_izinsakit(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/tendik/v_izinsakit', $data);
        $this->load->view('layouts/footer');
    }

    public function createIzinsakit()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_pizinsakit' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );

        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('tidakHadir', 'Lamanya Tidak Hadir', 'required|trim');
        $this->form_validation->set_rules('dariTanggal', 'Tanggal Mulai ', 'required|trim');
        $this->form_validation->set_rules('sampaiTanggal', 'Tanggal Selesai ', 'required|trim');
        $this->form_validation->set_rules('tglMasuk', 'Tanggal Masuk', 'required|trim');
        $this->form_validation->set_rules('ket', 'Alasan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('cuti/tendik/c_izinsakit', $data);
            $this->load->view('layouts/footer');
        } else {

            $tidakHadir = $this->input->post('tidakHadir');
            $email = $this->input->post('email');
            $date = date("Y-m-d H:i:s");

            $email = $this->input->post('email');
            $date = date("Y-m-d H:i:s");

            // cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['scan']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size']      = '10048';
                $config['upload_path'] = './assets/scan/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('scan')) {
                    $old_image = $data['occ_permohonan_cuti']['scan'];
                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'assets/scan/' . $old_image);
                    }
                    $new_image = $this->upload->data('file_name');
                } else {
                    echo $this->upload->display_errors();
                }
            }


            $data = [
                'email' => $email,
                'kode_refrensi' => time(),
                'tidakHadir' => $tidakHadir,
                'ket' => $this->input->post('ket'),
                'sts' => 1,
                'dariTanggal' => $this->input->post('dariTanggal'),
                'sampaiTanggal' => $this->input->post('sampaiTanggal'),
                'tglMasuk' => $this->input->post('tglMasuk'),
                'ket' => $this->input->post('ket'),
                'tglPengajuan' => $date,
                'created_at' => $date,
                'scan' => $new_image
            ];


            $this->db->insert('occ_izin_sakit', $data);
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('tendik/izinsakit');
        }
    }

    public function detailIzinsakit()
    {
        $id_is = $this->uri->segment(4);
        $data = array(
            'title' => 'Permohonan Izin Sakit',
            'active_menu_pizinsakit' => 'active',
            'd' => $this->mc->detailizinsakit($id_is)

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/tendik/d_izinsakit', $data);
        $this->load->view('layouts/footer');
    }


    public function ubahIzinsakit()
    {
        $id_is = $this->uri->segment(4);
        $data = array(
            'title' => 'Permohonan Izin Sakit',
            'active_menu_pizinsakit' => 'active',
            'd' => $this->mc->detail_izinsakit($id_is)

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/tendik/u_izinsakit', $data);
        $this->load->view('layouts/footer');
    }

    public function ubahIzinsakitGo()
    {
        $id_is = $this->uri->segment(4);
        $data = array(
            'title' => 'Permohonan Izin Sakit',
            'active_menu_pizinsakit' => 'active',
            'd' => $this->mc->detail_izinsakit($id_is)

        );
        $kode_refrensi = $this->input->post('kode_refrensi');
        $tidakHadir = $this->input->post('tidakHadir');
        $ket = $this->input->post('ket');
        $sts = 1;
        $dariTanggal = $this->input->post('dariTanggal');
        $sampaiTanggal = $this->input->post('sampaiTanggal');
        $tglMasuk = $this->input->post('tglMasuk');
        $ket = $this->input->post('ket');
        // cek jika ada gambar yang akan diupload
        $upload_image = $_FILES['scan']['name'];

        if ($upload_image) {
            $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
            $config['max_size']      = '10048';
            $config['upload_path'] = './assets/scan/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('scan')) {
                $old_image = $data['occ_izin_sakit']['scan'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'assets/scan/' . $old_image);
                }
                $new_image = $this->upload->data('file_name');
                $this->db->set('scan', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->set('tidakHadir', $tidakHadir);
        $this->db->set('dariTanggal', $dariTanggal);
        $this->db->set('sampaiTanggal', $sampaiTanggal);
        $this->db->set('tglMasuk', $tglMasuk);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->where('kode_refrensi', $kode_refrensi);
        $this->db->update('occ_izin_sakit');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('tendik/izinsakit');
    }
}
