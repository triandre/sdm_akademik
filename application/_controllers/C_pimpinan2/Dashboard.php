<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_db' => 'active',

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/dashboard', $data);
        $this->load->view('layouts/footer');
    }
}
