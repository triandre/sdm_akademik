<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Adminkeu extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->library('uuid');
        $this->load->model('m_presensi/ModelPresensi', 'mp');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_db' => 'active',

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/index', $data);
        $this->load->view('layouts/footer');
    }

    public function listPresensi()
    {
        $data = array(
            'title' => 'List Presensi Dosen Tetap',
            'active_menu_presensi' => 'menu-open',
            'active_menu_prs' => 'active',
            'active_menu_presensi_tetap' => 'active',
            'v' => $this->mp->getPresensiAdmin()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/v_presensi', $data);
        $this->load->view('layouts/footer');
    }


    public function detailPresensi()
    {
        $nidn = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Rekap Presensi Dosen',
            'active_menu_presensi' => 'menu-open',
            'active_menu_prs' => 'active',
            'active_menu_presensi_create' => 'active',
            'v' => $this->mp->getDetailPresensi($nidn),
            'd' => $this->mp->getDetailDosen($nidn),
            'jsks' => $this->mp->getJumlahSks($nidn),
            'hadir' => $this->mp->getJumlahKehadiran($nidn),
            'transport' => $this->mp->getJumlahTansport($nidn),
            'tarif' => $this->mp->getJumlahTarif($nidn),
            'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),
            'potongan' => $this->mp->getPotongan($nidn),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/d_presensi', $data);
        $this->load->view('layouts/footer');
    }
    public function tarifdosen()
    {
        $id_reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Tarif Pengajaran',
            'active_menu_presensi' => 'menu-open',
            'active_menu_prs' => 'active',
            'active_menu_presensi_tetap' => 'active',
            'd' => $this->mp->getTarifDosen2($id_reff),
            'v' => $this->mp->editTarifDosen($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/t_presensi', $data);
        $this->load->view('layouts/footer');
    }

    public function simpanTarif()
    {
        $id_reff = $this->input->post('id_reff', true);
        $nidn =  htmlspecialchars($this->input->post('nidn', true));
        $tarif =  htmlspecialchars($this->input->post('tarif', true));
        $total_tarif =  htmlspecialchars($this->input->post('total_tarif', true));


        $log = [
            'log' => "Rekap Kehadiran",
            'email' => $email,
            'date_created' => time()
        ];
        $this->db->set('tarif', $tarif);
        $this->db->set('total_tarif', $total_tarif);
        $this->db->where('id_reff', $id_reff);
        $this->db->update('occ_presensi_dosen');

        $this->db->insert('occ_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');

        redirect('adminkeu/detailPresensi/' . $nidn);
    }

    public function mtarif()
    {
        $data = array(
            'title' => 'Manajemen Tarif Pengajaran',
            'active_menu_mtarif' => 'active',
            'v' => $this->mp->getTarifDosen()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/v_mtarif', $data);
        $this->load->view('layouts/footer');
    }
    public function mtarifDosen()
    {
        $nidn = $this->uri->segment(3);
        $data = array(
            'title' => 'Manajemen Tarif Pengajaran',
            'active_menu_mtarif' => 'active',
            'd' => $this->mp->editTarifDosen2($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/u_mtarif', $data);
        $this->load->view('layouts/footer');
    }

    public function mtarifDosenGo()
    {
        $id = $this->uuid->v4();
        $reff = str_replace('-', '', $id);
        $nidn = $this->input->post('nidn', true);
        $tarif = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('tarif', true)));
        $pinjaman_lkk = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('pinjaman_lkk', true)));
        $sarana_dakwah = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('sarana_dakwah', true)));


        $this->db->set('tarif', $tarif);
        $this->db->set('pinjaman_lkk', $pinjaman_lkk);
        $this->db->set('sarana_dakwah', $sarana_dakwah);
        $this->db->where('nidn', $nidn);
        $this->db->update('occ_user');
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('adminkeu/mtarif');
    }

    public function lapPresensi()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_lap_presensi' => 'active',

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/lap_presensi', $data);
        $this->load->view('layouts/footer');
    }

    public function v_lapPresensi()
    {
        $bulan = $this->input->post('bulan', true);
        $tahun =  htmlspecialchars($this->input->post('tahun', true));
        $email_created =  htmlspecialchars($this->input->post('email_created', true));
        $data = array(
            'title' => 'Laporan Presensi',
            'active_menu_lap_presensi' => 'active',
            'v' => $this->mp->getLapPresensiAdmin($bulan, $tahun, $email_created)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/v_lap_presensi', $data);
        $this->load->view('layouts/footer');
    }

    public function listPresensi_dt()
    {
        $data = array(
            'title' => 'List Presensi Dosen Tidak Tetap',
            'active_menu_presensi' => 'menu-open',
            'active_menu_prs' => 'active',
            'active_menu_presensi_tidaktetap' => 'active',
            'v' => $this->mp->getPresensiAdmin_dt()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/v_presensi_dt', $data);
        $this->load->view('layouts/footer');
    }

    public function detailPresensi_dt()
    {
        $nidn = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Rekap Presensi Dosen Tidak Tetap',
            'active_menu_presensi' => 'menu-open',
            'active_menu_prs' => 'active',
            'active_menu_presensi_create' => 'active',
            'v' => $this->mp->getDetailPresensi($nidn),
            'jsks' => $this->mp->getJumlahSks($nidn),
            'hadir' => $this->mp->getJumlahKehadiran($nidn),
            'transport' => $this->mp->getJumlahTansport($nidn),
            'tarif' => $this->mp->getJumlahTarif($nidn),
            'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),
            'potongan' => $this->mp->getPotongan($nidn),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/d_presensi_dt', $data);
        $this->load->view('layouts/footer');
    }

    public function tarifdosen_dt()
    {
        $id_reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Tarif Pengajaran',
            'active_menu_presensi' => 'menu-open',
            'active_menu_prs' => 'active',
            'active_menu_presensi_tetap' => 'active',
            'd' => $this->mp->getTarifDosen2($id_reff),
            'v' => $this->mp->editTarifDosen($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('adminkeu/t_presensi_dt', $data);
        $this->load->view('layouts/footer');
    }

    public function simpanTarif_dt()
    {
        $id_reff = $this->input->post('id_reff', true);
        $nidn =  htmlspecialchars($this->input->post('nidn', true));
        $tarif =  htmlspecialchars($this->input->post('tarif', true));
        $total_tarif =  htmlspecialchars($this->input->post('total_tarif', true));


        $log = [
            'log' => "Rekap Kehadiran",
            'email' => $email,
            'date_created' => time()
        ];
        $this->db->set('tarif', $tarif);
        $this->db->set('total_tarif', $total_tarif);
        $this->db->where('id_reff', $id_reff);
        $this->db->update('occ_presensi_dosen');

        $this->db->insert('occ_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');

        redirect('adminkeu/detailPresensi_dt/' . $nidn);
    }
}
