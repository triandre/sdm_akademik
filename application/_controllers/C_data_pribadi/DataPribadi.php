<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DataPribadi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'du' => $this->md->getDetailUser(),
            'kdd' => $this->md->getDetailKdd(),
            'klg' => $this->md->getDetailKlg(),
            'ktam' => $this->md->getDetailKtam(),
            'bid' => $this->md->getDetailBid(),
            'alm' => $this->md->getDetailAlm(),
            'kpg' => $this->md->getDetaiKpg(),
            'lln' => $this->md->getDetailLln(),
            'ss' => $this->md->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/v_dashboard', $data);
        $this->load->view('layouts/footer');
    }
}
