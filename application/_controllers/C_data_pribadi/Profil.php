<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_data_pribadi/Model_profil', 'mmm');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'profil' => $this->mmm->getView(),
            'd' => $this->mmm->getDetail(),
            'du' => $this->md->getDetailUser(),
            'diajukan' => $this->mmm->viewDiajukan(),
            'disetujui' => $this->mmm->viewDisetujui(),
            'ditolak' => $this->mmm->viewTolak(),
            'nDiajukan' => $this->mmm->numsDiajukan(),
            'nDisetujui' => $this->mmm->numsDisetujui(),
            'nDitolak' => $this->mmm->numsTolak()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/profil/c_profil', $data);
        $this->load->view('layouts/footer');
    }

    public function updateProfil()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'profil' => $this->mmm->getView(),
            'd' => $this->mmm->getDetail(),
            'diajukan' => $this->mmm->viewDiajukan(),
            'disetujui' => $this->mmm->viewDisetujui(),
            'ditolak' => $this->mmm->viewTolak(),
            'nDiajukan' => $this->mmm->numsDiajukan(),
            'nDisetujui' => $this->mmm->numsDisetujui(),
            'nDitolak' => $this->mmm->numsTolak()
        );
        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/profil/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_profil']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/profil/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $id = $this->uuid->v4();
        $reff_profil = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADPROFIL";
        $jenis_ajuan = "Ajuan Data Profil";

        $data = array(
            'reff_profil' => $reff_profil,
            'nidn' => $nidn,
            'jk' => htmlspecialchars($this->input->post('jk', true)),
            't_lahir' => htmlspecialchars($this->input->post('t_lahir', true)),
            'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir', true)),
            'nama_ibu' => htmlspecialchars($this->input->post('nama_ibu', true)),
            'sts' => $sts,
            'file' => $upload1,
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_profil,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->mmm->storeProfil($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('uprofil');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('uprofil');
        }
    }

    public function uploadFoto()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'profil' => $this->mmm->getView(),
            'd' => $this->mmm->getDetail(),
            'diajukan' => $this->mmm->viewDiajukan(),
            'disetujui' => $this->mmm->viewDisetujui(),
            'ditolak' => $this->mmm->viewTolak(),
            'nDiajukan' => $this->mmm->numsDiajukan(),
            'nDisetujui' => $this->mmm->numsDisetujui(),
            'nDitolak' => $this->mmm->numsTolak()
        );


        //upload
        $nidn = $this->session->userdata('nidn');
        $upload_image = $_FILES['image']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size']      = '10000';
            $config['upload_path'] = './assets/images/profil/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('image')) {
                $old_image = $data['occ_user']['image'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'assets/images/profil/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('image', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->where('nidn', $nidn);
        $this->db->update('occ_user');
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('dpribadi');
    }

    public function detailRiwayatProfil()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Riwayat Pengajuan',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'profil' => $this->mmm->getView(),
            'd' => $this->mmm->getDetail(),
            'dr' => $this->mmm->detailRiwayatProfil($reff),
            'diajukan' => $this->mmm->viewDiajukan(),
            'disetujui' => $this->mmm->viewDisetujui(),
            'ditolak' => $this->mmm->viewTolak(),
            'nDiajukan' => $this->mmm->numsDiajukan(),
            'nDisetujui' => $this->mmm->numsDisetujui(),
            'nDitolak' => $this->mmm->numsTolak()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/profil/d_profil', $data);
        $this->load->view('layouts/footer');
    }
}
