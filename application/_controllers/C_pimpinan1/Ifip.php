<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ifip extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan1/Model_ifip', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Prosiding (Forum Ilmiah)',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vifip' => 'active',
            'v' => $this->ma->getIfip(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/ifip/v_ifip', $data);
        $this->load->view('layouts/footer');
    }


    public function fee()
    {
        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Persetujuan Usulan Insentif Prosiding (Forum Ilmiah)',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vifip' => 'active',
            'v' => $this->ma->getIfip(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan1/ifip/s_ifip', $data);
        $this->load->view('layouts/footer');
    }

    public function feeGo()
    {

        $id = $this->input->post('id', true);
        $catatan1 = $this->input->post('catatan1', true);
        $sts = 3;
        $date_wr1 = date('Y-m-d H:i:s');

        $this->db->set('date_wr1', $date_wr1);
        $this->db->set('sts', $sts);
        $this->db->set('catatan1', $catatan1);;
        $this->db->where('id', $id);
        $this->db->update('ins_ifip');

        $log = [
            'log' => "Pak WR 1 Menyetujui Usulan Insentif Prosiding (Forum Ilmiah) Insentif Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan1/vifip');
    }

    public function tolak()
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Penolakan Usulan Insentif Prosiding (Forum Ilmiah)',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vifip' => 'active',
            'v' => $this->ma->getIfip(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan1/ifip/t_ifip', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $tolak1 = $this->input->post('tolak1', true);
        $sts = 6;
        $date_wr1 = date('Y-m-d H:i:s');
       
        
        $this->db->set('date_wr1', $date_wr1);
        $this->db->set('sts', $sts);
        $this->db->set('tolak1', $tolak1);s
        $this->db->where('id', $id);
        $this->db->update('ins_ifip');

        $log = [
            'log' => "PAK Wr 1 Menolak Usulan Insentif Prosiding (Forum Ilmiah) ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan1/vifip');
    }
}
