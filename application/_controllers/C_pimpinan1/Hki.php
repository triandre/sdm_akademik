<!-- INSENTIF HKI -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hki extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan1/Model_hki', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif HKI',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vhki' => 'active',
            'hki' => $this->ma->getHki(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/hki/v_hki', $data);
        $this->load->view('layouts/footer');
    }
   
    public function fee()
    {
        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Persetujuan Usulan Insentif HKI',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vhki' => 'active',
            'hki' => $this->ma->getHki(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan/hki/s_hki', $data);
        $this->load->view('layouts/footer');
    }

    public function feeGo()
    {
        $id = $this->input->post('id', true);
        $catatan1 = $this->input->post('catatan1', true);
        $sts = 3;
        $date_wr1 = date('Y-m-d H:i:s');

       
        $this->db->set('date_wr1', $date_wr1);
        $this->db->set('sts', $sts);
        $this->db->set('catatan1', $catatan1);
        $this->db->where('id', $id);
        $this->db->update('ins_hki');

        $log = [
            'log' => "Mengusul Insentif HKI Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan/vhki');
    }


    public function tolak()
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Penolakan Usulan Insentif HKI',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vhki' => 'active',
            'hki' => $this->ma->getHki(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan/hki/t_hki', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $tolak1 = $this->input->post('tolak1', true);
        $sts = 6;
        $date_wr1 = date('Y-m-d H:i:s');

        $this->db->set('date_wr1', $date_wr1);
        $this->db->set('sts', $sts);
        $this->db->set('tolak1', $tolak1);
        $this->db->where('id', $id);
        $this->db->update('ins_hki');

        $log = [
            'log' => "Pak WR1 Menolak Insentif HKI ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan/vhki');
    }
}
