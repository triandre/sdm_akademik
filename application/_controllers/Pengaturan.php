<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengaturan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }

        //load model user
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pengaturan/Model_pengaturan', 'mdd');
        $this->load->model('m_sesi/Model_user', 'mu');
    }

    public function ubahPassword()
    {

        $this->form_validation->set_rules(
            'password',
            'Password Baru',
            'required|min_length[5]',
            array(
                'required' => "<p>Password tidak boleh kosong</p>",
                'min_length' => "<p>Password minimal 5 Karakter</p>"
            )
        );

        if ($this->form_validation->run() != false) {

            $password = $this->input->post('password');
            $password_dua = $this->input->post('password_dua');
            if ($password == $password_dua) {

                $data_user = array(
                    'password' => password_hash($password, PASSWORD_DEFAULT)
                );

                $email = $this->session->userdata('email');
                $query = $this->mu->update_user($email, $data_user);
                if ($query) {
                    $this->session->set_flashdata('sukses', 'Diubah');
                    redirect('ubahPassword');
                } else {
                    $this->session->set_flashdata('gagal', 'Diubah');
                    redirect('ubahPassword');
                }
            } else {
                $this->session->set_flashdata('gagal_store', 'Password yang anda masukan tidak sama..');
                redirect('ubahPassword');
            }
        } else {
            $data = array(
                'title' => 'Ubah Password',
                'active_menu_pengaturan' => 'menu-open',
                'active_menu_pr' => 'active',
                'active_menu_ubahpass' => 'active',
                'user' => $this->mdd->getUser()
            );
            $this->load->view('layouts/header', $data);
            $this->load->view('pengaturan/ubahPassword', $data);
            $this->load->view('layouts/footer');
        }
    }
}
