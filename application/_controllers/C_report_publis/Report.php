<!-- Track -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_track/Model_track', 'ma');
        $this->load->model('m_laporan_publis/Model_laporan', 'mlp');
        $this->load->model('m_laporan_publis/Model_file', 'mf');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Laporan Publis',
            'active_menu_rpublis' => 'active',
            'akun' => $this->mu->getUser(),
            'jenis' => $this->ma->getJi(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('report_publis/index', $data);
        $this->load->view('layouts/footer');
    }

    public function cariData()
    {
        $jenis_insentif = $this->input->post('jenis_insentif');
        $batch = $this->input->post('batch');
        //=====================================================================
        if ($jenis_insentif == 8) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->getPj($batch),
                'zat' => $this->mlp->total_inspj($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_pj', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 7) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->insiks($batch),
                'zat' => $this->mlp->total_insiks($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_iks', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 6) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->insbfio($batch),
                'zat' => $this->mlp->total_insbfio($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_bfio', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 5) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->insiba($batch),
                'zat' => $this->mlp->total_insiba($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_iba', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 4) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->insbbp($batch),
                'zat' => $this->mlp->total_insbpp($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_bbp', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 3) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->insifip($batch),
                'zat' => $this->mlp->total_insifip($batch)
            );
            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_ifip', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 2) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->inshki($batch),
                'zat' => $this->mlp->total_inshki($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_hki', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 1) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->insaji($batch),
                'zat' => $this->mlp->total_insaji($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_aji', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 10) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->insubpjs($batch),
                'zat' => $this->mlp->total_insubpjs($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_ubpjs', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 9) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->insuipjs($batch),
                'zat' => $this->mlp->total_insuipjs($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_uipjs', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
            //=====================================================================
        } elseif ($jenis_insentif == 11) {
            $data = array(
                'title' => 'Laporan Publis',
                'active_menu_rpublis' => 'active',
                'akun' => $this->mu->getUser(),
                'jenis' => $this->ma->getJi(),
                'v' => $this->mlp->insbhki($batch),
                'zat' => $this->mlp->total_bhki($batch)
            );

            if (count($data['v']) > 0) {
                $this->load->view('layouts/header', $data);
                $this->load->view('report_publis/v_bhki', $data);
                $this->load->view('layouts/footer');
            } else {
                $this->session->set_flashdata('gagal', 'Ditemukan');
                redirect('reportpublis');
            }
        }
    }
    public function pdf_Aji()
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->insaji($batch);
        $data['zat'] = $this->mf->total_insaji($batch);
        $this->load->view('report_publis/pdf/cetak_aji', $data);
    }
    public function pdf_Pj($batch)
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->inspj($batch);
        $data['zat'] = $this->mf->total_inspj($batch);
        $this->load->view('report_publis/pdf/cetak_pj', $data);
    }
    public function pdf_Hki($batch)
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->inshki($batch);
        $data['zat'] = $this->mf->total_inshki($batch);
        $this->load->view('report_publis/pdf/cetak_hki', $data);
    }
    public function pdf_Iks($batch)
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->insiks($batch);
        $data['zat'] = $this->mf->total_insiks($batch);
        $this->load->view('report_publis/pdf/cetak_iks', $data);
    }
    public function pdf_Ifip($batch)
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->insifip($batch);
        $data['zat'] = $this->mf->total_insifip($batch);
        $this->load->view('report_publis/pdf/cetak_ifip', $data);
    }
    public function pdf_Iba($batch)
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->insiba($batch);
        $data['zat'] = $this->mf->total_insiba($batch);
        $this->load->view('report_publis/pdf/cetak_iba', $data);
    }
    public function pdf_Uipjs($batch)
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->insuipjs($batch);
        $data['zat'] = $this->mf->total_insuipjs($batch);
        $this->load->view('report_publis/pdf/cetak_uipjs', $data);
    }
    public function pdf_Bbp($batch)
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->insbbp($batch);
        $data['zat'] = $this->mf->total_insbbp($batch);
        $this->load->view('report_publis/pdf/cetak_bbp', $data);
    }
    public function pdf_Bfio($batch)
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->insbfio($batch);
        $data['zat'] = $this->mf->total_insbfio($batch);
        $this->load->view('report_publis/pdf/cetak_bfio', $data);
    }
    public function pdf_Ubpjs($batch)
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->insubpjs($batch);
        $data['zat'] = $this->mf->total_insubpjs($batch);
        $this->load->view('report_publis/pdf/cetak_ubpjs', $data);
    }
    public function pdf_Bhki()
    {
        $batch = $this->uri->segment(3);
        $data['gas'] = $this->mf->insbhki($batch);
        $data['zat'] = $this->mf->total_bhki($batch);
        $this->load->view('report_publis/pdf/cetak_bhki', $data);
    }

    public function excel_Aji($batch)
    {
        $batch = $this->uri->segment(3);
        $aji = $this->mf->insaji_excel($batch);

        $spreadsheet = new Spreadsheet;
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'NO')
            ->setCellValue('B1', 'NIDN')
            ->setCellValue('C1', 'NAMA')
            ->setCellValue('D1', 'KONTRIBUSI')
            ->setCellValue('E1', 'JUDUL')
            ->setCellValue('F1', 'NAMA JURNAL')
            ->setCellValue('G1', 'HASIL PEMERIKSAAN')
            ->setCellValue('H1', 'INSENTIF');
        $kolom = 2;
        $nomor = 1;
        foreach ($aji as $item) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $kolom, $nomor)
                ->setCellValue('B' . $kolom, $item->nidn)
                ->setCellValue('C' . $kolom, $item->name)
                ->setCellValue('D' . $kolom, $item->kontribusi)
                ->setCellValue('E' . $kolom, $item->judul)
                ->setCellValue('F' . $kolom, $item->nama_jurnal)
                ->setCellValue('G' . $kolom, $item->ket)
                ->setCellValue('H' . $kolom, $item->insentif_disetujui);
            $kolom++;
            $nomor++;
        }

        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Data_Insentif_Artikel_Dijurnal.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function excel_Hki($batch)
    {
        $batch = $this->uri->segment(3);
        $aji = $this->mf->inshki_excel($batch);

        $spreadsheet = new Spreadsheet;
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'NO')
            ->setCellValue('B1', 'NIDN')
            ->setCellValue('C1', 'NAMA')
            ->setCellValue('D1', 'JUDUL')
            ->setCellValue('E1', 'NO. SERTIFIKAT')
            ->setCellValue('F1', 'JENIS HKI')
            ->setCellValue('G1', 'HASIL PEMERIKSAAN')
            ->setCellValue('H1', 'INSENTIF');
        $kolom = 2;
        $nomor = 1;
        foreach ($aji as $item) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $kolom, $nomor)
                ->setCellValue('B' . $kolom, $item->nidn)
                ->setCellValue('C' . $kolom, $item->name)
                ->setCellValue('D' . $kolom, $item->judul_hki)
                ->setCellValue('E' . $kolom, $item->no_sertifikat)
                ->setCellValue('F' . $kolom, $item->jenis_hki)
                ->setCellValue('G' . $kolom, $item->ket)
                ->setCellValue('H' . $kolom, $item->insentif_disetujui);
            $kolom++;
            $nomor++;
        }

        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Data_Insentif_HKI.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}
