<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Munit extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_manajemen/Model_munit', 'mm');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Manajemen Unit',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_munit' => 'active',
            'v' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/munit/v_munit', $data);
        $this->load->view('layouts/footer');
    }

    public function new_unit()
    {
        $data = array(
            'title' => 'Tambah Data Unit',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_munit' => 'active',
            'v' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/munit/c_munit', $data);
        $this->load->view('layouts/footer');
    }

    public function save_unit()
    {
        $data = array(
            'title' => 'Tambah Data Unit',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_munit' => 'active',
            'v' => $this->mm->getView()
        );
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/arsip/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['mm_unit']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/arsip/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'nickname' =>  htmlspecialchars($this->input->post('nickname', true)),
            'unit' =>  htmlspecialchars($this->input->post('unit', true)),
            'aktif' => 1,
            'file' => $upload1,
            'date_created' => date('Y-m-d H:i:s')
        );

        $result = $this->mm->save_unit($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('munit');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('munit/new');
        }
    }


    public function hapus_unit()
    {
        $id_unit = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_unit', $id_unit);
        $this->db->update('mm_unit');
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('munit');
    }

    public function edit_unit()
    {
        $id_unit = $this->uri->segment(3);
        $data = array(
            'title' => 'Edit Data Unit',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_munit' => 'active',
            'd' => $this->mm->detail_unit($id_unit)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/munit/e_munit', $data);
        $this->load->view('layouts/footer');
    }

    public function editGo_unit()
    {
        $data = array(
            'title' => 'Edit Data Unit',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_munit' => 'active',
            'v' => $this->mm->getView()
        );
        $id_unit =  htmlspecialchars($this->input->post('id_unit', true));
        $nickname = htmlspecialchars($this->input->post('nickname', true));
        $unit = htmlspecialchars($this->input->post('unit', true));

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/arsip/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['mm_unit']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/arsip/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->set('nickname', $nickname);
        $this->db->set('unit', $unit);
        $this->db->where('id_unit', $id_unit);
        $this->db->update('mm_unit');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('munit');
    }
}
