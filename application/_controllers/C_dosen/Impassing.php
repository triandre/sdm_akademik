<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Impassing extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/ModelImpassing', 'mi');
        $this->load->model('m_master/ModelPangkat', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Inpassing',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_impassing' => 'active',
            'impassing' => $this->mi->getImpassing(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/impassing/v_impassing', $data);
        $this->load->view('layouts/footer');
    }

    public function createImpassing()
    {
        $data = array(
            'title' => 'Impassing',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_impassing' => 'active',
            'impassing' => $this->mi->getImpassing(),
            'pangkat' => $this->mp->getPangkat()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/impassing/c_impassing', $data);
        $this->load->view('layouts/footer');
    }

    public function saveImpassing()
    {
        $data = array(
            'title' => 'Profil',
            'active_menu_profil' => 'active',
            'active_menu_data_pribadi' => 'active',
            'bio' => $this->md->getProfil()
        );

        $id = $this->uuid->v4();
        $reff_impassing = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADI";
        $jenis_ajuan = "Ajuan Data Inpassing";

        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/impassing/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_impassing']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/impassing/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_impassing' => $reff_impassing,
            'nidn' => $nidn,
            'id_pangkat' => htmlspecialchars($this->input->post('id_pangkat', true)),
            'no_sk_impassing' => htmlspecialchars($this->input->post('no_sk_impassing', true)),
            'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
            'sk_terhitung' => htmlspecialchars($this->input->post('sk_terhitung', true)),
            'angka_kredit' => htmlspecialchars($this->input->post('angka_kredit', true)),
            'masa_kerja_tahun' => htmlspecialchars($this->input->post('masa_kerja_tahun', true)),
            'masa_kerja_bulan' => htmlspecialchars($this->input->post('masa_kerja_bulan', true)),
            'sts' => $sts,
            'file' => $upload1,
            'active' => 1,
            'created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_impassing,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->mi->storeImpassing($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('impassing');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createImpassing');
        }
    }

    public function hapusImpassing($id_impassing)
    {
        $id_impassing = $this->uri->segment(2);
        $active = 0;

        $this->db->set('active', $active);
        $this->db->where('id_impassing', $id_impassing);
        $this->db->update('occ_impassing');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('impassing');
    }

    public function detailImpassing()
    {
        $id_impassing = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Impassing',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_impassing' => 'active',
            'd' => $this->mi->detailImpassing($id_impassing),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/impassing/d_impassing', $data);
        $this->load->view('layouts/footer');
    }

    public function editImpassing()
    {
        $id_impassing = $this->uri->segment(2);
        $data = array(
            'title' => 'Edit Inpassing',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_impassing' => 'active',
            'd' => $this->mi->detailImpassing($id_impassing),
            'pangkat' => $this->mp->getPangkat()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/impassing/e_impassing', $data);
        $this->load->view('layouts/footer');
    }

    public function updateImpassing()
    {

        $data = array(
            'title' => 'Profil',
            'active_menu_profil' => 'active',
            'active_menu_data_pribadi' => 'active',
            'bio' => $this->md->getProfil()
        );

        $id_impassing = $this->input->post('id_impassing');

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/inpassing/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_impassing']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/impassing/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'id_pangkat' => htmlspecialchars($this->input->post('id_pangkat', true)),
            'no_sk_impassing' => htmlspecialchars($this->input->post('no_sk_impassing', true)),
            'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
            'sk_terhitung' => htmlspecialchars($this->input->post('sk_terhitung', true)),
            'angka_kredit' => htmlspecialchars($this->input->post('angka_kredit', true)),
            'masa_kerja_tahun' => htmlspecialchars($this->input->post('masa_kerja_tahun', true)),
            'masa_kerja_bulan' => htmlspecialchars($this->input->post('masa_kerja_bulan', true))
        );
        $result = $this->mi->updateImpassing($id_impassing, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('impassing');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('editImpassing/' . $id_impassing);
        }
    }
}
