<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pubkar extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pel_penelitian/Model_pubkar', 'mpk');
        $this->load->model('m_master/ModelPangkat', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Riwayat Publikasi KArya',
            'active_menu_ppen' => 'menu-open',
            'active_menu_ppn' => 'active',
            'active_menu_pubkar' => 'active',
            'v' => $this->mpk->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_penelitian/publikasi/v_publikasi', $data);
        $this->load->view('layouts/footer');
    }

    public function createPenelitian()
    {
        $data = array(
            'title' => 'Form Tambah Riwayat Penelitian',
            'active_menu_ppen' => 'menu-open',
            'active_menu_ppn' => 'active',
            'active_menu_penelitian' => 'active',
            'v' => $this->mpk->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_penelitian/penelitian/c_penelitian', $data);
        $this->load->view('layouts/footer');
    }

    public function savePenelitian()
    {
        $data = array(
            'title' => 'Form Tambah Riwayat Penelitian',
            'active_menu_ppen' => 'menu-open',
            'active_menu_ppn' => 'active',
            'active_menu_penelitian' => 'active',
            'v' => $this->mpk->getView()

        );
        $id = $this->uuid->v4();
        $reff_penelitian = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        //upload
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/penelitian/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_penelitian']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/penelitian/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_penelitian' => $reff_penelitian,
            'nidn' => $nidn,
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'judul_kgt' => htmlspecialchars($this->input->post('judul_kgt', true)),
            'afiliasi' => htmlspecialchars($this->input->post('afiliasi', true)),
            'kelompok' => htmlspecialchars($this->input->post('kelompok', true)),
            'litabmas_sebelum' => htmlspecialchars($this->input->post('litabmas_sebelum', true)),
            'jenis_skim' => htmlspecialchars($this->input->post('jenis_skim', true)),
            'lokasi' => htmlspecialchars($this->input->post('lokasi', true)),
            'tahun_usulan' => htmlspecialchars($this->input->post('tahun_usulan', true)),
            'tahun_kegiatan' => htmlspecialchars($this->input->post('tahun_kegiatan', true)),
            'tahun_pelaksanaan' => htmlspecialchars($this->input->post('tahun_pelaksanaan', true)),
            'lama_kgt' => htmlspecialchars($this->input->post('lama_kgt', true)),
            'tahun_pelaksanaan_ke' => htmlspecialchars($this->input->post('tahun_pelaksanaan_ke', true)),
            'dana_dikti' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('dana_dikti', true))),
            'dana_pt' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('dana_pt', true))),
            'dana_pt_lain' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('dana_pt_lain', true))),
            'in_kind' => htmlspecialchars($this->input->post('in_kind', true)),
            'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
            'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
            'file' => $upload1,
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s')
        );


        $result = $this->mpk->storePenelitian($data);
        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('penelitian');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createPenelitian');
        }
    }

    public function perbaikanPenelitian()
    {
        $reff_penelitian = $this->uri->segment(3);
        $data = array(
            'title' => 'Update Riwayat Penelitian',
            'active_menu_ppen' => 'menu-open',
            'active_menu_ppn' => 'active',
            'active_menu_penelitian' => 'active',
            'v' => $this->mpk->getView(),
            'd' => $this->mpk->getDetail($reff_penelitian)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_penelitian/penelitian/u_penelitian', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanPenelitianGo()
    {

        $data = array(
            'title' => 'Update Riwayat Penelitian',
            'active_menu_ppen' => 'menu-open',
            'active_menu_ppn' => 'active',
            'active_menu_penelitian' => 'active',
            'v' => $this->mpk->getView()
        );

        $reff_penelitian = htmlspecialchars($this->input->post('reff_penelitian', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/penelitian/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_penelitian']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/penelitian/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'judul_kgt' => htmlspecialchars($this->input->post('judul_kgt', true)),
            'afiliasi' => htmlspecialchars($this->input->post('afiliasi', true)),
            'kelompok' => htmlspecialchars($this->input->post('kelompok', true)),
            'litabmas_sebelum' => htmlspecialchars($this->input->post('litabmas_sebelum', true)),
            'jenis_skim' => htmlspecialchars($this->input->post('jenis_skim', true)),
            'lokasi' => htmlspecialchars($this->input->post('lokasi', true)),
            'tahun_usulan' => htmlspecialchars($this->input->post('tahun_usulan', true)),
            'tahun_kegiatan' => htmlspecialchars($this->input->post('tahun_kegiatan', true)),
            'tahun_pelaksanaan' => htmlspecialchars($this->input->post('tahun_pelaksanaan', true)),
            'lama_kgt' => htmlspecialchars($this->input->post('lama_kgt', true)),
            'tahun_pelaksanaan_ke' => htmlspecialchars($this->input->post('tahun_pelaksanaan_ke', true)),
            'dana_dikti' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('dana_dikti', true))),
            'dana_pt' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('dana_pt', true))),
            'dana_pt_lain' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('dana_pt_lain', true))),
            'in_kind' => htmlspecialchars($this->input->post('in_kind', true)),
            'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
            'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
            'file' => $upload1,
        );


        $result = $this->mpk->updatePenelitian($reff_penelitian, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('penelitian');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('penelitian/perbaikanPenelitian/' . $reff_penelitian);
        }
    }

    public function hapusPenelitian($reff_penelitian)
    {
        $reff_penelitian = $this->uri->segment(2);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('reff_penelitian', $reff_penelitian);
        $this->db->update('occ_penelitian');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('penelitian');
    }

    public function detailPenelitian()
    {
        $reff_penelitian = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Tambah Riwayat Penelitian',
            'active_menu_ppen' => 'menu-open',
            'active_menu_ppn' => 'active',
            'active_menu_penelitian' => 'active',
            'v' => $this->mpk->getView(),
            'd' => $this->mpk->getDetail($reff_penelitian)

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_penelitian/penelitian/d_penelitian', $data);
        $this->load->view('layouts/footer');
    }
}
