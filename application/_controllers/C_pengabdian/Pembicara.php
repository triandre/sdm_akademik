<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembicara extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pengabdian/Model_pembicara', 'mpa');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Riwayat Pembicara',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pembicara' => 'active',
            'akun' => $this->mu->getUser(),
            'v' => $this->mpa->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pengabdian/pembicara/v_pembicara', $data);
        $this->load->view('layouts/footer');
    }

    public function detailPembicara()
    {
        $reff_jastru = $this->uri->segment(2);
        $data = array(
            'title' => 'Tambah Riwayat Pembicara',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pembicara' => 'active',
            'v' => $this->mpa->getView(),
            'd' => $this->mpa->getDetail($reff_jastru)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pengabdian/pembicara/d_pembicara', $data);
        $this->load->view('layouts/footer');
    }
}
