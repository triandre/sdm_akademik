<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pj extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_pj', 'mp');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Pengelola Jurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_pj' => 'active',
            'pj' => $this->mp->getPj(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/pj/v_pj', $data);
        $this->load->view('layouts/footer');
    }

    public function createPj()
    {
        $data = array(
            'title' => 'Form Usulan Pengelola Jurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_pj' => 'active',
            'pj' => $this->mp->getPj(),
            'akun' => $this->mu->getUser()

        );

        $this->form_validation->set_rules('nama_jurnal', 'Nama Jurnal', 'required|trim');
        $this->form_validation->set_rules('url_jurnal', 'URL Jurnal', 'required|trim');
        $this->form_validation->set_rules('peringkat_jurnal', 'Peringkat Jurnal', 'required|trim');
        $this->form_validation->set_rules('url_issue', 'URL ISSUE', 'required|trim');
        $this->form_validation->set_rules('vnt', 'vnt', 'required|trim');
        $this->form_validation->set_rules('url_garuda', 'url garuda', 'required|trim');
        $this->form_validation->set_rules('url_schoolar', 'url Schoolar', 'required|trim');
        $this->form_validation->set_rules('kategori_pj', 'Kategori', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/publis/pj/c_pj', $data);
            $this->load->view('layouts/footer');
        } else {
            //PERIKSA
            $email = $this->session->userdata('email');
            $judul = htmlspecialchars($this->input->post('nama_jurnal', true));
            $vnt = htmlspecialchars($this->input->post('vnt', true));
            $sql = $this->db->query("SELECT nama_jurnal, vnt FROM ins_pj where nama_jurnal='$judul' AND vnt='$vnt' AND batch='5'");
            $cek_pj = $sql->num_rows();
            if ($cek_pj > 0) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">
        <strong>Maaf!</strong> Nama Jurnal Dan Volume/Nomor/Tahun Sudah Pernah Diajukan.
      </div>');
                redirect('pj');
            } else {

                $upload_image = $_FILES['upload_permohonan']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_permohonan')) {
                        $old_image = $data['ins_pj']['upload_permohonan'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload1 = $this->upload->data('file_name');
                        $this->db->set('upload_permohonan', $upload1);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }

                $data = [
                    'nama_jurnal' => htmlspecialchars($this->input->post('nama_jurnal', true)),
                    'url_jurnal' => htmlspecialchars($this->input->post('url_jurnal', true)),
                    'peringkat_jurnal' => htmlspecialchars($this->input->post('peringkat_jurnal', true)),
                    'url_issue' => htmlspecialchars($this->input->post('url_issue', true)),
                    'vnt' => htmlspecialchars($this->input->post('vnt', true)),
                    'url_garuda' => htmlspecialchars($this->input->post('url_garuda', true)),
                    'url_schoolar' => htmlspecialchars($this->input->post('url_schoolar', true)),
                    'kategori_pj' => htmlspecialchars($this->input->post('kategori_pj', true)),
                    'email' => $email,
                    'sts' => 1,
                    'batch' => 5,
                    'aktif' => 1,
                    'upload_permohonan' => $upload1,
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Membuat Insentif Pengelola Jurnal $judul",
                    'email' => $email,
                    'date_created' => time()
                ];

                $this->db->insert('ins_pj', $data);
                $this->db->insert('occ_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('pj');
            }
        }
    }


    public function detailPj($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Pengelola Jurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_pj' => 'active',
            'pj' => $this->mp->getPj(),
            'd' => $this->mp->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/pj/d_pj', $data);
        $this->load->view('layouts/footer');
    }


    public function nonaktif($id)
    {

        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('ins_aji');


        $log = [
            'log' => "Menghapus ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('aji');
    }

    public function perbaikan()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Perbaikan Usulan Pengelola Jurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_pj' => 'active',
            'd' => $this->mp->getDetail($id),
            'pj' => $this->mp->getPj(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/pj/u_pj', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanGo()
    {
        $data = array(
            'title' => 'Perbaikan Usulan Pengelola Jurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_pj' => 'active',
            'pj' => $this->mp->getPj(),
            'akun' => $this->mu->getUser()
        );
        $id = htmlspecialchars($this->input->post('id', true));
        $nama_jurnal = htmlspecialchars($this->input->post('nama_jurnal', true));
        $url_jurnal = htmlspecialchars($this->input->post('url_jurnal', true));
        $peringkat_jurnal = htmlspecialchars($this->input->post('peringkat_jurnal', true));
        $url_issue = htmlspecialchars($this->input->post('url_issue', true));
        $vnt = htmlspecialchars($this->input->post('vnt', true));
        $kategori_pj = htmlspecialchars($this->input->post('kategori_pj', true));
        $url_garuda = htmlspecialchars($this->input->post('url_garuda', true));
        $url_schoolar = htmlspecialchars($this->input->post('url_schoolar', true));
        $email = $this->session->userdata('email');
        $sts = 1;
        $date_created = date('Y-m-d h:i:s');

        $upload_image = $_FILES['upload_permohonan']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_permohonan')) {
                $old_image = $data['ins_pj']['upload_permohonan'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('upload_permohonan', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $this->db->set('nama_jurnal', $nama_jurnal);
        $this->db->set('url_jurnal', $url_jurnal);
        $this->db->set('sts', $sts);
        $this->db->set('peringkat_jurnal', $peringkat_jurnal);
        $this->db->set('kategori_pj', $kategori_pj);
        $this->db->set('url_issue', $url_issue);
        $this->db->set('vnt', $vnt);
        $this->db->set('date_created', $date_created);
        $this->db->set('url_garuda', $url_garuda);
        $this->db->set('url_schoolar', $url_schoolar);
        $this->db->where('id', $id);
        $this->db->update('ins_pj');

        $log = [
            'log' => "Memngubah Data Insentif Pengolah Jurnal ID $id",
            'email' => $email,
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pj');
    }




    public function cetak()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Kwitansi',
            'pj' => $this->mp->getPj(),
            'd' => $this->mp->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('dosen/publis/cetak/c_pj', $data);
    }

    public function history($email)
    {

        $data['title'] = 'Dashboard | History Usulan Pengelola Jurnal';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['history'] = $this->db->get_where('ins_aji', ['email' => $email])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('user', ['email' => $email])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pj/history', $data);
        $this->load->view('templates/footer');
    }
}
