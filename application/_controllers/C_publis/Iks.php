<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Iks extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_iks', 'mik');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Keynote Speaker',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iks' => 'active',
            'iks' => $this->mik->getIks(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/iks/v_iks', $data);
        $this->load->view('layouts/footer');
    }

    public function createIks()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Keynote Speaker',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iks' => 'active',
            'iks' => $this->mik->getIks(),
            'akun' => $this->mu->getUser(),
        );

        $this->form_validation->set_rules('materi', 'materi', 'required|trim|is_unique[ins_ks.materi]', [
            'is_unique' => 'Materi Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'required|trim');
        $this->form_validation->set_rules('tgl_kegiatan', 'Tgl Pelaksanaan', 'required|trim');
        $this->form_validation->set_rules('tingkat_kegiatan', 'Tingkat Kegiatan', 'required|trim');
        $this->form_validation->set_rules('no_sertifikat', 'No Sertifikat', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/publis/iks/c_iks', $data);
            $this->load->view('layouts/footer');
        } else {
            $email = $this->session->userdata('email');
            $materi = htmlspecialchars($this->input->post('materi', true));
            $tingkat_kegiatan = htmlspecialchars($this->input->post('tingkat_kegiatan', true));
            $internasional = $this->db->query("SELECT * FROM ins_ks WHERE aktif=1 AND  tingkat_kegiatan='$tingkat_kegiatan' AND email='$email' AND batch=5")->num_rows();
            if ($internasional >= '1') {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Skim Anda sudah Melebihi Batas</div>');
                redirect('iks');
            } else {

                $upload_image = $_FILES['upload_bukti_kegiatan']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_bukti_kegiatan')) {
                        $old_image = $data['ins_ks']['upload_bukti_kegiatan'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload1 = $this->upload->data('file_name');
                        $this->db->set('upload_bukti_kegiatan', $upload1);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }

                $upload_image = $_FILES['upload_sertifikat']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_sertifikat')) {
                        $old_image = $data['ins_ks']['upload_sertifikat'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload2 = $this->upload->data('file_name');
                        $this->db->set('upload_sertifikat', $upload2);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }


                $upload_image = $_FILES['upload_undangan']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_undangan')) {
                        $old_image = $data['ins_ks']['upload_undangan'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload3 = $this->upload->data('file_name');
                        $this->db->set('upload_undangan', $upload3);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }


                $data = [
                    'materi' => htmlspecialchars($this->input->post('materi', true)),
                    'lokasi' => htmlspecialchars($this->input->post('lokasi', true)),
                    'tgl_kegiatan' => $this->input->post('tgl_kegiatan', true),
                    'tingkat_kegiatan' => htmlspecialchars($this->input->post('tingkat_kegiatan', true)),
                    'no_sertifikat' => $this->input->post('no_sertifikat', true),
                    'email' => $email,
                    'sts' => 1,
                    'aktif' => 1,
                    'batch' => 5,
                    'upload_bukti_kegiatan' => $upload1,
                    'upload_sertifikat' => $upload2,
                    'upload_undangan' => $upload3,
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Membuat Usulan Insentif Keynote Speaker $materi",
                    'email' => $email,
                    'date_created' => time()
                ];

                $this->db->insert('ins_ks', $data);
                $this->db->insert('occ_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('iks');
            }
        }
    }



    public function detailIks($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detata Usulan Insentif Keynote Speaker',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iks' => 'active',
            'iks' => $this->mik->getIks(),
            'd' => $this->mik->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/iks/d_iks', $data);
        $this->load->view('layouts/footer');
    }


    public function nonaktif($id)
    {

        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('ins_aji');


        $log = [
            'log' => "Menghapus ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('aji');
    }



    public function perbaikan()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detata Usulan Insentif Keynote Speaker',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iks' => 'active',
            'iks' => $this->mik->getIks(),
            'd' => $this->mik->getDetail($id),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/iks/u_iks', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanGo()
    {
        $data = array(
            'title' => 'Detata Usulan Insentif Keynote Speaker',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iks' => 'active',
            'iks' => $this->mik->getIks(),
            'akun' => $this->mu->getUser()

        );
        $this->form_validation->set_rules('materi', 'materi', 'required|trim|is_unique[ins_ks.materi]', [
            'is_unique' => 'Materi Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'required|trim');
        $this->form_validation->set_rules('tgl_pelaksanaan', 'Tgl Pelaksanaan', 'required|trim');
        $this->form_validation->set_rules('tingkat_kegiatan', 'Tingkat Kegiatan', 'required|trim');
        $this->form_validation->set_rules('no_sertifikat', 'No Sertifikat', 'required|trim');

        $email = $this->session->userdata('email');
        $tingkat_kegiatan = htmlspecialchars($this->input->post('tingkat_kegiatan', true));
        $internasional = $this->db->query("SELECT * FROM ins_ks WHERE aktif=1 AND  tingkat_kegiatan='$tingkat_kegiatan' AND email='$email' AND batch=3")->num_rows();

        $upload_image = $_FILES['upload_bukti_kegiatan']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_bukti_kegiatan')) {
                $old_image = $data['ins_ks']['upload_bukti_kegiatan'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('upload_bukti_kegiatan', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $upload_image = $_FILES['upload_sertifikat']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_sertifikat')) {
                $old_image = $data['ins_ks']['upload_sertifikat'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload2 = $this->upload->data('file_name');
                $this->db->set('upload_sertifikat', $upload2);
            } else {
                echo $this->upload->display_errors();
            }
        }


        $upload_image = $_FILES['upload_undangan']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_undangan')) {
                $old_image = $data['ins_ks']['upload_undangan'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload3 = $this->upload->data('file_name');
                $this->db->set('upload_undangan', $upload3);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $id = $this->input->post('id', true);
        $no_sertifikat = $this->input->post('no_sertifikat', true);
        $email = $this->session->userdata('email');
        $materi = htmlspecialchars($this->input->post('materi', true));
        $lokasi = htmlspecialchars($this->input->post('lokasi', true));
        $tgl_kegiatan = htmlspecialchars($this->input->post('tgl_kegiatan', true));
        $tingkat_kegiatan = htmlspecialchars($this->input->post('tingkat_kegiatan', true));
        $sts = 1;
        $date_created = date('Y-m-d h:i:s');



        $this->db->set('materi', $materi);
        $this->db->set('lokasi', $lokasi);
        $this->db->set('tgl_kegiatan', $tgl_kegiatan);
        $this->db->set('tingkat_kegiatan', $tingkat_kegiatan);
        $this->db->set('sts', $sts);
        $this->db->set('no_sertifikat', $no_sertifikat);
        $this->db->set('upload_bukti_kegiatan', $upload1);
        $this->db->set('upload_sertifikat', $upload2);
        $this->db->set('date_created', $date_created);
        $this->db->set('upload_undangan', $upload2);
        $this->db->where('id', $id);
        $this->db->update('ins_ks');

        $log = [
            'log' => "Memngubah Usulan Insentif Keynote Speaker ID $id",
            'email' => $email,
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('iks');
    }

    public function cetak($id)
    {
        $data['title'] = 'Cetak Kwitansi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['iden'] = $this->db->get_where('identitas', ['email' => $this->session->userdata('email')])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('ins_aji', ['id' => $id])->row_array();
        $this->load->view('cetak/c_iaj', $data);
    }

    public function history($email)
    {

        $data['title'] = 'Dashboard | History Usulan Insentif Artikel Dijurnal';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['history'] = $this->db->get_where('ins_aji', ['email' => $email])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('user', ['email' => $email])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('iaj/history', $data);
        $this->load->view('templates/footer');
    }
}
