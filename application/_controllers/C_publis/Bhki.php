<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bhki extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_bhki', 'mh');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan Hki/Paten',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bhki' => 'active',
            'bhki' => $this->mh->getBhki(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/bhki/v_bhki', $data);
        $this->load->view('layouts/footer');
    }

    public function createbhki()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan Hki/Paten',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bhki' => 'active',
            'bhki' => $this->mh->getBhki(),
            'akun' => $this->mu->getUser()
        );


        $this->form_validation->set_rules('judul_hki', 'Judul HKI', 'required|trim|is_unique[ins_bhki.judul_hki]', [
            'is_unique' => 'Judul HKI Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('no_reg', 'Nomor Registrasi', 'required|trim|is_unique[ins_bhki.no_reg]', [
            'is_unique' => 'Nomor Registrasi HKI Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('tgl_pengajuan_hki', 'Tanggal Pengajuan HKI', 'required|trim');
        $this->form_validation->set_rules('jenis_hki', 'Jenis HKI', 'required|trim');
        $this->form_validation->set_rules('pemegang_hki', 'Pemegang HKI', 'required|trim');
        $this->form_validation->set_rules('pertanyaan1', 'Pertanyaan', 'required|trim');
        $this->form_validation->set_rules('kategori_hki', 'Kategori HKI', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/publis/bhki/c_bhki', $data);
            $this->load->view('layouts/footer');
        } else {
            $email = $this->session->userdata('email');
            $kategori_hki = htmlspecialchars($this->input->post('kategori_hki'));
            $internasional = $this->db->query("SELECT * FROM ins_bhki WHERE aktif=1 AND email='$email' AND batch=5")->num_rows();
            if ($internasional >= '1') {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Pengajuan Bantuan HKI Anda sudah Melebihi Batas</div>');
                redirect('bhki');
            } else {

                $upload_image = $_FILES['upload_file']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_file')) {
                        $old_image = $data['ins_bhki']['upload_file'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $sertifikat = $this->upload->data('file_name');
                        $this->db->set('upload_file', $sertifikat);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }
                $jenis_hki = htmlspecialchars($this->input->post('jenis_hki', true));
                if ($jenis_hki == "HAK CIPTA UMUM") {
                    $pagu = "200000";
                } elseif ($jenis_hki == "HAK CIPTA UMKM") {
                    $pagu = "200000";
                } elseif ($jenis_hki == "HAK CIPTA KHUSUS UMUM(JENIS BASIS DATA, KOMPILASI CIPTAAN/DATA, PERMAINAN VIDEO & PROGRAM KOMPUTER)") {
                    $pagu = "600000";
                } elseif ($jenis_hki == "HAK CIPTA KHUSUS UMKM(JENIS BASIS DATA, KOMPILASI CIPTAAN/DATA, PERMAINAN VIDEO & PROGRAM KOMPUTER)") {
                    $pagu = "300000";
                } elseif ($jenis_hki == "PATEN UMUM") {
                    $pagu = "4250000";
                } elseif ($jenis_hki == "PATEN UMKM") {
                    $pagu = "3350000";
                } elseif ($jenis_hki == "PATEN SEDERHANA UMUM") {
                    $pagu = "1300000";
                } elseif ($jenis_hki == "PATEN SEDERHANA UMKM") {
                    $pagu = "700000";
                } elseif ($jenis_hki == "MEREK") {
                    $pagu = "1800000";
                } elseif ($jenis_hki == "DESAIN INDUSTRI") {
                    $pagu = "250000";
                } elseif ($jenis_hki == "DESAIN INDUSTRI UMUM (SATU DESAIN INDUSTRI)") {
                    $pagu = "800000";
                } elseif ($jenis_hki == "DESAIN INDUSTRI UMKM (SATU DESAIN INDUSTRI)") {
                    $pagu = "250000";
                } elseif ($jenis_hki == "DESAIN INDUSTRI (SET) UMUM") {
                    $pagu = "1250000";
                } elseif ($jenis_hki == "DESAIN INDUSTRI (SET) UMKM") {
                    $pagu = "550000";
                } elseif ($jenis_hki == "RAHASIA DAGANG UMUM (PENCATAAN LISENSI)") {
                    $pagu = "250000";
                } elseif ($jenis_hki == "RAHASIA DAGANG UMKM (PENCATAAN LISENSI)") {
                    $pagu = "150000";
                } elseif ($jenis_hki == "RAHASIA DAGANG UMUM (PENGALIH RAHASIA DAGANG)") {
                    $pagu = "400000";
                } elseif ($jenis_hki == "RAHASIA DAGANG UMKM (PENGALIH RAHASIA DAGANG)") {
                    $pagu = "200000";
                } else {
                    $pagu = "0";
                }

                $data = [
                    'judul_hki' =>  htmlspecialchars($this->input->post('judul_hki', true)),
                    'no_reg' =>  htmlspecialchars($this->input->post('no_reg', true)),
                    'tgl_pengajuan_hki' => $this->input->post('tgl_pengajuan_hki', true),
                    'jenis_hki' =>  $jenis_hki,
                    'pertanyaan1' =>  htmlspecialchars($this->input->post('pertanyaan1', true)),
                    'pemegang_hki' =>  htmlspecialchars($this->input->post('pemegang_hki', true)),
                    'kategori_hki' =>  htmlspecialchars($this->input->post('kategori_hki', true)),
                    'email' => $email,
                    'sts' => 3,
                    'aktif' => 1,
                    'batch' => 5,
                    'pagu' => $pagu,
                    'upload_file' => $sertifikat,
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Membuat Bantuan HKI",
                    'email' => $email,
                    'date_created' => time()
                ];

                $this->db->insert('ins_bhki', $data);
                $this->db->insert('occ_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('bhki');
            }
        }
    }

    public function detailBhki($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Bantuan Hki/Paten',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bhki' => 'active',
            'bhki' => $this->mh->getBhki(),
            'd' => $this->mh->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/bhki/d_bhki', $data);
        $this->load->view('layouts/footer');
    }


    public function nonaktif($id)
    {

        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('ins_aji');


        $log = [
            'log' => "Menghapus ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('aji');
    }



    public function perbaikan()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Perbaikan Usulan Bantuan HKI',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_aji' => 'active',
            'd' => $this->mh->getDetail($id),
            'bhki' => $this->mh->getBhki(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/bhki/u_bhki', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanGo()
    {

        $data = array(
            'title' => 'Form Usulan Insentif Artikel Dijurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bhki' => 'active',
            'hki' => $this->mh->getBhki(),
            'akun' => $this->mu->getUser()
        );
        $email = $this->session->userdata('email');

        $upload_image = $_FILES['upload_file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_file')) {
                $old_image = $data['ins_bhki']['upload_file'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $sertifikat = $this->upload->data('file_name');
                $this->db->set('upload_file', $sertifikat);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $jenis_hki = htmlspecialchars($this->input->post('jenis_hki', true));
        if ($jenis_hki == "HAK CIPTA UMUM") {
            $pagu = "400000";
        } elseif ($jenis_hki == "HAK CIPTA UMKM") {
            $pagu = "200000";
        } elseif ($jenis_hki == "HAK CIPTA KHUSUS UMUM(JENIS BASIS DATA, KOMPILASI CIPTAAN/DATA, PERMAINAN VIDEO & PROGRAM KOMPUTER)") {
            $pagu = "600000";
        } elseif ($jenis_hki == "HAK CIPTA KHUSUS UMKM(JENIS BASIS DATA, KOMPILASI CIPTAAN/DATA, PERMAINAN VIDEO & PROGRAM KOMPUTER)") {
            $pagu = "300000";
        } elseif ($jenis_hki == "PATEN UMUM") {
            $pagu = "4250000";
        } elseif ($jenis_hki == "PATEN UMKM") {
            $pagu = "3350000";
        } elseif ($jenis_hki == "PATEN SEDERHANA UMUM") {
            $pagu = "1300000";
        } elseif ($jenis_hki == "PATEN SEDERHANA UMKM") {
            $pagu = "700000";
        } elseif ($jenis_hki == "MEREK") {
            $pagu = "1800000";
        } elseif ($jenis_hki == "DESAIN INDUSTRI") {
            $pagu = "250000";
        } elseif ($jenis_hki == "DESAIN INDUSTRI UMUM (SATU DESAIN INDUSTRI)") {
            $pagu = "800000";
        } elseif ($jenis_hki == "DESAIN INDUSTRI UMKM (SATU DESAIN INDUSTRI)") {
            $pagu = "250000";
        } elseif ($jenis_hki == "DESAIN INDUSTRI (SET) UMUM") {
            $pagu = "1250000";
        } elseif ($jenis_hki == "DESAIN INDUSTRI (SET) UMKM") {
            $pagu = "550000";
        } elseif ($jenis_hki == "RAHASIA DAGANG UMUM (PENCATAAN LISENSI)") {
            $pagu = "250000";
        } elseif ($jenis_hki == "RAHASIA DAGANG UMKM (PENCATAAN LISENSI)") {
            $pagu = "150000";
        } elseif ($jenis_hki == "RAHASIA DAGANG UMUM (PENGALIH RAHASIA DAGANG)") {
            $pagu = "400000";
        } elseif ($jenis_hki == "RAHASIA DAGANG UMKM (PENGALIH RAHASIA DAGANG)") {
            $pagu = "200000";
        } else {
            $pagu = "0";
        }

        $id = htmlspecialchars($this->input->post('id', true));
        $judul_hki =  htmlspecialchars($this->input->post('judul_hki', true));
        $no_reg =  htmlspecialchars($this->input->post('no_reg', true));
        $tgl_pengajuan_hki = $this->input->post('tgl_pengajuan_hki', true);
        $jenis_hki =  $jenis_hki;
        $pertanyaan1 =  htmlspecialchars($this->input->post('pertanyaan1', true));
        $pemegang_hki =  htmlspecialchars($this->input->post('pemegang_hki', true));
        $kategori_hki =  htmlspecialchars($this->input->post('kategori_hki', true));
        $sts = 3;
        $aktif = 1;
        $batch = 5;
        $pagu = $pagu;
        $upload_file = $sertifikat;

        $this->db->set('judul_hki', $judul_hki);
        $this->db->set('no_reg', $no_reg);
        $this->db->set('tgl_pengajuan_hki', $tgl_pengajuan_hki);
        $this->db->set('jenis_hki', $jenis_hki);
        $this->db->set('kategori_hki', $kategori_hki);
        $this->db->set('pemegang_hki', $pemegang_hki);
        $this->db->set('pertanyaan1', $pertanyaan1);
        $this->db->set('upload_file', $upload_file);
        $this->db->set('sts', $sts);
        $this->db->set('aktif', $aktif);
        $this->db->set('batch', $batch);
        $this->db->where('id', $id);
        $this->db->update('ins_bhki');

        $log = [
            'log' => "Memngubah Data Bantuan HKI ID $id",
            'email' => $email,
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('bhki');
    }

    public function cetak()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Bantuan HKI',
            'bhki' => $this->mh->getBhki(),
            'd' => $this->mh->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('dosen/publis/cetak/c_bhki', $data);
    }

    public function history($email)
    {

        $data['title'] = 'Dashboard | History Usulan Insentif Artikel Dijurnal';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['history'] = $this->db->get_where('ins_aji', ['email' => $email])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('user', ['email' => $email])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('iaj/history', $data);
        $this->load->view('templates/footer');
    }
}
