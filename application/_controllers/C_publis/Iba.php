<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Iba extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_iba', 'mi');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Buku Ajar',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iba' => 'active',
            'iba' => $this->mi->getIba(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/iba/v_iba', $data);
        $this->load->view('layouts/footer');
    }

    public function createIba()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Buku Ajar',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iba' => 'active',
            'iba' => $this->mi->getIba(),
            'akun' => $this->mu->getUser()
        );

        $this->form_validation->set_rules('judul_buku', 'Judul Buku', 'required|trim|is_unique[ins_iba.judul_buku]', [
            'is_unique' => 'Judul Buku Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('pertanyaan1', 'Kategori', 'required|trim');
        $this->form_validation->set_rules('kategori_ba', 'Kategori', 'required|trim');


        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/publis/iba/c_iba', $data);
            $this->load->view('layouts/footer');
        } else {
            $email = $this->session->userdata('email');
            $judul = htmlspecialchars($this->input->post('judul_buku', true));
            $kategori_ba = htmlspecialchars($this->input->post('kategori_ba'));
            $jenis_buku = htmlspecialchars($this->input->post('jenis_buku', true));
            // periksa pengajuan Buku

            $internasional = $this->db->query("SELECT * FROM ins_iba WHERE jenis_buku='$jenis_buku' AND aktif=1 AND email='$email' AND batch=5")->num_rows();
            if ($internasional >= '1') {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Skim Anda sudah Melebihi Batas</div>');
                redirect('iba');
            } else {
                $upload_image = $_FILES['upload_cover']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_cover')) {
                        $old_image = $data['ins_iba']['upload_cover'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload1 = $this->upload->data('file_name');
                        $this->db->set('upload_cover', $upload1);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }

                $upload_image = $_FILES['upload_buku']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_buku')) {
                        $old_image = $data['ins_iba']['upload_buku'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload2 = $this->upload->data('file_name');
                        $this->db->set('upload_buku', $upload2);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }


                $data = [
                    'jenis_buku' => $jenis_buku,
                    'judul_buku' => htmlspecialchars($this->input->post('judul_buku', true)),
                    'kontribusi_pengusul' => htmlspecialchars($this->input->post('kontribusi_pengusul', true)),
                    'pertanyaan1' => htmlspecialchars($this->input->post('pertanyaan1', true)),
                    'pertanyaan2' => htmlspecialchars($this->input->post('pertanyaan2', true)),
                    'pertanyaan3' => htmlspecialchars($this->input->post('pertanyaan3', true)),
                    'kategori_ba' => $kategori_ba,
                    'penerbit' => htmlspecialchars($this->input->post('penerbit', true)),
                    'isbn' => htmlspecialchars($this->input->post('isbn', true)),
                    'web_penerbit' => htmlspecialchars($this->input->post('web_penerbit', true)),
                    'jumlah_halaman' => htmlspecialchars($this->input->post('jumlah_halaman', true)),
                    'tahun_terbit' => htmlspecialchars($this->input->post('tahun_terbit', true)),
                    'email' => $email,
                    'sts' => 1,
                    'aktif' => 1,
                    'batch' => 5,
                    'upload_cover' => $upload1,
                    'upload_buku' => $upload2,
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Membuat Insentif Buku Ajar $judul",
                    'email' => $email,
                    'date_created' => time()
                ];


                $this->db->insert('ins_iba', $data);
                $this->db->insert('occ_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('iba');
            }
        }
    }



    public function detailIba($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Insentif Buku Ajar',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iba' => 'active',
            'iba' => $this->mi->getIba(),
            'd' => $this->mi->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/iba/d_iba', $data);
        $this->load->view('layouts/footer');
    }


    public function nonaktif($id)
    {

        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('ins_aji');


        $log = [
            'log' => "Menghapus ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('aji');
    }



    public function perbaikan()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Usulan Insentif Artikel Dijurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iba' => 'active',
            'd' => $this->mi->getDetail($id),
            'iba' => $this->mi->getIba(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/iba/u_iba', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanGo()
    {
        $data = array(
            'title' => 'Form Usulan Insentif Artikel Dijurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_iba' => 'active',
            'iba' => $this->mi->getIba(),
            'akun' => $this->mu->getUser()

        );


        $email = $this->session->userdata('email');
        $id = htmlspecialchars($this->input->post('id', true));
        $jenis_buku = htmlspecialchars($this->input->post('jenis_buku', true));
        $judul_buku = htmlspecialchars($this->input->post('judul_buku', true));
        $kontribusi_pengusul = htmlspecialchars($this->input->post('kontribusi_pengusul', true));
        $pertanyaan1 = htmlspecialchars($this->input->post('pertanyaan1', true));
        $pertanyaan2 = htmlspecialchars($this->input->post('pertanyaan2', true));
        $pertanyaan3 = htmlspecialchars($this->input->post('pertanyaan3', true));
        $pertanyaan4 = htmlspecialchars($this->input->post('pertanyaan4', true));
        $penerbit = htmlspecialchars($this->input->post('penerbit', true));
        $isbn = htmlspecialchars($this->input->post('isbn', true));
        $web_penerbit = htmlspecialchars($this->input->post('web_penerbit', true));
        $jumlah_halaman = htmlspecialchars($this->input->post('jumlah_halaman', true));
        $harga_jual = htmlspecialchars($this->input->post('harga_jual', true));
        $tahun_terbit = htmlspecialchars($this->input->post('tahun_terbit', true));
        $date_created = date('Y-m-d h:i:s');
        $sts = 1;

        $kategori_ba = htmlspecialchars($this->input->post('kategori_ba'));

        $upload_image = $_FILES['upload_cover']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_cover')) {
                $old_image = $data['ins_iba']['upload_cover'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('upload_cover', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $upload_image = $_FILES['upload_buku']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_buku')) {
                $old_image = $data['ins_iba']['upload_buku'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload2 = $this->upload->data('file_name');
                $this->db->set('upload_buku', $upload2);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->set('sts', $sts);
        $this->db->set('judul_buku', $judul_buku);
        $this->db->set('jenis_buku', $jenis_buku);
        $this->db->set('kontribusi_pengusul', $kontribusi_pengusul);
        $this->db->set('pertanyaan1', $pertanyaan1);
        $this->db->set('pertanyaan2', $pertanyaan2);
        $this->db->set('pertanyaan3', $pertanyaan3);
        $this->db->set('pertanyaan4', $pertanyaan4);
        $this->db->set('penerbit', $penerbit);
        $this->db->set('kategori_ba', $kategori_ba);
        $this->db->set('date_created', $date_created);
        $this->db->set('isbn', $isbn);
        $this->db->set('web_penerbit', $web_penerbit);
        $this->db->set('jumlah_halaman', $jumlah_halaman);
        $this->db->set('harga_jual', $harga_jual);
        $this->db->set('tahun_terbit', $tahun_terbit);
        $this->db->where('id', $id);
        $this->db->update('ins_iba');

        $log = [
            'log' => "Perbaikan Data Insentif Buku Ajar ID $id",
            'email' => $email,
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('iba');
    }

    public function cetak($id)
    {
        $data['title'] = 'Cetak Kwitansi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['iden'] = $this->db->get_where('identitas', ['email' => $this->session->userdata('email')])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('ins_aji', ['id' => $id])->row_array();
        $this->load->view('cetak/c_iaj', $data);
    }

    public function history($email)
    {

        $data['title'] = 'Dashboard | History Usulan Insentif Artikel Dijurnal';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['history'] = $this->db->get_where('ins_aji', ['email' => $email])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('user', ['email' => $email])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('iaj/history', $data);
        $this->load->view('templates/footer');
    }
}
