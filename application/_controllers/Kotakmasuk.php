<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kotakmasuk extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('Model_kotakmasuk', 'mpp');
    }

    public function index()
    {
        $data = array(
            'title' => 'Kotak Masuk',
            'active_menu_kotak_masuk' => 'active',
            'v' => $this->mpp->getKotakmasuk()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('kotakmasuk/index', $data);
        $this->load->view('layouts/footer');
    }
}
