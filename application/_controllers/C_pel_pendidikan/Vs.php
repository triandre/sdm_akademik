<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pel_pendidikan/Model_vs', 'mv');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Visiting Scientist',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_vs' => 'active',
            'v' => $this->mv->getVs(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/vs/v_vs', $data);
        $this->load->view('layouts/footer');
    }

    public function createVs()
    {
        $data = array(
            'title' => 'Data Visiting Scientist',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_vs' => 'active',
            'v' => $this->mv->getVs(),
            'akun' => $this->mu->getUser(),
        );


        $this->form_validation->set_rules('aktivitas', 'aktivitas', 'required|trim');
        $this->form_validation->set_rules('kategori', 'Kategori', 'required|trim');
        $this->form_validation->set_rules('pt', 'Perguruan Tinggi', 'required|trim');
        $this->form_validation->set_rules('lama_kgt', 'Lama Kegiatan', 'required|trim');
        $this->form_validation->set_rules('kategori_kgt', 'Kategori Kegiatan', 'required|trim');
        $this->form_validation->set_rules('kgt_penting', 'Kegiatan Penting', 'required|trim');
        $this->form_validation->set_rules('tgl_pelaksanaan', 'Tanggal Pelaksanaan', 'required|trim');
        $this->form_validation->set_rules('no_sk', 'No. SK', 'required|trim');
        $this->form_validation->set_rules('tgl_sk', 'Tanggal SK', 'required|trim');


        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/pel_pendidikan/vs/c_vs', $data);
            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();
            $reff = str_replace('-', '', $id);
            $nidn = $this->session->userdata('nidn');
            //UPLOAD
            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/vs/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['occ_vs']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/vs/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $data = array(
                'reff' => $reff,
                'nidn' => $nidn,
                'aktivitas' => htmlspecialchars($this->input->post('aktivitas', true)),
                'kategori' => htmlspecialchars($this->input->post('kategori', true)),
                'pt' => htmlspecialchars($this->input->post('pt', true)),
                'lama_kgt' => htmlspecialchars($this->input->post('lama_kgt', true)),
                'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
                'kgt_penting' => htmlspecialchars($this->input->post('kgt_penting', true)),
                'tgl_pelaksanaan' => htmlspecialchars($this->input->post('tgl_pelaksanaan', true)),
                'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
                'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );
            $result = $this->mv->storeVs($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('dosenVs');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createVs');
            }
        }
    }

    public function detailVs()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Visiting Scientist',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_vs' => 'active',
            'v' => $this->mv->getVs(),
            'd' => $this->mv->getDetail($reff)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/vs/d_vs', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanVs()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Visiting Scientist',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_vs' => 'active',
            'v' => $this->mv->getVs(),
            'd' => $this->mv->getDetail($reff)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/vs/u_vs', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanVsGo()
    {

        $data = array(
            'title' => 'Data Visiting Scientist',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_vs' => 'active',
            'v' => $this->mv->getVs(),
        );

        $reff = htmlspecialchars($this->input->post('reff', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/vs/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_vs']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/vs/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'aktivitas' => htmlspecialchars($this->input->post('aktivitas', true)),
            'kategori' => htmlspecialchars($this->input->post('kategori', true)),
            'pt' => htmlspecialchars($this->input->post('pt', true)),
            'lama_kgt' => htmlspecialchars($this->input->post('lama_kgt', true)),
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'kgt_penting' => htmlspecialchars($this->input->post('kgt_penting', true)),
            'tgl_pelaksanaan' => htmlspecialchars($this->input->post('tgl_pelaksanaan', true)),
            'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
            'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
            'file' => $upload1,
            'aktif' => 1
        );


        $result = $this->mv->updateVs($reff, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('dosenVs');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('vs/perbaikanVsGo/' . $reff);
        }
    }

    public function hapusVs($reff)
    {
        $reff = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('reff', $reff);
        $this->db->update('occ_vs');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vs');
    }
}
