<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_valDataPribadi extends CI_Model
{

    public function viewDiajukan()
    {
        $kode1 = "ADPROFIL";
        $kode2 = "ADKDD";
        $kode3 = "ADKLG";
        $kode4 = "ADKTAM";
        $kode5 = "ADALAMAT";
        $kode6 = "ADKPG";
        $kode7 = "ADLLN";
        $viewDiajukan = "SELECT occ_pdd.reff, 
        occ_user.nidn,
        occ_user.name,
        occ_pdd.tgl_ajuan,
        occ_pdd.sts,
        occ_pdd.kode_ajuan,
        occ_pdd.jenis_ajuan
        FROM occ_pdd
        LEFT JOIN occ_user ON occ_user.nidn = occ_pdd.nidn 
        WHERE occ_pdd.sts=1 AND occ_pdd.kode_ajuan IN ('$kode1', '$kode2', '$kode3', '$kode4', '$kode5', '$kode6', '$kode7')";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDitolak()
    {
        $kode1 = "ADPROFIL";
        $kode2 = "ADKDD";
        $kode3 = "ADKLG";
        $kode4 = "ADKTAM";
        $kode5 = "ADALAMAT";
        $kode6 = "ADKPG";
        $kode7 = "ADLLN";
        $viewDiajukan = "SELECT occ_pdd.reff, 
        occ_user.nidn,
        occ_user.name,
        occ_pdd.tgl_ajuan,
        occ_pdd.sts,
        occ_pdd.kode_ajuan,
        occ_pdd.tgl_verifikasi,
        occ_pdd.komentar,
        occ_pdd.jenis_ajuan
        FROM occ_pdd
        LEFT JOIN occ_user ON occ_user.nidn = occ_pdd.nidn 
        WHERE occ_pdd.sts=3 AND occ_pdd.kode_ajuan IN ('$kode1', '$kode2', '$kode3', '$kode4', '$kode5', '$kode6', '$kode7')";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDisetujui()
    {
        $kode1 = "ADPROFIL";
        $kode2 = "ADKDD";
        $kode3 = "ADKLG";
        $kode4 = "ADKTAM";
        $kode5 = "ADALAMAT";
        $kode6 = "ADKPG";
        $kode7 = "ADLLN";
        $viewDiajukan = "SELECT occ_pdd.reff, 
        occ_user.nidn,
        occ_user.name,
        occ_pdd.tgl_ajuan,
        occ_pdd.sts,
        occ_pdd.tgl_verifikasi,
        occ_pdd.kode_ajuan,
        occ_pdd.komentar,
        occ_pdd.jenis_ajuan
        FROM occ_pdd
        LEFT JOIN occ_user ON occ_user.nidn = occ_pdd.nidn 
        WHERE occ_pdd.sts=2 AND occ_pdd.kode_ajuan IN ('$kode1', '$kode2', '$kode3', '$kode4', '$kode5', '$kode6', '$kode7')";
        return $this->db->query($viewDiajukan)->result_array();
    }
    public function detailProfilLama($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_profil');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 4);
        $this->db->order_by('id_profil', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailProfil($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_profil');
        $this->db->where('reff_profil', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailKeluarga($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_keluarga');
        $this->db->where('reff_keluarga', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function detailKeluargaLama($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_keluarga');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 4);
        $this->db->order_by('id_keluarga', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailKependudukanLama($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_kependudukan');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 4);
        $this->db->order_by('id_kependudukan', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailKependudukan($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_kependudukan');
        $this->db->where('reff_kependudukan', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailKeanggotaanLama($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_muhammadiyah');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 4);
        $this->db->order_by('id_km', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailKeanggotaan($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_muhammadiyah');
        $this->db->where('reff_km', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailKepegawaian($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_kepegawaian a');
        $this->db->join('master_prodi b', 'b.kode_prodi = a.kode_prodi');
        $this->db->where('a.reff_kepegawaian', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailKepegawaianLama($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_kepegawaian a');
        $this->db->join('master_prodi b', 'b.kode_prodi = a.kode_prodi');
        $this->db->where('a.nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 4);
        $this->db->order_by('id_kepegawaian', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailAlamat($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_alamat');
        $this->db->where('reff_alamat', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailAlamatLama($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_alamat');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 4);
        $this->db->order_by('id_alamat', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailLainlain($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_lain');
        $this->db->where('reff_lain', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function detailLainlainLama($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_lain');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 4);
        $this->db->order_by('id_lain', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
