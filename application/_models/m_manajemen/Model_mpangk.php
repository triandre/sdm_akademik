<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mpangk extends CI_Model
{
    public function getview()
    {

        $this->db->select('*');
        $this->db->from('mm_pangkat');
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function save_jaf($data)
    {
        $query = $this->db->insert('mm_pangkat', $data);
        return $query;
    }

    public function detail_jaf($id_jfungsional)
    {
        $this->db->select('*');
        $this->db->from('mm_pangkat');
        $this->db->where('id_jfungsional', $id_jfungsional);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function edit_jaf($id_jfungsional, $data)
    {
        $this->db->where(array('id_jfungsional' => $id_jfungsional));
        $res = $this->db->update('mm_pangkat', $data);
        return $res;
    }
}

/* End of file ModelName.php */
