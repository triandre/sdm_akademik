<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_maik extends CI_Model
{
    public function getview()
    {

        $this->db->select('*');
        $this->db->from('mm_aik');
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function save_aik($data)
    {
        $query = $this->db->insert('mm_aik', $data);
        return $query;
    }

    public function detail_aik($id_kriteria)
    {
        $this->db->select('*');
        $this->db->from('mm_aik');
        $this->db->where('id_kriteria', $id_kriteria);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function edit_aik($id_kriteria, $data)
    {
        $this->db->where(array('id_kriteria' => $id_kriteria));
        $res = $this->db->update('mm_aik', $data);
        return $res;
    }
}

/* End of file ModelName.php */
