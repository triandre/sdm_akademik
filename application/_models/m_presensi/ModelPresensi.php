<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelPresensi extends CI_Model
{
    public function getDosen()
    {
        $this->db->select('*');
        $this->db->from('occ_user ');
        $this->db->where('role_id', 2);
        $this->db->where('is_active', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPresensi()
    {
        $email = $this->session->userdata('email');
        $bulan = date('m');
        $tahun = date('Y');
        $this->db->select('*');
        $this->db->from('occ_presensi_dosen a');
        $this->db->join('occ_user b', 'b.nidn = a.nidn');
        $this->db->where('a.bulan', $bulan);
        $this->db->where('a.tahun', $tahun);
        $this->db->where('a.email_created', $email);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDetail($id_reff)
    {
        $this->db->select('*');
        $this->db->from('occ_presensi_dosen');
        $this->db->where('id_reff', $id_reff);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function ubahPresensiGo($id_reff, $data)
    {
        $this->db->where(array('id_reff' => $id_reff));
        $res = $this->db->update('occ_presensi_dosen', $data);
        return $res;
    }

    public function __getPresensiAdmin()
    {

        $bulan = date('m');
        $tahun = date('Y');
        $this->db->select('*');
        $this->db->from('occ_presensi_dosen a');
        $this->db->join('occ_user b', 'b.nidn = a.nidn');
        $this->db->where('a.bulan', $bulan);
        $this->db->where('a.tahun', $tahun);
        $this->db->group_by('a.nidn');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPresensiAdmin()
    {

        $bulan = date('m');
        $tahun = date('Y');
        $total_bersih = "SELECT occ_user.nidn, occ_user.name, occ_user.payrol, occ_user.role_id, occ_user.sarana_dakwah, occ_user.pinjaman_lkk, 
        sum(occ_presensi_dosen.total_tarif) as jumlah_sementara,
        sum(occ_presensi_dosen.total_tarif)+sum(occ_presensi_dosen.transport) as total FROM `occ_presensi_dosen` 
        LEFT JOIN occ_user ON occ_user.nidn = occ_presensi_dosen.nidn 
        WHERE occ_presensi_dosen.bulan='$bulan' AND occ_presensi_dosen.tahun='$tahun' AND occ_user.role_id='2'  GROUP BY nidn";
        return $this->db->query($total_bersih)->result_array();
    }

    public function getPresensiAdmin_dt()
    {

        $bulan = date('m');
        $tahun = date('Y');
        $total_bersih = "SELECT occ_user.nidn, occ_user.name, occ_user.payrol, occ_user.role_id, sum(occ_presensi_dosen.total_tarif)-(sarana_dakwah+pinjaman_lkk) as total FROM `occ_presensi_dosen` 
        LEFT JOIN occ_user ON occ_user.nidn = occ_presensi_dosen.nidn 
        WHERE occ_presensi_dosen.bulan='$bulan' AND occ_presensi_dosen.tahun='$tahun' AND occ_user.role_id='10' GROUP BY nidn";
        return $this->db->query($total_bersih)->result_array();
    }

    public function getDetailPresensi($nidn)
    {

        $bulan = date('m');
        $tahun = date('Y');
        $this->db->select('*');
        $this->db->from('occ_presensi_dosen a');
        $this->db->join('occ_user b', 'b.nidn = a.nidn');
        $this->db->where('a.bulan', $bulan);
        $this->db->where('a.tahun', $tahun);
        $this->db->where('a.nidn', $nidn);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDetailDosen($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getJumlahSks($nidn)
    {

        $bulan = date('m');
        $tahun = date('Y');
        $jumlah_sks = "SELECT SUM(sks) AS jsks FROM occ_presensi_dosen
        WHERE bulan='$bulan' AND tahun='$tahun' AND nidn='$nidn'";
        return $this->db->query($jumlah_sks)->row_array();
    }

    public function getJumlahKehadiran($nidn)
    {

        $bulan = date('m');
        $tahun = date('Y');
        $jumlah_sks = "SELECT SUM(jumlah_kehadiran) AS jkehadiran FROM occ_presensi_dosen
        WHERE bulan='$bulan' AND tahun='$tahun' AND nidn='$nidn'";
        return $this->db->query($jumlah_sks)->row_array();
    }

    public function getJumlahTansport($nidn)
    {

        $bulan = date('m');
        $tahun = date('Y');
        $jumlah_sks = "SELECT SUM(transport) AS jtransport FROM occ_presensi_dosen
        WHERE bulan='$bulan' AND tahun='$tahun' AND nidn='$nidn'";
        return $this->db->query($jumlah_sks)->row_array();
    }

    public function getJumlahTarif($nidn)
    {
        $bulan = date('m');
        $tahun = date('Y');
        $jumlah_sks = "SELECT SUM(tarif) AS jtarif FROM occ_presensi_dosen
        WHERE bulan='$bulan' AND tahun='$tahun' AND nidn='$nidn'";
        return $this->db->query($jumlah_sks)->row_array();
    }

    public function getJumlahTotalTarif($nidn)
    {
        $bulan = date('m');
        $tahun = date('Y');
        $jumlah_sks = "SELECT SUM(total_tarif) + SUM(transport) AS jtotal_tarif FROM occ_presensi_dosen
        WHERE bulan='$bulan' AND tahun='$tahun' AND nidn='$nidn'";
        return $this->db->query($jumlah_sks)->row_array();
    }


    public function getPotongan($nidn)
    {

        $pot = "SELECT nidn,name, tarif, sarana_dakwah, pinjaman_lkk, madani, pph_psl, zis FROM occ_user
        WHERE nidn='$nidn'";
        return $this->db->query($pot)->row_array();
    }


    public function getTarifDosen2($id_reff)
    {
        $this->db->select('*');
        $this->db->from('occ_presensi_dosen a');
        $this->db->join('occ_user b', 'b.nidn = a.nidn');
        $this->db->where('id_reff', $id_reff);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getTarifDosen()
    {
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('role_id', 2);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function editTarifDosen($nidn)
    {
        $tarif = "SELECT occ_user.nidn,
        occ_user.name,
        mm_tarif_dosen.tarif 
        FROM occ_user
        LEFT JOIN  mm_tarif_dosen ON mm_tarif_dosen.nidn = occ_user.nidn
        WHERE occ_user.nidn='$nidn'";
        return $this->db->query($tarif)->row_array();
    }

    public function editTarifDosen2($nidn)
    {
        $tarif = "SELECT nidn,name, tarif, payrol, sarana_dakwah, pinjaman_lkk, madani, pph_psl, zis FROM occ_user
        WHERE nidn='$nidn'";
        return $this->db->query($tarif)->row_array();
    }

    public function getLapPresensiAdmin($bulan, $tahun, $email_created)
    {
        $pot = 0.025;
        $total_bersih = "SELECT occ_user.nidn, occ_user.name, occ_user.payrol, occ_user.role_id, occ_user.sarana_dakwah,
        sum(occ_presensi_dosen.transport) as jumlah_transport,
        sum(occ_presensi_dosen.total_tarif) as jumlah_sementara,
        sum(occ_presensi_dosen.transport) + sum(occ_presensi_dosen.total_tarif) as jumlah_penerimaan,
        sum(occ_presensi_dosen.transport) + sum(occ_presensi_dosen.total_tarif) as pot_pph,
        sum(occ_presensi_dosen.total_tarif)-(sarana_dakwah+pinjaman_lkk+pph_psl+madani+zis) as total FROM `occ_presensi_dosen` 
        LEFT JOIN occ_user ON occ_user.nidn = occ_presensi_dosen.nidn
        WHERE occ_presensi_dosen.email_created='$email_created' AND occ_presensi_dosen.bulan='$bulan' AND occ_presensi_dosen.tahun='$tahun' GROUP BY nidn";
        return $this->db->query($total_bersih)->result_array();
    }

    public function getLapPresensiOperator()
    {
        $email = $this->session->userdata('email');
        $bulan = date('m');
        $tahun = date('Y');
        $total_bersih = "SELECT * FROM `occ_presensi_dosen` 
        LEFT JOIN occ_user ON occ_user.nidn = occ_presensi_dosen.nidn 
        WHERE occ_presensi_dosen.bulan='$bulan' AND occ_presensi_dosen.tahun='$tahun' AND occ_presensi_dosen.email_created='$email'";
        return $this->db->query($total_bersih)->result_array();
    }
}
