<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_file extends CI_Model
{
    public function inspj($batch)
    {
        $hasil = "SELECT occ_user.name,
        occ_user.nidn,
                   ins_pj.nama_jurnal,
                   ins_pj.peringkat_jurnal,
                   ins_pj.insentif_disetujui,
		           ins_pj.ket,
                   ins_pj.sts
                   from occ_user
                   LEFT JOIN ins_pj
                   ON ins_pj.email = occ_user.email
                   WHERE ins_pj.sts=4 AND ins_pj.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_inspj($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_pj where sts=4 AND ins_pj.batch='$batch'";
        return $this->db->query($total)->row_array();
    }


    public function insiks($batch)
    {
        $hasil = "SELECT occ_user.nidn, occ_user.name,
                   ins_ks.materi,
		           ins_ks.lokasi,
                   ins_ks.tingkat_kegiatan,
                   ins_ks.tgl_kegiatan,
                   ins_ks.insentif_disetujui,
		           ins_ks.ket,
                   ins_ks.sts
                   from occ_user
                   LEFT JOIN ins_ks
                   ON ins_ks.email = occ_user.email
                   WHERE ins_ks.sts=4 AND ins_ks.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_insiks($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_ks where sts=4 AND ins_ks.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insbfio($batch)
    {
        $hasil = "SELECT occ_user.nidn, 
                    occ_user.name,
                   ins_bfio.nama_fi,
                   ins_bfio.web_fi,
                   ins_bfio.institusi_penyelenggara, 
                   ins_bfio.lokasi,
                   ins_bfio.jenis_fi,
                   ins_bfio.judul_artikel,
		           ins_bfio.ket,
                   ins_bfio.insentif_disetujui,
                   ins_bfio.sts
                   from occ_user
                   LEFT JOIN ins_bfio
                   ON ins_bfio.email = occ_user.email
                   WHERE ins_bfio.sts=4 AND ins_bfio.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_insbfio($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_bfio where sts=4 AND ins_bfio.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insiba($batch)
    {
        $hasil = "SELECT occ_user.nidn, occ_user.name,
                   ins_iba.kontribusi_pengusul,
                   ins_iba.judul_buku,
                   ins_iba.penerbit,
                   ins_iba.isbn,
                   ins_iba.jenis_buku,
                   ins_iba.tahun_terbit,
                   ins_iba.jumlah_halaman,
		           ins_iba.ket,
                   ins_iba.insentif_disetujui,
                   ins_iba.sts
                   from occ_user
                   LEFT JOIN ins_iba
                   ON ins_iba.email = occ_user.email
                   WHERE ins_iba.sts=4 AND ins_iba.batch='$batch' ORDER BY ins_iba.jenis_buku ASC";
        return $this->db->query($hasil)->result_array();
    }

    public function total_insiba($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_iba where sts=4 AND ins_iba.batch='$batch'";
        return $this->db->query($total)->row_array();
    }


    public function insbbp($batch)
    {
        $hasil = "SELECT occ_user.nidn,
                   occ_user.name,
                   ins_bbp.judul_artikel,
                   ins_bbp.nama_jurnal,
                   ins_bbp.issn,
                   ins_bbp.vnt,
                   ins_bbp.ket,
                   ins_bbp.biaya_disetujui,
                   ins_bbp.sts
                   from occ_user
                   LEFT JOIN ins_bbp
                   ON ins_bbp.email = occ_user.email
                   WHERE ins_bbp.sts=4 AND ins_bbp.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_insbbp($batch)
    {
        $total = "SELECT SUM(biaya_disetujui) AS total FROM ins_bbp where sts=4 AND ins_bbp.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insifip($batch)
    {
        $hasil = "SELECT occ_user.nidn,
                   occ_user.name,
                   ins_ifip.kontribusi,
                   ins_ifip.kategori,
                   ins_ifip.nama_forum,
                   ins_ifip.institusi_penyelenggara,
                   ins_ifip.lokasi,
                   ins_ifip.judul_artikel_proseding,
                   ins_ifip.tahun_publikasi_proseding,
                   ins_ifip.ket,
                   ins_ifip.insentif_disetujui,
                   ins_ifip.nama_program,
                   ins_ifip.tahun_penelitian,
                   ins_ifip.sts
                   from occ_user
                   ON ins_ifip.email = occ_user.email
                   WHERE ins_ifip.sts=4 AND ins_ifip.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_insifip($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_ifip where sts=4 AND ins_ifip.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function inshki($batch)
    {
        $hasil = "SELECT occ_user.name,
                   occ_user.nidn,
                   ins_hki.judul_hki,
                   ins_hki.no_sertifikat,
                   ins_hki.jenis_hki,
                   ins_hki.tgl_pengajuan_hki,
                   ins_hki.ket,
                   ins_hki.insentif_disetujui,
                   ins_hki.sts
                   from occ_user
                   LEFT JOIN ins_hki
                   ON ins_hki.email = occ_user.email
                   WHERE ins_hki.sts=4 AND ins_hki.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_inshki($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_hki where sts=4 AND ins_hki.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insaji($batch)
    {
        $hasil = "SELECT occ_user.nidn,
                   occ_user.name,
                   ins_aji.kontribusi,
                   ins_aji.judul,
                   ins_aji.vnt,
                   ins_aji.nama_jurnal,
                   ins_aji.kategori_jurnal,
                   ins_aji.lembaga_pengindeks,
                   ins_aji.nama_program,
                   ins_aji.kategori_jurnal,
                   ins_aji.insentif_disetujui,
		           ins_aji.ket,
                   ins_aji.sts
                   from occ_user
                   LEFT JOIN ins_aji
                   ON ins_aji.email = occ_user.email
                   WHERE ins_aji.sts=4 AND ins_aji.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_insaji($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_aji where sts=4 AND ins_aji.batch='$batch'";
        return $this->db->query($total)->row_array();
    }


    public function insubpjs($batch)
    {
        $hasil = "SELECT occ_user.nidn,
                   occ_user.name,
                   ins_ubpjs.judul_artikel,
                   ins_ubpjs.nama_jurnal,
                   ins_ubpjs.issn,
                   ins_ubpjs.vnt,
                   ins_ubpjs.ket,
                   ins_ubpjs.biaya_disetujui,
                   ins_ubpjs.sts
                   from occ_user
                   LEFT JOIN ins_ubpjs
                   ON ins_ubpjs.email = occ_user.email
                   WHERE ins_ubpjs.sts=4 AND ins_ubpjs.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_insubpjs($batch)
    {
        $total = "SELECT SUM(biaya_disetujui) AS total FROM ins_ubpjs where sts=4 AND ins_ubpjs.batch='$batch'";
        return $this->db->query($total)->row_array();
    }


    public function insuipjs($batch)
    {
        $hasil = "SELECT occ_user.nidn, 
                   occ_user.name,
                   ins_uipjs.kontribusi,
                   ins_uipjs.judul,
                   ins_uipjs.vnt,
                   ins_uipjs.nama_jurnal,
                   ins_uipjs.kategori_jurnal,
                   ins_uipjs.lembaga_pengindeks,
                   ins_uipjs.nama_program,
                   ins_uipjs.kategori_jurnal,
                   ins_uipjs.insentif_disetujui,
		           ins_uipjs.ket,
                   ins_uipjs.sts
                   from occ_user
                   LEFT JOIN ins_uipjs
                   ON ins_uipjs.email = occ_user.email
                   WHERE ins_uipjs.sts=4 AND ins_uipjs.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_insuipjs($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_uipjs where sts=4 AND ins_uipjs.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insbhki($batch)
    {
        $hasil = "SELECT occ_user.nidn,
                   occ_user.name,
                   ins_bhki.judul_hki,
                   ins_bhki.jenis_hki,
                   ins_bhki.pemegang_hki,
                   ins_bhki.no_reg,
                   ins_bhki.tgl_pengajuan_hki,
                   ins_bhki.insentif_disetujui,
                   ins_bhki.sts
                   from occ_user
                   LEFT JOIN ins_bhki
                   ON ins_bhki.email = occ_user.email
                   WHERE ins_bhki.sts=4 AND ins_bhki.batch='$batch'";
        return $this->db->query($hasil)->result_array();
    }

    public function total_bhki($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_bhki where sts=4 AND ins_bhki.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insaji_excel($batch)
    {
        $this->db->select('*');
        $this->db->from('occ_user a');
        $this->db->join('ins_aji b', 'b.email = a.email');
        $this->db->where('batch', $batch);
        $query = $this->db->get();
        return $query->result();
    }

    public function inshki_excel($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_hki');
        $this->db->where('batch', $batch);
        $query = $this->db->get();
        return $query->result();
    }
}
