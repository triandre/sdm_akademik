<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_iks extends CI_Model
{
    public function getIks()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ins_ks ');
        $this->db->where('email', $email);
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from('ins_ks');
        $this->db->where('id', $id);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
