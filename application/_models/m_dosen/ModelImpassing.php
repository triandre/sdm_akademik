<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelImpassing extends CI_Model
{
    public function getImpassing()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_impassing a');
        $this->db->join('mm_pangkat b', 'b.id_pangkat = a.id_pangkat');
        $this->db->where('nidn', $nidn);
        $this->db->where('active', 1);
        $this->db->order_by('id_impassing', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeImpassing($data)
    {
        $query = $this->db->insert('occ_impassing', $data);
        return $query;
    }

    public function detailImpassing($id_impassing)
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_impassing a');
        $this->db->join('mm_pangkat b', 'b.id_pangkat = a.id_pangkat');
        $this->db->where('id_impassing', $id_impassing);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateImpassing($id_impassing, $data)
    {
        $this->db->where(array('id_impassing' => $id_impassing));
        $res = $this->db->update('occ_impassing', $data);
        return $res;
    }
}

/* End of file ModelName.php */
