<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelBeasiswa extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_beasiswa');
        $this->db->where('aktif', 1);
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeBeasiswa($data)
    {
        $query = $this->db->insert('occ_beasiswa', $data);
        return $query;
    }

    public function detailBeasiswa($id_beasiswa)
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_beasiswa');
        $this->db->where('id_beasiswa', $id_beasiswa);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateBeasiswa($id_beasiswa, $data)
    {
        $this->db->where(array('id_beasiswa' => $id_beasiswa));
        $res = $this->db->update('occ_beasiswa', $data);
        return $res;
    }
}

/* End of file ModelName.php */
