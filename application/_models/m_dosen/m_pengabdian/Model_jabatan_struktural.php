<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_jabatan_struktural extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_jabatan_struktural');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeJabatan($data)
    {
        $query = $this->db->insert('occ_jabatan_struktural', $data);
        return $query;
    }

    public function getDetail($reff_jastru)
    {
        $this->db->select('*');
        $this->db->from('occ_jabatan_struktural');
        $this->db->where('reff_jastru', $reff_jastru);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateJabatan($reff_jastru, $data)
    {
        $this->db->where(array('reff_jastru' => $reff_jastru));
        $res = $this->db->update('occ_jabatan_struktural', $data);
        return $res;
    }
}

/* End of file ModelName.php */
