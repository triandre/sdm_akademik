<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelTrack extends CI_Model
{

    public function getData()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_pdd');
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->result_array();
    }
}
