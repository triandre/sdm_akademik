<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_cuti_pimpinan extends CI_Model
{
    public function getView()
    {

        $this->db->select('*');
        $this->db->from('occ_permohonan_cuti a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.sts', 2);
        $this->db->order_by('a.id_cuti', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getViewProses()
    {

        $this->db->select('*');
        $this->db->from('occ_permohonan_cuti a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.sts', 2);
        $this->db->order_by('a.id_cuti', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getViewTolak()
    {

        $this->db->select('*');
        $this->db->from('occ_permohonan_cuti a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.sts', 4);
        $this->db->order_by('a.id_cuti', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getViewSetujui()
    {

        $this->db->select('*');
        $this->db->from('occ_permohonan_cuti a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.sts', 3);
        $this->db->order_by('a.id_cuti', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function cutiku()
    {

        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('mm_hak_cuti a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }

    function detailCuti($id_cuti)
    {
        $this->db->select('*');
        $this->db->from('occ_permohonan_cuti');
        $this->db->where('id_cuti', $id_cuti);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function getDetail()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_alamat');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 2);
        $this->db->order_by('id_alamat', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    function cutiMu($email)
    {

        $this->db->select('*');
        $this->db->from('mm_hak_cuti');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
