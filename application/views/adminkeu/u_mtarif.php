<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">

<link rel="stylesheet"

    href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <div class="container-fluid">

            <div class="row mb-2">

                <div class="col-sm-6">

                    <h1><?= $title; ?></h1>

                </div>

                <div class="col-sm-6">

                    <ol class="breadcrumb float-sm-right">

                        <li class="breadcrumb-item"><a href="#>">Dashboard</a></li>

                        <li class="breadcrumb-item "><a href="#">Manajemen Tarif</a></li>

                        <li class="breadcrumb-item active">Tarif Dosen</li>

                    </ol>

                </div>

            </div>

        </div><!-- /.container-fluid -->

    </section>



    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>

    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->

    <section class="content">



        <!-- Default box -->

        <div class="card card-primary card-outline">

            <div class="card-header">

                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"

                        title="Collapse">

                        <i class="fas fa-minus"></i></button>

                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"

                        title="Remove">

                        <i class="fas fa-times"></i></button>

                </div>

            </div>

            <div class="card-body">

                <?php if (validation_errors()) : ?>

                <div class="alert alert-danger col-md-8 alert-dismissible">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                    <?= validation_errors(); ?>

                </div>

                <?php endif ?>



                <form class="form-horizontal" action="<?= base_url('adminkeu/mtarifDosenGo'); ?>"

                    enctype="multipart/form-data" autocomplete="off" method="post">

                    <div class="card-body">

                        <div class="table-responsive">

                            <table class="table table-bordered">

                                <tbody>

                                    <tr>

                                        <td style="width:50%">NIDN<span style="color:red;">*</span> </td>

                                        <td><input class="form-control" type="text" id="nidn" name="nidn"

                                                value="<?= $d['nidn']; ?>" required readonly> </td>

                                    </tr>

                                    <tr>

                                        <td style="width:50%">Nama Dosen <span style="color:red;">*</span> </td>

                                        <td><input class="form-control" type="text" id="name" name="name"

                                                value="<?= $d['name']; ?>, <?= $d['gelar']; ?>" required readonly> </td>

                                        <td>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td>Jabatan <span style="color:red;">*</span> </td>

                                        <td><input class="form-control" type="text" id="jabatan" name="jabatan" value=""

                                                required readonly> </td>

                                    </tr>

                                    <tr>

                                        <td>Masa Kerja <span style="color:red;">*</span> </td>

                                        <td><input class="form-control" type="text" id="masa_kerja" name="masa_kerja"

                                                value="" required readonly> </td>

                                    </tr>

                                    <tr>

                                        <td>Tarif <span style="color:red;">*</span> </td>

                                        <td><input class="form-control" type="text" id="duit" name="tarif"

                                                value="<?= $d['tarif']; ?>" required> </td>

                                    </tr>



                                    <tr>

                                        <td>Potongan Sarana Dakwah <span style="color:red;">*</span> </td>

                                        <td><input class="form-control" type="text" id="duit" name="sarana_dakwah"

                                                value="<?= $d['sarana_dakwah']; ?>" required></td>

                                    </tr>



                                    <tr>

                                        <td>PPH <span style="color:red;">*</span> </td>

                                        <td><select id="pph_psl" name="pph_psl" class="form-control selectx" required>

                                                <option selected disabled value="">Pilih</option>

                                                <option value="0.025">IYA</option>

                                                <option value="0">TIDAK</option>



                                            </select> </td>

                                    </tr>



                                    <tr>

                                        <td>Potongan ZIS <span style="color:red;">*</span> </td>

                                        <td><select id="p_zis" name="p_zis" class="form-control selectx" required>

                                                <option selected disabled value="">Pilih</option>

                                                <option value="0.025">IYA</option>

                                                <option value="0">TIDAK</option>



                                            </select> </td>

                                    </tr>



                                    <tr>

                                        <td>Pinjaman LKK <span style="color:red;">*</span> </td>

                                        <td><input class="form-control" type="text" id="pulus" name="pinjaman_lkk"

                                                value="<?= $d['pinjaman_lkk']; ?>" required> </td>

                                    </tr>



                                    <tr>

                                        <td>Beban SKS <span style="color:red;">*</span> </td>

                                        <td><input class="form-control" type="text" id="beban_sks" name="beban_sks"

                                                value="<?= $d['beban_sks']; ?>" required> </td>

                                    </tr>



                                    <tr>

                                        <td>Label Dosen <span style="color:red;">*</span> </td>

                                        <td><select id="status_beban_dosen" name="status_beban_dosen"

                                                class="form-control selectx" required>

                                                <option selected disabled value="">Pilih</option>

                                                <option value="1">Dekanat</option>

                                                <option value="2">Prodi</option>

                                                <option value="3">PNS/DPK</option>

                                                <option value="4">Ketua badan/pusat/lembaga</option>

                                                <option value="5">Ketua UPT</option>

                                                <option value="6">Kepala Biro</option>

                                                 <option value="7">Dosen Tetap</option>
<option value="7">Dosen Tidak Tetap</option>
                                            </select> </td>

                                    </tr>

                                </tbody>



                            </table>



                        </div>



                    </div>



                    <!-- /.card-body -->



                    <div class="card-footer">



                        <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i>

                            Kembali</a>



                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>



                    </div>



                    <!-- /.card-footer -->



                </form>



            </div>







        </div>



        <!-- /.card -->



    </section>



    <!-- /.content -->























</div>



<!-- /.content-wrapper -->



<!-- Select2 -->



<script src="<?= base_url() ?>assets/js/rupiah.js"></script>



<script src="<?= base_url() ?>assets/js/duit.js"></script>



<script src="<?= base_url() ?>assets/js/pulus.js"></script>



<script src="<?= base_url() ?>assets/js/money.js"></script>



<script src="<?= base_url() ?>assets/js/cuan.js"></script>



<script src="<?= base_url() ?>assets/js/duitku.js"></script>