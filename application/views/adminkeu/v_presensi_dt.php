<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.5/css/buttons.dataTables.min.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/jquery.dataTables.min.css">

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <div class="container-fluid">

            <div class="row mb-2">

                <div class="col-sm-6">

                    <h1><?= $title; ?></h1>

                </div>

                <div class="col-sm-6">

                    <ol class="breadcrumb float-sm-right">

                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>

                        <li class="breadcrumb-item active"><a href="#">Kehadiran Dosen</a></li>

                    </ol>

                </div>

            </div>

        </div><!-- /.container-fluid -->

    </section>



    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>

    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>



    <!-- Main content -->

    <section class="content">



        <!-- Default box -->

        <div class="card">

            <div class="card-header">

                <?php if ($this->session->flashdata('gagal_store')) { ?>

                    <div class="alert alert-danger col-md-12">

                        <?= $this->session->flashdata('gagal_store') ?>

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">

                            <span aria-hidden="true">&times;</span>

                        </button>

                    </div>

                <?php } ?>



                <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>

                <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>



                <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">

                        <i class="fas fa-minus"></i></button>

                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">

                        <i class="fas fa-times"></i></button>

                </div>

            </div>

            <div class="card-body">

                <div class="table-responsive">

                    <table id="example" class="table table-bordered table-striped table-sm">

                        <thead>

                            <tr>

                                <th>No.</th>

                                <th>Payroll</th>

                                <th>NIDN</th>

                                <th>Nama</th>

                                <th>Beban Sks</th>

                                <th>Sks Diterima</th>

                                <th>Kelebihan Sks</th>

                                <th>Status Dosen</th>

                                <th>Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php

                            $no = 1;

                            foreach ($v as $row) : ?>

                                <tr>

                                    <td><?= $no++; ?></td>

                                    <td><?= $row['payrol']; ?></td>

                                    <td><?= $row['nidn']; ?></td>

                                    <td><?= $row['name']; ?></td>

                                    <td><?= $row['beban_sks']; ?></td>

                                    <td><?= $row['jsks']; ?></td>

                                    <td>

                                        <?php

                                        $b = $row['beban_sks'];

                                        $a = $row['jsks'];

                                        $c = $a - $b;  ?>



                                        <?= $c; ?>

                                    </td>

                                    <td><?php

                                        if ($row['role_id'] == 2) {

                                            echo '<span class="badge badge-info">Dosen Tetap</span> ';
                                        } elseif ($row['role_id'] == 10) {

                                            echo '<span class="badge badge-primary">Dosen Tidak Tetap</span>';
                                        }

                                        ?></td>





                                    <td>

                                        <a href="<?= base_url('adminkeu/detailPresensi_dt/' . $row['nidn']) ?>" class="btn btn-warning btn-sm" title="Detail Data">

                                            <i class="fas fa-eye"></i>

                                        </a>





                                    </td>

                                </tr>

                            <?php endforeach ?>

                        </tbody>

                    </table>

                </div>

            </div>

    </section>

    <!-- /.content -->



</div>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

<script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/buttons/2.3.5/js/dataTables.buttons.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

<script src="https://cdn.datatables.net/buttons/2.3.5/js/buttons.html5.min.js"></script>

<script src="https://cdn.datatables.net/buttons/2.3.5/js/buttons.print.min.js"></script>

<script>
    $(document).ready(function() {

        $('#example').DataTable({

            dom: 'Bfrtip',

            buttons: [

                'excel', 'pdf'

            ]

        });

    });
</script>