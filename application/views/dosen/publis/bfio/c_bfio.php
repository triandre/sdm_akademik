<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Form UB. Seminar Nasional/Internasional</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Form UB. Biaya Publikasi</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <form action="<?= base_url('createBfio'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <h4>Form Usulan Bantuan Mengikuti Seminar Internasional/Nasional</h4>
                        <small>Isi data-data Berikut: </small><small class="text-danger">(**)Wajib Diisi.</small>
                        <hr>
                        <div class="row form-group">
                            <label class="col-md-4 text-md-left" for="nama_barang"><b>A.Identitas Forum Ilmiah</b></label>

                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Kegiatan Seminar <small class="text-danger">**</small></label>
                            <div class="col-md-3">
                                <select class="form-control" id="kgt" name="kgt" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Daring">Daring</option>
                                    <option value="luring">luring</option>
                                </select>
                                <?= form_error('kgt', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>



                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Nama Forum Ilmiah <small class="text-danger">**</small></label>
                            <div class="col-md-6">
                                <input value="<?= set_value('nama_fi'); ?>" name="nama_fi" id="nama_fi" type="text" class="form-control">
                                <?= form_error('nama_fi', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Website Forum Ilmiah <small class="text-danger">**</small></label>
                            <div class="col-md-6">
                                <input value="<?= set_value('web_fi'); ?>" name="web_fi" id="web_fi" type="text" class="form-control">
                                <?= form_error('web_fi', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Institusi Penyelenggara <small class="text-danger">**</small></label>
                            <div class="col-md-6">
                                <input value="<?= set_value('institusi_penyelenggara'); ?>" name="institusi_penyelenggara" id="institusi_penyelenggara" type="text" class="form-control">
                                <?= form_error('institusi_penyelenggara', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Tanggal Kegiatan Forum Ilmiah <small class="text-danger">**</small></label>
                            <div class="col-md-3">
                                <input value="<?= set_value('tgl_penyelenggara'); ?>" name="tgl_penyelenggara" id="tgl_penyelenggara" type="date" class="form-control">
                                <?= form_error('tgl_penyelenggara', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Lokasi Penyelenggara <small class="text-danger">**</small></label>
                            <div class="col-md-6">
                                <input value="<?= set_value('lokasi'); ?>" name="lokasi" id="lokasi" type="text" class="form-control">
                                <?= form_error('lokasi', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Jenis Forum Ilmiah <small class="text-danger">**</small></label>
                            <div class="col-md-3">
                                <select class="form-control" id="jenis_fi" name="jenis_fi" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Nasional">Nasional</option>
                                    <option value="Internasional">Internasional</option>
                                </select>
                                <?= form_error('jenis_fi', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Biaya Pendaftaran <small class="text-danger">**</small></label>
                            <div class="col-md-3">
                                <input name="biaya_pendaftaran" id="duit" type="text" class="form-control">
                                <?= form_error('biaya_pendaftaran', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">UPLOAD LAPORAN MANDIRI(Laporan Mandiri Boleh Menyusul untuk diserahkan),Surat Permohonan LoA, Artikel Dan Bukti Pembayaran Dijadikan 1 File Pdf</label>
                            <div class="col-md-5">
                                <input type="file" id="upload_laporan" name="upload_laporan" accept="application/pdf">
                            </div>
                        </div>



                        <div class="row form-group">
                            <label class="col-md-4 text-md-left" for="nama_barang"><b>B. Indetitas Artikel</b></label>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Judul Artikel <small class="text-danger">**</small></label>
                            <div class="col-md-6">
                                <input value="<?= set_value('judul_artikel'); ?>" name="judul_artikel" id="judul_artikel" type="text" class="form-control">
                                <?= form_error('judul_artikel', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Apakah Artikel Sudah Pernah di Publikasi? <small class="text-danger">**</small> </label>
                            <div class="col-md-3">
                                <select class="form-control" id="pertanyaan1" name="pertanyaan1" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Sudah">Sudah</option>
                                    <option value="Belum">Belum</option>
                                </select>
                                <?= form_error('pertanyaan1', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Publikasi Proseding Terindeks <small class="text-danger">**</small></label>
                            <div class="col-md-3">
                                <select class="form-control" id="pertanyaan2" name="pertanyaan2" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="scopus">scopus</option>
                                    <option value="Google Scholar">Google Scholar</option>
                                    <option value="Tidak Terindeks">Tidak Terindeks</option>
                                </select>
                                <?= form_error('pertanyaan2', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Apakah Artikel Merupakan Hasil Penelitian/Pengabdian <small class="text-danger">**</small></label>
                            <div class="col-md-3">
                                <select class="form-control" id="pertanyaan3" name="pertanyaan3" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="internal UMSU">internal UMSU</option>
                                    <option value="Dikti">Dikti</option>
                                    <option value="Mandiri">Mandiri</option>
                                    <option value="Ekternal Lain">Ekternal Lain</option>
                                    <option value="Bukan Hasil Penelitian/Pengabdian">Bukan Hasil Penelitian/Pengabdian</option>
                                </select>
                                <?= form_error('pertanyaan3', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-12 text-md-left" for="nama_barang"><b>C. Khusus untuk Penelitian/Pengabdian Masyarakat Mandiri harus mengupload Laporan Akhir yang sudah di Sahkan oleh Ketua LP2M. Standar Laporan Mandiri sama dengan Standar Laporan Penelitian/Pengabdian Masyarakat Internal.</b></label>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-left" for="nama_barang">Saya Bersedia Menyerahkan Laporan Penelitian Mandiri ?</label>
                            <div class="col-md-5">
                                <select class="form-control" id="pertanyaan4" name="pertanyaan4">
                                    <option selected disabled value="">Pilih</option>
                                    <option value="IYA">IYA</option>
                                    <option value="TIDAK">TIDAK</option>
                                </select>
                            </div>
                        </div>




                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>

        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script src="<?= base_url() ?>assets/js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>