<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('') ?>">UI. Keynote Speaker</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">Email</td>
                            <td width="50px">:</td>
                            <td><?= $d['email'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Judul Materi</td>
                            <td width="50px">:</td>
                            <td><?= $d['materi']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Lokasi</td>
                            <td width="50px">:</td>
                            <td><?= $d['lokasi'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tgl Pelaksanaan </td>
                            <td width="50px">:</td>
                            <td><?= $d['tgl_kegiatan'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tingkat Kegiatan</td>
                            <td width="50px">:</td>
                            <td><?= $d['tingkat_kegiatan'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Nomor Sertifikat</td>
                            <td width="50px">:</td>
                            <td><?= $d['no_sertifikat'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Dokumen Undangan Sebagai Keynote Speaker</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/') ?><?= $d['upload_undangan'] ?>" target="_blank"><i class="fa fa-download"></i></td>
                        </tr>
                        <tr>
                            <td width="100px">Dokumen Materi Dan Dokumentasi Acara</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/') ?><?= $d['upload_bukti_kegiatan']; ?>" target="_blank"><i class="fa fa-download"></i></td>
                        </tr>
                        <tr>
                            <td width="100px">Dokumen sERTIFIKAT</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/') ?><?= $d['upload_sertifikat']; ?>" target="_blank"><i class="fa fa-download"></i></td>
                        </tr>



                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="<?= base_url('pembicara') ?>">
                    <button type="button" class="btn btn-danger"><i class="fa fa-backward"></i> Kembali</button>
                </a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->