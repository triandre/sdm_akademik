<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Riwayat Ajuan Data Jabatan Fungsional</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="">Jabatan Fungsional</a></li>
                        <li class="breadcrumb-item"><a href=""><?= $title; ?></a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <section class="content">

        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    Riwayat Ajuan Data Jabatan Fungsional
                </h3>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true"><span class="badge badge-default"><?= $nDraft; ?> </span> Draft</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false"><span class="badge badge-primary"><?= $nDiajukan; ?> </span> Diajukan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-settings-tab" data-toggle="pill" href="#custom-content-below-setujui" role="tab" aria-controls="custom-content-below-settings" aria-selected="false"><span class="badge badge-success"><?= $nDisetujui; ?> </span> Disetujui</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-messages-tab" data-toggle="pill" href="#custom-content-below-messages" role="tab" aria-controls="custom-content-below-messages" aria-selected="false"><span class="badge badge-danger"><?= $nDitolak; ?> </span> Ditolak</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-settings-tab" data-toggle="pill" href="#custom-content-below-settings" role="tab" aria-controls="custom-content-below-settings" aria-selected="false"><span class="badge badge-warning"><?= $nDitangguhkan; ?> </span> Ditangguhkan</a>
                    </li>

                </ul>
                <div class="tab-content" id="custom-content-below-tabContent">
                    <!-- DRAFT -->
                    <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Dibuat</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Perubahan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($draft as $row) : ?>
                                            <td><?= $row['tgl_ajuan']; ?></td>
                                            <td><?= $row['jenis_ajuan']; ?></td>
                                            <td><span class="badge badge-default">Belum Dikirim</span></td>
                                            <td><a href="<?= base_url('profil/detail/'); ?><?= $row['reff']; ?>" title="Detail"><i class="fa fa-information"></i> </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- DIAJUKAN -->
                    <div class="tab-pane fade" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Perubahan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($diajukan as $row2) : ?>
                                            <td><?= date("d M Y", strtotime($row2['tgl_ajuan'])); ?></td>
                                            <td><?= $row2['jenis_ajuan']; ?></td>
                                            <td><span class="badge badge-primary">Diajukan</span></td>
                                            <td> <a href="<?= base_url('detailJafungRiwayat/' . $row2['reff']) ?>" class="btn btn-info btn-sm" title="Detail Data">
                                                    <i class="fas fa-info"></i>
                                                </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="custom-content-below-setujui" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Disetujui</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Perubahan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($disetujui as $row3) : ?>
                                            <td><?= date("d M Y", strtotime($row3['tgl_ajuan'])); ?></td>
                                            <td><?= date("d M Y", strtotime($row3['tgl_verifikasi'])); ?></td>
                                            <td><?= $row3['jenis_ajuan']; ?></td>
                                            <td><span class="badge badge-success">Disetujui</span></td>
                                            <td> <a href="<?= base_url('detailJafungRiwayat/' . $row3['reff']) ?>" class="btn btn-info btn-sm" title="Detail Data">
                                                    <i class="fas fa-info"></i>
                                                </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="custom-content-below-messages" role="tabpanel" aria-labelledby="custom-content-below-messages-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Ditolak</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Penolakan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($ditolak as $row4) : ?>
                                            <td><?= date("d M Y", strtotime($row4['tgl_ajuan'])); ?></td>
                                            <td><?= date("d M Y", strtotime($row4['tgl_verifikasi'])); ?></td>
                                            <td><?= $row4['komentar']; ?></td>
                                            <td><span class="badge badge-warning">Ditolak</span></td>
                                            <td> <a href="<?= base_url('detailJafungRiwayat/' . $row4['reff']) ?>" class="btn btn-info btn-sm" title="Detail Data">
                                                    <i class="fas fa-info"></i>
                                                </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="custom-content-below-settings" role="tabpanel" aria-labelledby="custom-content-below-settings-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Ditangguhkan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Penangguhan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($ditangguhkan as $row5) : ?>
                                            <td><?= date("d M Y", strtotime($row5['tgl_ajuan'])); ?></td>
                                            <td><?= date("d M Y", strtotime($row5['tgl_verifikasi'])); ?></td>
                                            <td><?= $row5['komentar']; ?></td>
                                            <td><span class="badge badge-danger">Ditangguhkan</span></td>
                                            <td> <a href="<?= base_url('detailJafungRiwayat/' . $row5['reff']) ?>" class="btn btn-info btn-sm" title="Detail Data">
                                                    <i class="fas fa-info"></i>
                                                </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            </div>



        </div>
        <!-- /.card -->
</div>
</section>
<!-- /.content -->