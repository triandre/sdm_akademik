<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">Beranda</li>
                        <li class="breadcrumb-item">Ajuan Pdd</li>
                        <li class="breadcrumb-item">Data Pribadi</li>
                        <li class="breadcrumb-item active">Validasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Detail Ajuan Data Profil</h3>

                <div class="card-tools">
                    <a href="javascript:history.back()" class="btn btn-info"><i class="fa fa-backward"></i> Kembali</a>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>

                <form class="form-horizontal">
                    <div class="card-body">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th></th>
                                    <th>Data Profil Lama</th>
                                    <th>Data Profil Baru</th>
                                </tr>

                                <tr>
                                    <td>Jenis Kelamin</td>
                                    <td><?= $d['jk']; ?></td>
                                    <td><?= $dr['jk']; ?></td>
                                </tr>
                                <tr>
                                    <td>Tempat Lahir</td>
                                    <td><?= $d['t_lahir']; ?></td>
                                    <td><?= $dr['t_lahir']; ?></td>
                                </tr>

                                <tr>
                                    <td>Tanggal Lahir</td>
                                    <td><?= $d['tgl_lahir']; ?></td>
                                    <td><?= $dr['tgl_lahir']; ?></td>

                                    </td>
                                </tr>

                                <tr>
                                    <td>Nama Ibu Knadung</td>
                                    <td><?= $d['nama_ibu']; ?></td>
                                    <td><?= $dr['nama_ibu']; ?></td>
                                </tr>


                                <tr>
                                    <td>Document Pendukung</td>
                                    <td> <a href="<?= base_url('archive/profil/'); ?><?= $d['file']; ?>" target="_blank" rel="noopener noreferrer">File Lama </a></td>
                                    <td> <a href="<?= base_url('archive/profil/'); ?><?= $dr['file']; ?>" target="_blank" rel="noopener noreferrer">File Baru </a></td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                    <!-- /.card-body -->
                    <?php if ($this->session->userdata('role_id') == '7' and $dr['sts'] == 1) { ?>
                        <div class="card-footer">
                            <a href="<?= base_url('ajuan_pdd/setujuiDataProfil/'); ?><?= $dr['reff_profil']; ?>" class="btn btn-success"><i class="fa fa-check"></i> Setujui</a>
                            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-danger"><i class="fa fa-times"></i> Tolak</button>
                        </div>
                        <!-- /.card-footer -->
                    <?php } ?>
                </form>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Keterangan Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="post" action="<?= base_url('ajuan_pdd/tolakProfil'); ?>">
                        <div class="form-group">
                            <label for="usr">Reff:</label>
                            <input type="text" class="form-control" id="reff_profil" name="reff_profil" value="<?= $dr['reff_profil']; ?>" readonly>
                        </div>

                        <div class="form-group">
                            <label for="usr">Keterangan:</label>
                            <textarea type="text" class="form-control" id="komentar" name="komentar" rows="8" required></textarea>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- /.content-wrapper -->
<!-- Select2 -->