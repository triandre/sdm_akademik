<?php

$mpdf = new \Mpdf\Mpdf([
  'mode' => 'utf-8',
  'format' => [210, 330],
  'orientation' => 'L'
]);

$isi ='<!DOCTYPE html>
<html>
<head>
    <title>Report Penerima Insentif Keynote Speaker </title>
</head>
<style>
table {
  font-family: arial, sans-serif;
  font-size: 10px;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<body>
<h4 align="center">Laporan Penerima Keynote Speaker </h4>
<table>
<thead>
<tr>
<th>No</th>
<th>Nama Pengusul</th>
<th>Program Studi</th>
<th>Materi</th>
<th>Lokasi</th>
<th>Tingkat Kegiatan</th>
<th>Tanggal Kegiatan</th>
<th>Hasil Pemeriksaan</th>
<th>Insentif</th>
</tr>
</thead>
<tbody>';

$i = 1; 
foreach ($gas as $gs) :

$isi .='<tr>
<td>' . $i . '</td>
<td>' . $gs["name"] . '</td>
<td>' . $gs["prodi"] . '</td>
<td>' . $gs["materi"] . '</td>
<td>' . $gs["lokasi"] . '</td>
<td>' . $gs["tingkat_kegiatan"] . '</td>
<td>' . $gs["tgl_kegiatan"] . '</td>
<td>' . $gs["ket"] . '</td>
<td>Rp. ' . number_format($gs['insentif_disetujui']) . '</td>
</tr>';

$i++;
endforeach;
$isi .='
<tr>
<td colspan="8"><b>TOTAL KESELURUHAN</b></td>
<td><b>Rp. '.number_format($zat["total"]).'</b></td>
</tr>
</tbody>
</table> 
</body>
</html>';

$mpdf->WriteHTML($isi);
$mpdf->Output();
