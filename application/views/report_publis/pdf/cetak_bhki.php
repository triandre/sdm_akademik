<?php

$mpdf = new \Mpdf\Mpdf([
  'mode' => 'utf-8',
  'format' => [210, 330],
  'orientation' => 'L'
]);

$isi = '<!DOCTYPE html>
<html>
<head>
    <title>Report Bantuan HKI </title>
</head>
<style>
table {
  font-family: arial, sans-serif;
  font-size: 10px;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<body>
<h4 align="center">Laporan Bantuan HKI</h4>
<table>
<thead>
<tr>
<th>No</th>
<th>NIDN</th>
<th>Nama Pengusul</th>
<th>Judul HKI</th>
<th>No. Reg</th>
<th>Jenis HKI</th>
<th>Bantuan Dana</th>
</tr>
</thead>
<tbody>';

$i = 1;
foreach ($gas as $gs) :

  $isi .= '<tr>
<td>' . $i . '</td>
<td>' . $gs["nidn"] . '</td>
<td>' . $gs["name"] . '</td>
<td>' . $gs["judul_hki"] . '</td>
<td>' . $gs["no_reg"] . '</td>
<td>' . $gs["jenis_hki"] . '</td>
<td>Rp. ' . number_format($gs['insentif_disetujui']) . '</td>
</tr>';

  $i++;
endforeach;
$isi .= '
<tr>
<td colspan="6"><b>TOTAL KESELURUHAN</b></td>
<td><b>Rp. ' . number_format($zat["total"]) . '</b></td>
</tr>
</tbody>
</table> 
</body>
</html>';

$mpdf->WriteHTML($isi);
$mpdf->Output();
