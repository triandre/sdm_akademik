<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_muser extends CI_Model
{
    public function getview()
    {

        $this->db->select('*');
        $this->db->from('occ_user');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function save_user($data)
    {
        $query = $this->db->insert('occ_user', $data);
        return $query;
    }

    public function detail_user($id)
    {
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function update_user($id, $data)
    {
        $this->db->where(array('id' => $id));
        $res = $this->db->update('occ_user', $data);
        return $res;
    }
}

/* End of file ModelName.php */
