<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelPenempatan extends CI_Model
{
    public function getPenempatan()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_penempatan a');
        $this->db->join('occ_user b', 'b.nidn = a.nidn');
        $this->db->where('b.nidn', $nidn);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getPenempatanAdmin()
    {

        $this->db->select('*');
        $this->db->from('occ_penempatan a');
        $this->db->join('occ_user b', 'b.nidn = a.nidn');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPenempatanDetail($reff_penempatan)
    {

        $this->db->select('*');
        $this->db->from('occ_penempatan a');
        $this->db->join('occ_user b', 'b.nidn = a.nidn');
        $this->db->where('reff_penempatan', $reff_penempatan);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updatePenempatan($reff_penempatan, $data)
    {
        $this->db->where(array('reff_penempatan' => $reff_penempatan));
        $res = $this->db->update('occ_penempatan', $data);
        return $res;
    }

    public function getPegawai()
    {

        $this->db->select('*');
        $this->db->from('occ_user');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUnit()
    {

        $this->db->select('*');
        $this->db->from('mm_unit');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function save_penempatan($data)
    {
        $query = $this->db->insert('occ_penempatan', $data);
        return $query;
    }
}

/* End of file ModelName.php */
