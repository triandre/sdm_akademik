<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelRiwpek extends CI_Model
{
    public function getRiwpek()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_riwpek');
        $this->db->where('nidn', $nidn);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeRiwpek($data)
    {
        $query = $this->db->insert('occ_riwpek', $data);
        return $query;
    }

    public function detailRiwpek($id_riwpek)
    {
        $this->db->select('*');
        $this->db->from('occ_riwpek');
        $this->db->where('id_riwpek', $id_riwpek);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateRiwpek($id_riwpek, $data)
    {
        $this->db->where(array('id_riwpek' => $id_riwpek));
        $res = $this->db->update('occ_riwpek', $data);
        return $res;
    }
}

/* End of file ModelName.php */
