<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelDiklat extends CI_Model
{
    public function getDiklat()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_diklat');
        $this->db->where('nidn', $nidn);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeDiklat($data)
    {
        $query = $this->db->insert('occ_diklat', $data);
        return $query;
    }

    public function detailDiklat($id_diklat)
    {
        $this->db->select('*');
        $this->db->from('occ_diklat');
        $this->db->where('id_diklat', $id_diklat);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function detailDiklatRiwayat($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_diklat');
        $this->db->where('reff_diklat', $reff);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function viewDraft()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->result_array();
    }

    public function viewDiajukan()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDisetujui()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->result_array();
    }

    public function viewTolak()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->result_array();
    }

    public function viewDitangguhkan()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=5 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->result_array();
    }

    public function numsDraft()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->num_rows();
    }

    public function numsDiajukan()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->num_rows();
    }

    public function numsDisetujui()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->num_rows();
    }

    public function numsTolak()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->num_rows();
    }

    public function numsDitangguhkan()
    {
        $kode = "DKT";
        $kode2 = "NDKT";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=5 AND kode_ajuan='$kode' OR kode_ajuan='$kode2' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->num_rows();
    }
}

/* End of file ModelName.php */
