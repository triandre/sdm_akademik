<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pem_dosen extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_bimbingan_dosen');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storePemdos($data)
    {
        $query = $this->db->insert('occ_bimbingan_dosen', $data);
        return $query;
    }

    public function getDetail($reff_bdos)
    {
        $this->db->select('*');
        $this->db->from('occ_bimbingan_dosen');
        $this->db->where('reff_bdos', $reff_bdos);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updatePemdos($reff_bdos, $data)
    {
        $this->db->where(array('reff_bdos' => $reff_bdos));
        $res = $this->db->update('occ_bimbingan_dosen', $data);
        return $res;
    }
}

/* End of file ModelName.php */
