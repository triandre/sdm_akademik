<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_uipjs extends CI_Model
{
    public function getUipjs()
    {
        $this->db->select('*');
        $this->db->from('ins_uipjs ');
        $this->db->where('sts', 3);
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from('ins_uipjs');
        $this->db->where('id', $id);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getHistory($email)
    {
        $this->db->select('*');
        $this->db->from('ins_uipjs ');
        $this->db->where('email', $email);
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
}

/* End of file ModelName.php */
