<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_track extends CI_Model
{
    public function getAji()
    {
        $this->db->select('*');
        $this->db->from('ins_aji ');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPj()
    {
        $this->db->select('*');
        $this->db->from('ins_pj ');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getHki()
    {
        $this->db->select('*');
        $this->db->from('ins_hki ');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getIks()
    {
        $this->db->select('*');
        $this->db->from('ins_ks ');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getIfip()
    {
        $this->db->select('*');
        $this->db->from('ins_ifip');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getIba()
    {
        $this->db->select('*');
        $this->db->from('ins_iba');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getUipjs()
    {
        $this->db->select('*');
        $this->db->from('ins_uipjs');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getBbp()
    {
        $this->db->select('*');
        $this->db->from('ins_bbp');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getBfio()
    {
        $this->db->select('*');
        $this->db->from('ins_bfio');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUbpjs()
    {
        $this->db->select('*');
        $this->db->from('ins_ubpjs');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getJi()
    {
        $this->db->select('*');
        $this->db->from('jenis_insentif');
        $query = $this->db->get();
        return $query->result_array();
    }
}

/* End of file ModelName.php */
