<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pengaturan extends CI_Model
{
    public function getUser()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }
}
