<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Diklat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/ModelDiklat', 'mdk');
        $this->load->model('m_master/ModelPangkat', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Diklat',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_diklat' => 'active',
            'diklat' => $this->mdk->getDiklat(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/diklat/v_diklat', $data);
        $this->load->view('layouts/footer');
    }

    public function createDiklat()
    {
        $data = array(
            'title' => 'Ajuan Data Diklat Pekerti/AA',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_diklat' => 'active',
            'diklat' => $this->mdk->getDiklat(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/diklat/c_diklat', $data);
        $this->load->view('layouts/footer');
    }

    public function createDiklatNon()
    {
        $data = array(
            'title' => 'Ajuan Data Diklat Non Pekerti/AA',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_diklat' => 'active',
            'diklat' => $this->mdk->getdiklat(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/diklat/c_non_diklat', $data);
        $this->load->view('layouts/footer');
    }

    public function saveDiklat()
    {
        $data = array(
            'title' => 'Ajuan Data Diklat',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_diklat' => 'active',
            'diklat' => $this->mdk->getdiklat(),

        );
        $id = $this->uuid->v4();
        $reff_diklat = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 2;
        $kode_ajuan = "DKT";
        $jenis_ajuan = "Ajuan Data Diklat (Pekerti/AA)";
        $jenis_pekerti = "Pekerti/AA";

        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/diklat/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_diklat']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/diklat/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_diklat' => $reff_diklat,
            'nidn' => $nidn,
            'jenis_pekerti' => $jenis_pekerti,
            'jenis_diklat' => htmlspecialchars($this->input->post('jenis_diklat', true)),
            'kategori_kegiatan' => htmlspecialchars($this->input->post('kategori_kegiatan', true)),
            'tingkat_kegiatan' => htmlspecialchars($this->input->post('tingkat_kegiatan', true)),
            'penyelenggara' => htmlspecialchars($this->input->post('penyelenggara', true)),
            'peran' => htmlspecialchars($this->input->post('peran', true)),
            'jumlah_jam' => htmlspecialchars($this->input->post('jumlah_jam', true)),
            'no_sertifikat' => htmlspecialchars($this->input->post('no_sertifikat', true)),
            'tgl_sertifikat' => htmlspecialchars($this->input->post('tgl_sertifikat', true)),
            'tahun_penyelenggara' => htmlspecialchars($this->input->post('tahun_penyelenggara', true)),
            'tempat' => htmlspecialchars($this->input->post('tempat', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
            'no_sk_penugasan' => htmlspecialchars($this->input->post('no_sk_penugasan', true)),
            'tgl_sk_penugasan' => htmlspecialchars($this->input->post('tgl_sk_penugasan', true)),
            'nama_diklat' => htmlspecialchars($this->input->post('nama_diklat', true)),
            'sts' => $sts,
            'file' => $upload1,
            'active' => 1,
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_diklat,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->mdk->storediklat($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('diklat');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createDiklat');
        }
    }


    public function saveDiklatNon()
    {
        $data = array(
            'title' => 'Ajuan Data Diklat',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_diklat' => 'active',
            'diklat' => $this->mdk->getdiklat(),

        );
        $id = $this->uuid->v4();
        $reff_diklat = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 2;
        $kode_ajuan = "NDKT";
        $jenis_ajuan = "Ajuan Data Diklat (Non Pekerti/AA)";
        $jenis_pekerti = "Non Pekerti/AA";

        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/diklat/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_diklat']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/diklat/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_diklat' => $reff_diklat,
            'nidn' => $nidn,
            'jenis_pekerti' => $jenis_pekerti,
            'jenis_diklat' => htmlspecialchars($this->input->post('jenis_diklat', true)),
            'kategori_kegiatan' => htmlspecialchars($this->input->post('kategori_kegiatan', true)),
            'tingkat_kegiatan' => htmlspecialchars($this->input->post('tingkat_kegiatan', true)),
            'penyelenggara' => htmlspecialchars($this->input->post('penyelenggara', true)),
            'peran' => htmlspecialchars($this->input->post('peran', true)),
            'jumlah_jam' => htmlspecialchars($this->input->post('jumlah_jam', true)),
            'no_sertifikat' => htmlspecialchars($this->input->post('no_sertifikat', true)),
            'tgl_sertifikat' => htmlspecialchars($this->input->post('tgl_sertifikat', true)),
            'tahun_penyelenggara' => htmlspecialchars($this->input->post('tahun_penyelenggara', true)),
            'tempat' => htmlspecialchars($this->input->post('tempat', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
            'no_sk_penugasan' => htmlspecialchars($this->input->post('no_sk_penugasan', true)),
            'tgl_sk_penugasan' => htmlspecialchars($this->input->post('tgl_sk_penugasan', true)),
            'nama_diklat' => htmlspecialchars($this->input->post('nama_diklat', true)),
            'sts' => $sts,
            'file' => $upload1,
            'active' => 1,
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_diklat,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->mdk->storediklat($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('diklat');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createDiklat');
        }
    }

    public function hapusDiklat($id_diklat)
    {
        $id_diklat = $this->uri->segment(2);
        $active = 0;

        $this->db->set('active', $active);
        $this->db->where('id_diklat', $id_diklat);
        $this->db->update('occ_diklat');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('diklat');
    }

    public function detailDiklat()
    {
        $id_diklat = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Diklat',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_diklat' => 'active',
            'd' => $this->mdk->detailDiklat($id_diklat),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/diklat/d_diklat', $data);
        $this->load->view('layouts/footer');
    }


    public function riwayatDiklat()
    {
        $data = array(
            'title' => 'Riwayat Diklat',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_diklat' => 'active',
            'diklat' => $this->mdk->getdiklat(),
            'draft' => $this->mdk->viewDraft(),
            'diajukan' => $this->mdk->viewDiajukan(),
            'disetujui' => $this->mdk->viewDisetujui(),
            'ditolak' => $this->mdk->viewTolak(),
            'ditangguhkan' => $this->mdk->viewDitangguhkan(),
            'nDraft' => $this->mdk->numsDraft(),
            'nDiajukan' => $this->mdk->numsDiajukan(),
            'nDisetujui' => $this->mdk->numsDisetujui(),
            'nDitangguhkan' => $this->mdk->numsDitangguhkan(),
            'nDitolak' => $this->mdk->numsTolak()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/diklat/r_diklat', $data);
        $this->load->view('layouts/footer');
    }

    public function detailDiklatRiwayat()
    {
        $reff = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Diklat',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_diklat' => 'active',
            'd' => $this->mdk->detailDiklatRiwayat($reff)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/diklat/d_diklat', $data);
        $this->load->view('layouts/footer');
    }
}
