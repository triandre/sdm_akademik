<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mprod extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_manajemen/Model_mfak', 'mm');
        $this->load->model('m_manajemen/Model_mprod', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Manajemen Program Studi',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mprod' => 'active',
            'v' => $this->mp->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/mprod/v_mprod', $data);
        $this->load->view('layouts/footer');
    }

    public function new_prod()
    {
        $data = array(
            'title' => 'Tambah Data Program Studi',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mprod' => 'active',
            'fakultas' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/mprod/c_mprod', $data);
        $this->load->view('layouts/footer');
    }

    public function save_prod()
    {
        $data = array(
            'title' => 'Tambah Data Program Studi',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mprod' => 'active',
            'v' => $this->mp->getView()
        );
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/arsip/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['mm_prodi']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/arsip/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'kode_prodi' =>  htmlspecialchars($this->input->post('kode_prodi', true)),
            'prodi' =>  htmlspecialchars($this->input->post('prodi', true)),
            'kode_fakultas' =>  htmlspecialchars($this->input->post('kode_fakultas', true)),
            'aktif' => 1,
            'file' => $upload1,
            'date_created' => date('Y-m-d H:i:s')
        );

        $result = $this->mp->save_prod($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('mprod');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('mprod/new');
        }
    }


    public function hapus_prod()
    {
        $id_prodi = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_prodi', $id_prodi);
        $this->db->update('mm_prodi');
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('mprod');
    }

    public function edit_prod()
    {
        $id_prodi = $this->uri->segment(3);
        $data = array(
            'title' => 'Edit Data Program Studi',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mprod' => 'active',
            'd' => $this->mp->detail_prod($id_prodi),
            'fakultas' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/mprod/e_mprod', $data);
        $this->load->view('layouts/footer');
    }

    public function editGo_prod()
    {
        $data = array(
            'title' => 'Edit Data Fakultas',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mprod' => 'active',
            'v' => $this->mm->getView()
        );
        $id_prodi =  htmlspecialchars($this->input->post('id_prodi', true));
        $kode_fakultas = htmlspecialchars($this->input->post('kode_fakultas', true));
        $kode_prodi = htmlspecialchars($this->input->post('kode_prodi', true));
        $prodi = htmlspecialchars($this->input->post('prodi', true));

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/arsip/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['mm_prodi']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/arsip/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->set('kode_fakultas', $kode_fakultas);
        $this->db->set('kode_prodi', $kode_prodi);
        $this->db->set('prodi', $prodi);
        $this->db->where('id_prodi', $id_prodi);
        $this->db->update('mm_prodi');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('mprod');
    }
}
