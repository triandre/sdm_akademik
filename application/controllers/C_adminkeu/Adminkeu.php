<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Adminkeu extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		if ($this->session->userdata("logged") <> 1) {

			redirect(site_url('login'));
		}

		$this->load->library('uuid');

		$this->load->model('m_presensi/ModelPresensi', 'mp');
	}



	public function index()

	{

		$data = array(

			'title' => 'Dashboard',

			'active_menu_db' => 'active',

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/index', $data);

		$this->load->view('layouts/footer');
	}



	public function listPresensi()

	{

		$data = array(

			'title' => 'List Presensi Dosen Tetap',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_tetap' => 'active',

			'v' => $this->mp->getPresensiAdmin()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_presensi', $data);

		$this->load->view('layouts/footer');
	}



	public function detailPresensi()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Detail Rekap Presensi Dosen',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_create' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'vi' => $this->mp->getHonor($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/d_presensi', $data);

		$this->load->view('layouts/footer');
	}



	public function tarifdosen()

	{

		$id_reff = $this->uri->segment(3);

		$nidn = $this->uri->segment(4);

		$data = array(

			'title' => 'Tarif Pengajaran',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_tetap' => 'active',

			'd' => $this->mp->getTarifDosen2($id_reff),

			'v' => $this->mp->editTarifDosen($nidn)

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/t_presensi', $data);

		$this->load->view('layouts/footer');
	}







	public function kelebihan_sks()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Tarif Pengajaran',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_tetap' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/c_kelebihan_sks', $data);

		$this->load->view('layouts/footer');
	}



	public function kelebihan_sks_dt()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Tarif Pengajaran',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_tetap' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/c_kelebihan_sks_dt', $data);

		$this->load->view('layouts/footer');
	}



	public function simpanTarif()

	{

		$id_reff = $this->input->post('id_reff', true);

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$tarif =  htmlspecialchars($this->input->post('tarif', true));

		$total_tarif =  htmlspecialchars($this->input->post('total_tarif', true));

		$log = [

			'log' => "Rekap Kehadiran",

			'email' => $email,

			'date_created' => time()

		];

		$this->db->set('tarif', $tarif);

		$this->db->set('total_tarif', $total_tarif);

		$this->db->where('id_reff', $id_reff);

		$this->db->update('occ_presensi_dosen');



		$this->db->insert('occ_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi/' . $nidn);
	}







	public function mtarif()

	{

		$data = array(

			'title' => 'Manajemen Tarif Pengajaran',

			'active_menu_mtarif' => 'active',

			'v' => $this->mp->getTarifDosen()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_mtarif', $data);

		$this->load->view('layouts/footer');
	}



	public function mtarifDosen()



	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Manajemen Tarif Pengajaran',

			'active_menu_mtarif' => 'active',

			'd' => $this->mp->editTarifDosen2($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/u_mtarif', $data);

		$this->load->view('layouts/footer');
	}



	public function mtarifDosenGo()

	{

		$id = $this->uuid->v4();

		$reff = str_replace('-', '', $id);

		$nidn = $this->input->post('nidn', true);

		$tarif = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('tarif', true)));

		$pinjaman_lkk = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('pinjaman_lkk', true)));

		$sarana_dakwah = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('sarana_dakwah', true)));

		$beban_sks = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('beban_sks', true)));

		$pph_psl = $this->input->post('pph_psl', true);

		$p_zis = $this->input->post('p_zis', true);

		$this->db->set('tarif', $tarif);

		$this->db->set('pinjaman_lkk', $pinjaman_lkk);

		$this->db->set('sarana_dakwah', $sarana_dakwah);

		$this->db->set('beban_sks', $beban_sks);

		$this->db->set('pph_psl', $pph_psl);

		$this->db->set('p_zis', $p_zis);

		$this->db->where('nidn', $nidn);

		$this->db->update('occ_user');

		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/mtarif');
	}



	public function lapPresensi()

	{

		$data = array(

			'title' => 'Dashboard',

			'active_menu_lap_presensi' => 'active',

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/lap_presensi', $data);

		$this->load->view('layouts/footer');
	}







	public function v_lapPresensi()



	{

		$bulan = $this->input->post('bulan', true);

		$tahun =  htmlspecialchars($this->input->post('tahun', true));

		$data = array(

			'title' => 'Laporan Presensi',

			'active_menu_lap_presensi' => 'active',

			'v' => $this->mp->getLapPresensiAdmin($bulan, $tahun)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_lap_presensi', $data);

		$this->load->view('layouts/footer');
	}







	public function listPresensi_dt()

	{

		$data = array(

			'title' => 'List Presensi Dosen Tidak Tetap',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_tidaktetap' => 'active',

			'v' => $this->mp->getPresensiAdmin_dt()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_presensi_dt', $data);

		$this->load->view('layouts/footer');
	}



	public function detailPresensi_dt()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Detail Rekap Presensi Dosen',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_create' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'vi' => $this->mp->getHonor($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)



		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/d_presensi_dt', $data);

		$this->load->view('layouts/footer');
	}







	public function tarifdosen_dt()

	{

		$id_reff = $this->uri->segment(3);

		$nidn = $this->uri->segment(4);

		$data = array(

			'title' => 'Tarif Pengajaran',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_tetap' => 'active',

			'd' => $this->mp->getTarifDosen2($id_reff),

			'v' => $this->mp->editTarifDosen($nidn)

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/t_presensi_dt', $data);

		$this->load->view('layouts/footer');
	}







	public function simpanTarif_dt()

	{

		$id_reff = $this->input->post('id_reff', true);

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$tarif =  htmlspecialchars($this->input->post('tarif', true));

		$total_tarif =  htmlspecialchars($this->input->post('total_tarif', true));

		$log = [

			'log' => "Rekap Kehadiran",

			'email' => $email,

			'date_created' => time()

		];

		$this->db->set('tarif', $tarif);

		$this->db->set('total_tarif', $total_tarif);

		$this->db->where('id_reff', $id_reff);

		$this->db->update('occ_presensi_dosen');

		$this->db->insert('occ_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi_dt/' . $nidn);
	}







	public function simpan_kelebihan_sks()



	{

		$id = $this->uuid->v4();

		$reff = str_replace('-', '', $id);

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$kelebihan_sks =  htmlspecialchars($this->input->post('kelebihan_sks', true));

		$transport =  htmlspecialchars($this->input->post('transport', true));

		$tarif =  htmlspecialchars($this->input->post('tarif', true));

		$total_tarif =  htmlspecialchars($this->input->post('total_tarif', true));

		$jumlah_kehadiran =  htmlspecialchars($this->input->post('jumlah_kehadiran', true));

		$data = [

			'id_reff' => $reff,

			'nidn' =>  $nidn,

			'kelebihan_sks' =>  $kelebihan_sks,

			'jumlah_kehadiran' => $jumlah_kehadiran,

			'transport' => $transport,

			'tarif' => $tarif,

			'total_tarif' => $total_tarif,

			'bulan' => date('m'),

			'tahun' => date('Y'),

			'date_created' => date('Y-m-d H:i:s')

		];

		$log = [

			'log' => "Rekap Kehadiran",

			'email' => $email,

			'date_created' => time()

		];

		$this->db->insert('lap_presensi_dosen', $data);

		$this->db->insert('occ_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi/' . $nidn);
	}



	public function simpan_kelebihan_sks_dt()



	{

		$id = $this->uuid->v4();

		$reff = str_replace('-', '', $id);

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$kelebihan_sks =  htmlspecialchars($this->input->post('kelebihan_sks', true));

		$transport =  htmlspecialchars($this->input->post('transport', true));

		$tarif =  htmlspecialchars($this->input->post('tarif', true));

		$total_tarif =  htmlspecialchars($this->input->post('total_tarif', true));

		$jumlah_kehadiran =  htmlspecialchars($this->input->post('jumlah_kehadiran', true));

		$data = [

			'id_reff' => $reff,

			'nidn' =>  $nidn,

			'kelebihan_sks' =>  $kelebihan_sks,

			'jumlah_kehadiran' => $jumlah_kehadiran,

			'transport' => $transport,

			'tarif' => $tarif,

			'total_tarif' => $total_tarif,

			'bulan' => date('m'),

			'tahun' => date('Y'),

			'date_created' => date('Y-m-d H:i:s')

		];

		$log = [

			'log' => "Rekap Kehadiran",

			'email' => $email,

			'date_created' => time()

		];

		$this->db->insert('lap_presensi_dosen', $data);

		$this->db->insert('occ_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi_dt/' . $nidn);
	}



	public function hapuslappengajaran()



	{

		$no_urut = $this->uri->segment(3);

		$nidn = $this->uri->segment(4);

		$this->db->where('no_urut', $no_urut);

		$this->db->delete('lap_presensi_dosen');

		$this->session->set_flashdata('sukses', 'Di Hapus');

		redirect('adminkeu/detailPresensi/' . $nidn);
	}



	public function hapuslappengajaran_dt()



	{

		$no_urut = $this->uri->segment(3);

		$nidn = $this->uri->segment(4);

		$this->db->where('no_urut', $no_urut);

		$this->db->delete('lap_presensi_dosen');

		$this->session->set_flashdata('sukses', 'Di Hapus');

		redirect('adminkeu/detailPresensi_dt/' . $nidn);
	}



	public function listPresensi_fai()

	{

		$data = array(

			'title' => 'List Presensi Dosen FAI',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fai' => 'active',

			'v' => $this->mp->getPresensiAdmin_fai()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_presensi_fai', $data);

		$this->load->view('layouts/footer');
	}



	public function detailPresensi_fai()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Detail Rekap Presensi Fai',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fai' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'vi' => $this->mp->getHonor($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/d_presensi_fai', $data);

		$this->load->view('layouts/footer');
	}



	public function editSksKehadiran()

	{



		$id_reff =  htmlspecialchars($this->input->post('id_reff', true));

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$sks_keu =  htmlspecialchars($this->input->post('sks_keu', true));

		$jumlah_kehadiran_keu =  htmlspecialchars($this->input->post('jumlah_kehadiran_keu', true));



		$this->db->set('sks_keu', $sks_keu);

		$this->db->set('jumlah_kehadiran_keu', $jumlah_kehadiran_keu);

		$this->db->where('id_reff', $id_reff);

		$this->db->update('occ_presensi_dosen');



		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi_fai/' . $nidn);
	}



	public function listPresensi_fkip()

	{

		$data = array(

			'title' => 'List Presensi Dosen FKIP',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fkip' => 'active',

			'v' => $this->mp->getPresensiAdmin_fkip()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_presensi_fkip', $data);

		$this->load->view('layouts/footer');
	}



	public function detailPresensi_fkip()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Detail Rekap Presensi FKIP',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fkip' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'vi' => $this->mp->getHonor($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/d_presensi_fkip', $data);

		$this->load->view('layouts/footer');
	}



	public function editSksKehadiran_fkip()

	{



		$id_reff =  htmlspecialchars($this->input->post('id_reff', true));

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$sks_keu =  htmlspecialchars($this->input->post('sks_keu', true));

		$jumlah_kehadiran_keu =  htmlspecialchars($this->input->post('jumlah_kehadiran_keu', true));



		$this->db->set('sks_keu', $sks_keu);

		$this->db->set('jumlah_kehadiran_keu', $jumlah_kehadiran_keu);

		$this->db->where('id_reff', $id_reff);

		$this->db->update('occ_presensi_dosen');



		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi_fkip/' . $nidn);
	}



	public function listPresensi_faperta()

	{

		$data = array(

			'title' => 'List Presensi Dosen FAPERTA',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_faperta' => 'active',

			'v' => $this->mp->getPresensiAdmin_faperta()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_presensi_faperta', $data);

		$this->load->view('layouts/footer');
	}



	public function detailPresensi_faperta()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Detail Rekap Presensi FAPERTA',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_faperta' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'vi' => $this->mp->getHonor($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/d_presensi_faperta', $data);

		$this->load->view('layouts/footer');
	}



	public function editSksKehadiran_faperta()

	{



		$id_reff =  htmlspecialchars($this->input->post('id_reff', true));

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$sks_keu =  htmlspecialchars($this->input->post('sks_keu', true));

		$jumlah_kehadiran_keu =  htmlspecialchars($this->input->post('jumlah_kehadiran_keu', true));



		$this->db->set('sks_keu', $sks_keu);

		$this->db->set('jumlah_kehadiran_keu', $jumlah_kehadiran_keu);

		$this->db->where('id_reff', $id_reff);

		$this->db->update('occ_presensi_dosen');



		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi_faperta/' . $nidn);
	}



	public function listPresensi_fahum()

	{

		$data = array(

			'title' => 'List Presensi Dosen FAHUM',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fahum' => 'active',

			'v' => $this->mp->getPresensiAdmin_fahum()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_presensi_fahum', $data);

		$this->load->view('layouts/footer');
	}



	public function detailPresensi_fahum()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Detail Rekap Presensi FAHUM',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fahum' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'vi' => $this->mp->getHonor($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/d_presensi_fahum', $data);

		$this->load->view('layouts/footer');
	}



	public function editSksKehadiran_fahum()

	{



		$id_reff =  htmlspecialchars($this->input->post('id_reff', true));

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$sks_keu =  htmlspecialchars($this->input->post('sks_keu', true));

		$jumlah_kehadiran_keu =  htmlspecialchars($this->input->post('jumlah_kehadiran_keu', true));



		$this->db->set('sks_keu', $sks_keu);

		$this->db->set('jumlah_kehadiran_keu', $jumlah_kehadiran_keu);

		$this->db->where('id_reff', $id_reff);

		$this->db->update('occ_presensi_dosen');



		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi_fahum/' . $nidn);
	}



	public function listPresensi_fisip()

	{

		$data = array(

			'title' => 'List Presensi Dosen FISIP',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fisip' => 'active',

			'v' => $this->mp->getPresensiAdmin_fisip()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_presensi_fisip', $data);

		$this->load->view('layouts/footer');
	}



	public function detailPresensi_fisip()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Detail Rekap Presensi FISIP',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fisip' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'vi' => $this->mp->getHonor($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/d_presensi_fisip', $data);

		$this->load->view('layouts/footer');
	}



	public function editSksKehadiran_fisip()

	{



		$id_reff =  htmlspecialchars($this->input->post('id_reff', true));

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$sks_keu =  htmlspecialchars($this->input->post('sks_keu', true));

		$jumlah_kehadiran_keu =  htmlspecialchars($this->input->post('jumlah_kehadiran_keu', true));



		$this->db->set('sks_keu', $sks_keu);

		$this->db->set('jumlah_kehadiran_keu', $jumlah_kehadiran_keu);

		$this->db->where('id_reff', $id_reff);

		$this->db->update('occ_presensi_dosen');



		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi_fisip/' . $nidn);
	}



	public function listPresensi_feb()

	{

		$data = array(

			'title' => 'List Presensi Dosen FEB',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_feb' => 'active',

			'v' => $this->mp->getPresensiAdmin_feb()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_presensi_feb', $data);

		$this->load->view('layouts/footer');
	}



	public function detailPresensi_feb()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Detail Rekap Presensi FEB',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_feb' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'vi' => $this->mp->getHonor($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/d_presensi_feb', $data);

		$this->load->view('layouts/footer');
	}



	public function editSksKehadiran_feb()

	{



		$id_reff =  htmlspecialchars($this->input->post('id_reff', true));

		$nidn =  htmlspecialchars($this->input->post('nidn', true));

		$sks_keu =  htmlspecialchars($this->input->post('sks_keu', true));

		$jumlah_kehadiran_keu =  htmlspecialchars($this->input->post('jumlah_kehadiran_keu', true));



		$this->db->set('sks_keu', $sks_keu);

		$this->db->set('jumlah_kehadiran_keu', $jumlah_kehadiran_keu);

		$this->db->where('id_reff', $id_reff);

		$this->db->update('occ_presensi_dosen');



		$this->session->set_flashdata('sukses', 'Disimpan');

		redirect('adminkeu/detailPresensi_feb/' . $nidn);
	}



	public function listPresensi_fikti()

	{

		$data = array(

			'title' => 'List Presensi Dosen FIKTI',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fikti' => 'active',

			'v' => $this->mp->getPresensiAdmin_fikti()

		);



		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/v_presensi_fikti', $data);

		$this->load->view('layouts/footer');
	}



	public function detailPresensi_fikti()

	{

		$nidn = $this->uri->segment(3);

		$data = array(

			'title' => 'Detail Rekap Presensi FIKTI',

			'active_menu_presensi' => 'menu-open',

			'active_menu_prs' => 'active',

			'active_menu_presensi_fikti' => 'active',

			'v' => $this->mp->getDetailPresensi($nidn),

			'vi' => $this->mp->getHonor($nidn),

			'd' => $this->mp->getDetailDosen($nidn),

			'jsks' => $this->mp->getJumlahSks($nidn),

			'hadir' => $this->mp->getJumlahKehadiran($nidn),

			'transport' => $this->mp->getJumlahTansport($nidn),

			'tarif' => $this->mp->getJumlahTarif($nidn),

			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),

			'potongan' => $this->mp->getPotongan($nidn),

			'namaDosen' => $this->mp->namaDosen($nidn)

		);

		$this->load->view('layouts/header', $data);

		$this->load->view('adminkeu/d_presensi_fikti', $data);

		$this->load->view('layouts/footer');
	}



	public function editSksKehadiran_fikti()

	{

		$id_reff =  htmlspecialchars($this->input->post('id_reff', true));
		$nidn =  htmlspecialchars($this->input->post('nidn', true));
		$sks_keu =  htmlspecialchars($this->input->post('sks_keu', true));
		$jumlah_kehadiran_keu =  htmlspecialchars($this->input->post('jumlah_kehadiran_keu', true));
		$this->db->set('sks_keu', $sks_keu);
		$this->db->set('jumlah_kehadiran_keu', $jumlah_kehadiran_keu);
		$this->db->where('id_reff', $id_reff);
		$this->db->update('occ_presensi_dosen');
		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('adminkeu/detailPresensi_fikti/' . $nidn);
	}



	public function listPresensi_fatek()

	{

		$data = array(
			'title' => 'List Presensi Dosen FATEK',
			'active_menu_presensi' => 'menu-open',
			'active_menu_prs' => 'active',
			'active_menu_presensi_fatek' => 'active',
			'v' => $this->mp->getPresensiAdmin_fatek()

		);

		$this->load->view('layouts/header', $data);
		$this->load->view('adminkeu/v_presensi_fatek', $data);
		$this->load->view('layouts/footer');
	}

	public function detailPresensi_fatek()

	{
		$nidn = $this->uri->segment(3);
		$data = array(
			'title' => 'Detail Rekap Presensi FATEK',
			'active_menu_presensi' => 'menu-open',
			'active_menu_prs' => 'active',
			'active_menu_presensi_fatek' => 'active',
			'v' => $this->mp->getDetailPresensi($nidn),
			'vi' => $this->mp->getHonor($nidn),
			'd' => $this->mp->getDetailDosen($nidn),
			'jsks' => $this->mp->getJumlahSks($nidn),
			'hadir' => $this->mp->getJumlahKehadiran($nidn),
			'transport' => $this->mp->getJumlahTansport($nidn),
			'tarif' => $this->mp->getJumlahTarif($nidn),
			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),
			'potongan' => $this->mp->getPotongan($nidn),
			'namaDosen' => $this->mp->namaDosen($nidn)
		);
		$this->load->view('layouts/header', $data);
		$this->load->view('adminkeu/d_presensi_fatek', $data);
		$this->load->view('layouts/footer');
	}

	public function editSksKehadiran_fatek()

	{

		$id_reff =  htmlspecialchars($this->input->post('id_reff', true));
		$nidn =  htmlspecialchars($this->input->post('nidn', true));
		$sks_keu =  htmlspecialchars($this->input->post('sks_keu', true));
		$jumlah_kehadiran_keu =  htmlspecialchars($this->input->post('jumlah_kehadiran_keu', true));
		$this->db->set('sks_keu', $sks_keu);
		$this->db->set('jumlah_kehadiran_keu', $jumlah_kehadiran_keu);
		$this->db->where('id_reff', $id_reff);
		$this->db->update('occ_presensi_dosen');
		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('adminkeu/detailPresensi_fatek/' . $nidn);
	}


	public function listPresensi_pns()
	{
		$data = array(
			'title' => 'List Presensi Dosen PNS/NIDK',
			'active_menu_presensi' => 'menu-open',
			'active_menu_prs' => 'active',
			'active_menu_presensi_pns' => 'active',
			'v' => $this->mp->getPresensiAdmin_pns()
		);

		$this->load->view('layouts/header', $data);
		$this->load->view('adminkeu/v_presensi_pns', $data);
		$this->load->view('layouts/footer');
	}
	public function detailPresensi_pns()

	{
		$nidn = $this->uri->segment(3);
		$data = array(
			'title' => 'Detail Rekap Presensi PNS/NIDK',
			'active_menu_presensi' => 'menu-open',
			'active_menu_prs' => 'active',
			'active_menu_presensi_pns' => 'active',
			'v' => $this->mp->getDetailPresensi($nidn),
			'vi' => $this->mp->getHonor($nidn),
			'd' => $this->mp->getDetailDosen($nidn),
			'jsks' => $this->mp->getJumlahSks($nidn),
			'hadir' => $this->mp->getJumlahKehadiran($nidn),
			'transport' => $this->mp->getJumlahTansport($nidn),
			'tarif' => $this->mp->getJumlahTarif($nidn),
			'total_tarif' => $this->mp->getJumlahTotalTarif($nidn),
			'potongan' => $this->mp->getPotongan($nidn),
			'namaDosen' => $this->mp->namaDosen($nidn)
		);
		$this->load->view('layouts/header', $data);
		$this->load->view('adminkeu/d_presensi_pns', $data);
		$this->load->view('layouts/footer');
	}
}
