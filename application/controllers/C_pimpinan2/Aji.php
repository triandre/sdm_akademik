<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AJi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan2/Model_aji', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vaji' => 'active',
            'aji' => $this->ma->getAji(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/aji/v_aji', $data);
        $this->load->view('layouts/footer');
    }

    public function fee()
    {
        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Persetujuan Usulan Insentif Publikasi Jurnal',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vaji' => 'active',
            'aji' => $this->ma->getAji(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/aji/s_aji', $data);
        $this->load->view('layouts/footer');
    }

    public function feeGo()
    {

        $id = $this->input->post('id', true);
        $catatan2 = $this->input->post('catatan2', true);
        $insentif_disetujui = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('insentif_disetujui', true)));
        $sts = 4;
        $date_wr2 = date('Y-m-d H:i:s');


        $this->db->set('date_wr2', $date_wr2);
        $this->db->set('sts', $sts);
        $this->db->set('catatan2', $catatan2);
        $this->db->set('insentif_disetujui', $insentif_disetujui);
        $this->db->where('id', $id);
        $this->db->update('ins_aji');

        $log = [
            'log' => "Pak Wr2 Setujui Insentif Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan2/vaji');
    }

    public function tolak()
    {
        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Penolakan Usulan Insentif Publikasi Jurnal',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vaji' => 'active',
            'aji' => $this->ma->getAji(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/aji/t_aji', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $catatan2 = $this->input->post('catatan2', true);
        $sts = 6;
        $date_wr2 = date('Y-m-d H:i:s');

        $this->db->set('date_wr2', $date_wr2);
        $this->db->set('sts', $sts);
        $this->db->set('catatan2', $catatan2);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_aji');

        $log = [
            'log' => "Pak WR 2 Menolak Insentif Jurnal Ilmiah Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan2/vaji');
    }
}
