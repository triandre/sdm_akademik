<!-- INSENTIF HKI -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bhki extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan2/Model_bhki', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan HKI',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbhki' => 'active',
            'bhki' => $this->ma->getBhki(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/bhki/v_bhki', $data);
        $this->load->view('layouts/footer');
    }

    public function history()
    {
        $email = $this->uri->segment(4);
        $data = array(
            'title' => 'Histori Bantuan Biaya HKI',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbhki' => 'active',
            'v' => $this->ma->getBhki(),
            'h' => $this->ma->getHistory($email),
            'akun' => $this->mu->getUser(),
        );

        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/bhki/h_bhki', $data);
        $this->load->view('layouts/footer');
    }

    public function fee()
    {
        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Persetujuan Usulan Bantuan HKI',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vhbki' => 'active',
            'bhki' => $this->ma->getBhki(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/bhki/s_bhki', $data);
        $this->load->view('layouts/footer');
    }

    public function feeGo()
    {
        $id = $this->input->post('id', true);
        $catatan2 = $this->input->post('catatan2', true);
        $insentif_disetujui = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('insentif_disetujui', true)));
        $sts = 4;
        $stempel = 1;
        $date_wr2 = date('Y-m-d H:i:s');

        $this->db->set('insentif_disetujui', $insentif_disetujui);
        $this->db->set('date_wr2', $date_wr2);
        $this->db->set('sts', $sts);
        $this->db->set('stempel', $stempel);
        $this->db->set('catatan2', $catatan2);
        $this->db->where('id', $id);
        $this->db->update('ins_bhki');

        $log = [
            'log' => "Pak WR Setujui Bantuan HKI Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan2/vbhki');
    }


    public function tolak()
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Penolakan Usulan Bantuan HKI',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbhki' => 'active',
            'bhki' => $this->ma->getBhki(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/bhki/t_bhki', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $tolak2 = $this->input->post('tolak2', true);
        $sts = 6;
        $date_wr2 = date('Y-m-d H:i:s');

        $this->db->set('date_wr2', $date_wr2);
        $this->db->set('sts', $sts);
        $this->db->set('tolak2', $tolak2);
        $this->db->where('id', $id);
        $this->db->update('ins_bhki');

        $log = [
            'log' => "Pak WR 2 Menolak Insentif HKI ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan2/vbhki');
    }
}
