<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bfio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan2/Model_bfio', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bfio/v_bfio', $data);
        $this->load->view('layouts/footer');
    }

    public function tolak()
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/bfio/t_bfio', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $tolak2 = $this->input->post('tolak2', true);
        $sts = 6;
        $date_wr2 = date('Y-m-d H:i:s');
        $this->db->set('date_wr2', $date_wr2);
        $this->db->set('sts', $sts);
        $this->db->set('tolak2', $tolak2);
        $this->db->where('id', $id);
        $this->db->update('ins_bfio');

        $log = [
            'log' => "Pak WR2 Menolak Form Usulan Bantuan Mengikuti Seminar Internasional/Nasional  ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan2/vbfio');
    }

    public function fee()
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/bfio/s_bfio', $data);
        $this->load->view('layouts/footer');
    }


    public function feeGo()
    {

        $id = $this->input->post('id', true);
        $biaya_pendaftaran = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('biaya_pendaftaran', true)));
        $insentif_tambahan = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('insentif_tambahan', true)));
        $catatan2 = $this->input->post('catatan2', true);
        $sts = 4;
        $stempel = 1;
        $date_wr2 = date('Y-m-d H:i:s');

        $this->db->set('biaya_pendaftaran', $biaya_pendaftaran);
        $this->db->set('insentif', $biaya_pendaftaran);
        $this->db->set('insentif_tambahan', $insentif_tambahan);
        $this->db->set('date_wr2', $date_wr2);
        $this->db->set('sts', $sts);
        $this->db->set('stempel', $stempel);
        $this->db->set('catatan2', $catatan2);
        $this->db->where('id', $id);
        $this->db->update('ins_bfio');

        $log = [
            'log' => "Pak Wr2 Setujui Insentif Form Usulan Bantuan Mengikuti Seminar Internasional/Nasional ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan2/vbfio');
    }
}
