<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_Pengumuman extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/ModelDiklat', 'mdk');
        $this->load->model('m_master/ModelPangkat', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Pengumuman',
            'active_menu_pengumuman' => 'active'

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pengumuman/index', $data);
        $this->load->view('layouts/footer');
    }
}
