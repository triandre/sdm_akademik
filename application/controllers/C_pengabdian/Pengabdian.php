<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengabdian extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pengabdian/Model_pengabdian', 'mpg');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Riwayat Pengabdian Dosen',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengabdian' => 'active',
            'v' => $this->mpg->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pengabdian/pengabdian/v_pengabdian', $data);
        $this->load->view('layouts/footer');
    }

    public function createPemdos()
    {
        $data = array(
            'title' => 'Data Bimbingan Dosen',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengabdian' => 'active',
            'v' => $this->mpg->getView()
        );

        $this->form_validation->set_rules('nidn_pembimbing', 'NIDN Pembimbing', 'required|trim');
        $this->form_validation->set_rules('nama_pembimbing', 'Nama Pembimbing', 'required|trim');
        $this->form_validation->set_rules('nama_bimbingan', 'Nama Bimbingan', 'required|trim');
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'required|trim');
        $this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/pel_pendidikan/bim_dosen/c_bim_dosen', $data);
            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();
            $reff_bdos = str_replace('-', '', $id);
            $nidn = $this->session->userdata('nidn');
            //UPLOAD
            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/bimbingan_dosen/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['occ_bimbingan_dosen']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/bimbingan_dosen/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $data = array(
                'reff_bdos' => $reff_bdos,
                'nidn' => $nidn,
                'nidn_pembimbing' => htmlspecialchars($this->input->post('nidn_pembimbing', true)),
                'nama_pembimbing' => htmlspecialchars($this->input->post('nama_pembimbing', true)),
                'nama_bimbingan' => htmlspecialchars($this->input->post('nama_bimbingan', true)),
                'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
                'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );
            $result = $this->mpg->storePemdos($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('pemdos');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createPemdos');
            }
        }
    }

    public function detailPemdos()
    {
        $reff_bdos = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Bimbingan Dosen',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengabdian' => 'active',
            'v' => $this->mpg->getView(),
            'd' => $this->mpg->getDetail($reff_bdos)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/bim_dosen/d_bim_dosen', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanPemdos()
    {
        $reff_bdos = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Bimbingan Dosen',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengabdian' => 'active',
            'v' => $this->mpg->getView(),
            'd' => $this->mpg->getDetail($reff_bdos)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/bim_dosen/u_bim_dosen', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanPemdosGo()
    {

        $data = array(
            'title' => 'Data Bimbingan Dosen',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengabdian' => 'active',
            'v' => $this->mpg->getView()
        );

        $reff_bdos = htmlspecialchars($this->input->post('reff_bdos', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/bimbingan_dosen/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_bimbingan_dosen']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/bimbingan_dosen/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'reff_bdos' => $reff_bdos,
            'nidn_pembimbing' => htmlspecialchars($this->input->post('nidn_pembimbing', true)),
            'nama_pembimbing' => htmlspecialchars($this->input->post('nama_pembimbing', true)),
            'nama_bimbingan' => htmlspecialchars($this->input->post('nama_bimbingan', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
            'file' => $upload1,
            'aktif' => 1
        );


        $result = $this->mpg->updatePemdos($reff_bdos, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('pemdos');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('pemdos/perbaikanPemdos/' . $reff_bdos);
        }
    }

    public function hapusPemdos($reff_bdos)
    {
        $reff_bdos = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('reff_bdos', $reff_bdos);
        $this->db->update('occ_bimbingan_dosen');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pemdos');
    }
}
