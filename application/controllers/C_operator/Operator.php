<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Operator extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->library('uuid');
        $this->load->model('m_presensi/ModelPresensi', 'mp');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_db' => 'active'
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('operator/index', $data);
        $this->load->view('layouts/footer');
    }



    public function listPresensi()

    {

        $data = array(

            'title' => 'List Presensi',

            'active_menu_presensi' => 'menu-open',

            'active_menu_prs' => 'active',

            'active_menu_presensi_listData' => 'active',

            'v' => $this->mp->getsetDosen()

        );

        $this->load->view('layouts/header', $data);

        $this->load->view('operator/v_presensi', $data);

        $this->load->view('layouts/footer');
    }



    public function detailpengajaran()

    {

        $nidn = $this->uri->segment(3);

        $data = array(

            'title' => 'List Presensi',

            'active_menu_presensi' => 'menu-open',

            'active_menu_prs' => 'active',

            'active_menu_presensi_listData' => 'active',

            'v' => $this->mp->detailpengajaran($nidn),

            'namaDosen' => $this->mp->namaDosen($nidn)

        );

        $this->load->view('layouts/header', $data);

        $this->load->view('operator/d_pengajaran', $data);

        $this->load->view('layouts/footer');
    }



    public function tambahpengajaran()

    {

        $nidn = $this->uri->segment(3);

        $data = array(

            'title' => 'Tambah Pengajaran',

            'active_menu_presensi' => 'menu-open',

            'active_menu_prs' => 'active',

            'active_menu_presensi_listData' => 'active',

            'namaDosen' => $this->mp->namaDosen($nidn)

        );

        $this->load->view('layouts/header', $data);

        $this->load->view('operator/c_presensi', $data);

        $this->load->view('layouts/footer');
    }



    public function savepengajaran()

    {



        $this->form_validation->set_rules('nidn', 'Nama Dosen', 'required|trim');

        $this->form_validation->set_rules('matakuliah', 'Matakuliah', 'required|trim');

        $this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');

        $this->form_validation->set_rules('semester', 'SKS', 'required|trim');

        $this->form_validation->set_rules('jumlah_kehadiran', 'Jumlah Kehadiran', 'required|trim');



        if ($this->form_validation->run() == false) {

            $this->load->view('layouts/header', $data);

            $this->load->view('operator/c_presensi', $data);

            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();

            $reff = str_replace('-', '', $id);

            $email = $this->session->userdata('email');

            $uraian = $this->input->post('uraian', true);

            $nidn =  htmlspecialchars($this->input->post('nidn', true));

            $jumlah_kehadiran =  htmlspecialchars($this->input->post('jumlah_kehadiran', true));

            $ongkos = "25000";

            if ($uraian == "Malam") {

                $transport = $ongkos * $jumlah_kehadiran;
            } else {

                $transport = "0";
            }



            $data = [

                'id_reff' => $reff,

                'nidn' =>  $nidn,

                'matakuliah' =>  htmlspecialchars($this->input->post('matakuliah', true)),

                'kelas' => $this->input->post('kelas', true),

                'uraian' => $uraian,

                'semester' =>  htmlspecialchars($this->input->post('semester', true)),

                'sks' =>  htmlspecialchars($this->input->post('sks', true)),

                'jumlah_kehadiran' => $jumlah_kehadiran,

                'transport' => $transport,

                'tarif' => '0',

                'total_tarif' => '0',

                'email_created' => $email,

                'bulan' => date('m'),

                'tahun' => date('Y'),

                'date_created' => date('Y-m-d H:i:s')

            ];



            $log = [

                'log' => "Rekap Kehadiran",

                'email' => $email,

                'date_created' => time()

            ];



            $this->db->insert('occ_presensi_dosen', $data);

            $this->db->insert('occ_log', $log);



            $this->session->set_flashdata('sukses', 'Disimpan');

            redirect('presensi/detailpengajaran/' . $nidn);
        }
    }







    public function createPresensi()

    {

        $data = array(

            'title' => 'Rekap Presensi Dosen',

            'active_menu_presensi' => 'menu-open',

            'active_menu_prs' => 'active',

            'active_menu_presensi_create' => 'active',

            'dosen' => $this->mp->getDosen()

        );





        $this->form_validation->set_rules('nidn', 'Nama Dosen', 'required|trim');

        $this->form_validation->set_rules('matakuliah', 'Matakuliah', 'required|trim');

        $this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');

        $this->form_validation->set_rules('semester', 'SKS', 'required|trim');

        $this->form_validation->set_rules('jumlah_kehadiran', 'Jumlah Kehadiran', 'required|trim');



        if ($this->form_validation->run() == false) {

            $this->load->view('layouts/header', $data);

            $this->load->view('operator/c_presensi', $data);

            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();

            $reff = str_replace('-', '', $id);

            $email = $this->session->userdata('email');

            $uraian = $this->input->post('uraian', true);

            $jumlah_kehadiran =  htmlspecialchars($this->input->post('jumlah_kehadiran', true));

            $ongkos = "25000";

            if ($uraian == "Malam") {

                $transport = $ongkos * $jumlah_kehadiran;
            } else {

                $transport = "0";
            }



            $data = [

                'id_reff' => $reff,

                'nidn' =>  htmlspecialchars($this->input->post('nidn', true)),

                'matakuliah' =>  htmlspecialchars($this->input->post('matakuliah', true)),

                'kelas' => $this->input->post('kelas', true),

                'uraian' => $uraian,

                'semester' =>  htmlspecialchars($this->input->post('semester', true)),

                'sks' =>  htmlspecialchars($this->input->post('sks', true)),

                'jumlah_kehadiran' => $jumlah_kehadiran,

                'transport' => $transport,

                'tarif' => '0',

                'total_tarif' => '0',

                'email_created' => $email,

                'tahun' => date('Y'),

                'date_created' => date('Y-m-d H:i:s')

            ];



            $log = [

                'log' => "Rekap Kehadiran",

                'email' => $email,

                'date_created' => time()

            ];



            $this->db->insert('occ_presensi_dosen', $data);

            $this->db->insert('occ_log', $log);



            $this->session->set_flashdata('sukses', 'Disimpan');

            redirect('presensi/listPresensi');
        }
    }



    public function ubahPresensi()

    {

        $id_reff = $this->uri->segment(3);

        $data = array(

            'title' => 'Ubah Rekap Presensi Dosen',

            'active_menu_presensi' => 'menu-open',

            'active_menu_prs' => 'active',

            'active_menu_presensi_create' => 'active',

            'd' => $this->mp->getDetail($id_reff)



        );

        $this->load->view('layouts/header', $data);

        $this->load->view('operator/u_presensi', $data);

        $this->load->view('layouts/footer');
    }



    public function ubahPresensiGo()

    {

        $data = array(

            'title' => 'Ubah Rekap Presensi Dosen',

            'active_menu_presensi' => 'menu-open',

            'active_menu_prs' => 'active',

            'active_menu_presensi_create' => 'active'

        );



        $id_reff = htmlspecialchars($this->input->post('id_reff', true));

        $nidn = htmlspecialchars($this->input->post('nidn', true));

        $data = array(

            'matakuliah' =>  htmlspecialchars($this->input->post('matakuliah', true)),

            'kelas' => $this->input->post('kelas', true),

            'semester' =>  htmlspecialchars($this->input->post('semester', true)),

            'sks' =>  htmlspecialchars($this->input->post('sks', true)),

            'jumlah_kehadiran' =>  htmlspecialchars($this->input->post('jumlah_kehadiran', true)),

        );





        $result = $this->mp->ubahPresensiGo($id_reff, $data);



        if ($result >= 1) {

            $this->session->set_flashdata('sukses', 'Diubah');

            redirect('presensi/detailpengajaran/' . $nidn);
        } else {

            $this->session->set_flashdata('gagal', 'Diubah');

            redirect('presensi/ubahPresensi/' . $id_reff);
        }
    }



    public function hapusPresensi()

    {

        $id_reff = $this->uri->segment(3);

        $nidn = $this->uri->segment(4);

        $this->db->where('id_reff', $id_reff);

        $this->db->delete('occ_presensi_dosen');



        $this->session->set_flashdata('sukses', 'Disimpan');

        redirect('presensi/detailpengajaran/' . $nidn);
    }



    public function hapus_setDosen()

    {

        $id_set = $this->uri->segment(3);

        $this->db->where('id_set', $id_set);

        $this->db->delete('occ_set_dosen');



        $this->session->set_flashdata('sukses', 'Disimpan');

        redirect('presensi/setDosen');
    }



    public function lapPresensi()

    {

        $data = array(

            'title' => 'Dashboard',

            'active_menu_lap_presensi' => 'active',



        );

        $this->load->view('layouts/header', $data);

        $this->load->view('operator/lap_presensi', $data);

        $this->load->view('layouts/footer');
    }



    public function v_lapPresensi()

    {

        $bulan = $this->input->post('bulan', true);

        $tahun =  htmlspecialchars($this->input->post('tahun', true));

        $data = array(

            'title' => 'Laporan Presensi',

            'active_menu_lap_presensi' => 'active',

            'v' => $this->mp->getLapPresensiOperator($bulan, $tahun)

        );

        $this->load->view('layouts/header', $data);

        $this->load->view('operator/v_lap_presensi', $data);

        $this->load->view('layouts/footer');
    }



    public function setDosen()

    {

        $data = array(

            'title' => 'Set Dosen ',

            'active_menu_presensi' => 'menu-open',

            'active_menu_prs' => 'active',

            'active_menu_setDosen' => 'active',

            'v' => $this->mp->getsetDosen()

        );

        $this->load->view('layouts/header', $data);

        $this->load->view('operator/v_set_dosen', $data);

        $this->load->view('layouts/footer');
    }



    public function invateDosen()

    {

        $data = array(

            'title' => 'Invate Dosen ',

            'active_menu_presensi' => 'menu-open',

            'active_menu_prs' => 'active',

            'active_menu_setDosen' => 'active',

            'dosen' => $this->mp->getDosen()

        );

        $this->load->view('layouts/header', $data);

        $this->load->view('operator/c_set_dosen', $data);

        $this->load->view('layouts/footer');
    }



    public function c_invateDosen()

    {



        $this->form_validation->set_rules('nidn', 'Nidn', 'required|trim');

        $email = $this->session->userdata('email');

        $nidn = htmlspecialchars($this->input->post('nidn', true));

        $cekdosen = $this->db->query("SELECT * FROM occ_set_dosen WHERE email_created='$email' AND nidn='$nidn'")->num_rows();

        if ($cekdosen >= '1') {

            $this->session->set_flashdata('gagal_store', '<div class="alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>INFO!</strong> Mohon Maaf Data Sudah Ada!</div>');

            redirect('presensi/invateDosen');
        } else {



            $data = [

                'nidn' =>  $nidn,

                'email_created' => $email,

                'date_created' => date('Y-m-d H:i:s')

            ];





            $this->db->insert('occ_set_dosen', $data);



            $this->session->set_flashdata('sukses', 'Disimpan');

            redirect('presensi/invateDosen');
        }
    }
}
