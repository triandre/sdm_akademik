<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tunjangan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_reward/ModelTunjangan', 'mt');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Tunjangan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_tunjangan' => 'active',
            'tunjangan' => $this->mt->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/tunjangan/v_tunjangan', $data);
        $this->load->view('layouts/footer');
    }

    public function createTunjangan()
    {
        $data = array(
            'title' => 'Tunjangan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_tunjangan' => 'active',
            'tunjangan' => $this->mt->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/tunjangan/c_tunjangan', $data);
        $this->load->view('layouts/footer');
    }

    public function saveTunjangan()
    {
        $data = array(
            'title' => 'Tunjangan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_tunjangan' => 'active',
            'tunjangan' => $this->mt->getView()

        );
        $nidn = $this->session->userdata('nidn');

        $data = array(
            'nidn' => $nidn,
            'jenis_tunjangan' => htmlspecialchars($this->input->post('jenis_tunjangan', true)),
            'nama_tunjangan' => htmlspecialchars($this->input->post('nama_tunjangan', true)),
            'instansi' => htmlspecialchars($this->input->post('instansi', true)),
            'sumber_dana' => htmlspecialchars($this->input->post('sumber_dana', true)),
            'tahun_mulai' => htmlspecialchars($this->input->post('tahun_mulai', true)),
            'tahun_selesai' => htmlspecialchars($this->input->post('tahun_selesai', true)),
            'nominal' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('nominal', true))),
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s')
        );


        $result = $this->mt->storeTunjangan($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('tunjangan');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createTunjangan');
        }
    }


    public function hapusTunjangan($id_tunjangan)
    {
        $id_tunjangan = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_tunjangan', $id_tunjangan);
        $this->db->update('occ_tunjangan');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('tunjangan');
    }

    public function detailTunjangan()
    {
        $id_tunjangan = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Tunjangan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_tunjangan' => 'active',
            'd' => $this->mt->detailTunjangan($id_tunjangan),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/tunjangan/d_tunjangan', $data);
        $this->load->view('layouts/footer');
    }

    public function editTunjangan()
    {
        $id_tunjangan = $this->uri->segment(3);
        $data = array(
            'title' => 'Update Tunjangan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_tunjangan' => 'active',
            'd' => $this->mt->detailTunjangan($id_tunjangan),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/tunjangan/u_tunjangan', $data);
        $this->load->view('layouts/footer');
    }

    public function editTunjanganGo()
    {

        $data = array(
            'title' => 'Update Tunjangan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_tunjangan' => 'active',
        );

        $id_tunjangan = htmlspecialchars($this->input->post('id_tunjangan', true));

        $data = array(
            'jenis_tunjangan' => htmlspecialchars($this->input->post('jenis_tunjangan', true)),
            'instansi' => htmlspecialchars($this->input->post('instansi', true)),
            'nama_tunjangan' => htmlspecialchars($this->input->post('nama_tunjangan', true)),
            'sumber_dana' => htmlspecialchars($this->input->post('sumber_dana', true)),
            'tahun_mulai' => htmlspecialchars($this->input->post('tahun_mulai', true)),
            'tahun_selesai' => htmlspecialchars($this->input->post('tahun_selesai', true)),
            'nominal' => htmlspecialchars($this->input->post('nominal', true)),
            'aktif' => 1,
        );


        $result = $this->mt->updateTunjangan($id_tunjangan, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('tunjangan');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('tunjangan/edittunjangan/' . $id_tunjangan);
        }
    }
}
