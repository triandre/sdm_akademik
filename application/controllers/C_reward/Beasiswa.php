<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beasiswa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_reward/ModelBeasiswa', 'mb');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Beasiswa',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_beasiswa' => 'active',
            'beasiswa' => $this->mb->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/beasiswa/v_beasiswa', $data);
        $this->load->view('layouts/footer');
    }

    public function createBeasiswa()
    {
        $data = array(
            'title' => 'Beasiswa',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_beasiswa' => 'active',
            'beasiswa' => $this->mb->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/beasiswa/c_beasiswa', $data);
        $this->load->view('layouts/footer');
    }

    public function saveBeasiswa()
    {
        $data = array(
            'title' => 'Beasiswa',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_beasiswa' => 'active',
            'beasiswa' => $this->mb->getView()

        );
        $nidn = $this->session->userdata('nidn');

        $data = array(
            'nidn' => $nidn,
            'jenis_beasiswa' => htmlspecialchars($this->input->post('jenis_beasiswa', true)),
            'nama_beasiswa' => htmlspecialchars($this->input->post('nama_beasiswa', true)),
            'tahun_mulai' => htmlspecialchars($this->input->post('tahun_mulai', true)),
            'tahun_selesai' => htmlspecialchars($this->input->post('tahun_selesai', true)),
            'masih_terima' => htmlspecialchars($this->input->post('masih_terima', true)),
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s')
        );


        $result = $this->mb->storeBeasiswa($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('beasiswa');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createBeasiswa');
        }
    }


    public function hapusBeasiswa($id_beasiswa)
    {
        $id_beasiswa = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_beasiswa', $id_beasiswa);
        $this->db->update('occ_beasiswa');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('beasiswa');
    }

    public function detailBeasiswa()
    {
        $id_beasiswa = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Beasiswa',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_beasiswa' => 'active',
            'd' => $this->mb->detailBeasiswa($id_beasiswa),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/beasiswa/d_beasiswa', $data);
        $this->load->view('layouts/footer');
    }

    public function editBeasiswa()
    {
        $id_beasiswa = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Beasiswa',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_beasiswa' => 'active',
            'd' => $this->mb->detailBeasiswa($id_beasiswa),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/beasiswa/u_beasiswa', $data);
        $this->load->view('layouts/footer');
    }

    public function editBeasiswaGo()
    {

        $data = array(
            'title' => 'Beasiswa',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_beasiswa' => 'active',
            'beasiswa' => $this->mb->getView()
        );
        $id_beasiswa = htmlspecialchars($this->input->post('id_beasiswa', true));

        $data = array(
            'jenis_beasiswa' => htmlspecialchars($this->input->post('jenis_beasiswa', true)),
            'nama_beasiswa' => htmlspecialchars($this->input->post('nama_beasiswa', true)),
            'tahun_mulai' => htmlspecialchars($this->input->post('tahun_mulai', true)),
            'tahun_selesai' => htmlspecialchars($this->input->post('tahun_selesai', true)),
            'masih_terima' => htmlspecialchars($this->input->post('masih_terima', true)),
            'aktif' => 1
        );


        $result = $this->mb->updateBeasiswa($id_beasiswa, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('beasiswa');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('beasiswa/editBeasiswa/' . $id_beasiswa);
        }
    }
}
