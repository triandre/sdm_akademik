<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ifip extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_ifip', 'mif');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Forum Ilmiah (Proceeding)',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_ifip' => 'active',
            'ifip' => $this->mif->getIfip(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/ifip/v_ifip', $data);
        $this->load->view('layouts/footer');
    }

    public function createIfip()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Forum Ilmiah (Proceeding)',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_ifip' => 'active',
            'ifip' => $this->mif->getIfip(),
            'akun' => $this->mu->getUser()
        );

        $this->form_validation->set_rules('kontribusi', 'Kontribusi', 'required|trim');
        $this->form_validation->set_rules('kategori', 'Kategori', 'required|trim');
        $this->form_validation->set_rules('nama_forum', 'Nama Forum', 'required|trim');
        $this->form_validation->set_rules('institusi_penyelenggara', 'institusi penyelenggara', 'required|trim');
        $this->form_validation->set_rules('lokasi', 'lokasi', 'required|trim');
        $this->form_validation->set_rules('tgl_pelaksanaan', 'tglpelaksanaan', 'required|trim');
        $this->form_validation->set_rules('url_web_forum', 'url web forum', 'required|trim');
        $this->form_validation->set_rules('judul_artikel_proseding', 'judul artikel proseding', 'required|trim|is_unique[ins_ifip.judul_artikel_proseding]', [
            'is_unique' => 'Judul Artikel Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('url_artikel_proseding_publikasi', 'url artikel proseding publikasi', 'required|trim');
        $this->form_validation->set_rules('issn', 'ISSN', 'required|trim');
        $this->form_validation->set_rules('tahun_publikasi_proseding', 'tahun publikasi proseding', 'required|trim');
        $this->form_validation->set_rules('lembaga_pengindeks', 'lembaga pengindeks', 'required|trim');
        $this->form_validation->set_rules('nama_program', 'nama program', 'required|trim');
        $this->form_validation->set_rules('tahun_penelitian', 'tahun penelitian', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/publis/ifip/c_ifip', $data);
            $this->load->view('layouts/footer');
        } else {
            $email = $this->session->userdata('email');
            $kategori = htmlspecialchars($this->input->post('kategori'));

            $internasional = $this->db->query("SELECT * FROM ins_ifip WHERE aktif=1 AND email='$email' AND batch=6")->num_rows();
            if ($internasional >= '1') {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Skim Anda sudah Melebihi Batas</div>');
                redirect('ifip');
            } else {

                $upload_image = $_FILES['upload_artikel_proseding']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_artikel_proseding')) {
                        $old_image = $data['ins_ifip']['upload_artikel_proseding'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload1 = $this->upload->data('file_name');
                        $this->db->set('upload_artikel_proseding', $upload1);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }
                $upload_image = $_FILES['upload_loa']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_loa')) {
                        $old_image = $data['ins_ifip']['upload_loa'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload2 = $this->upload->data('file_name');
                        $this->db->set('upload_loa', $upload2);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }

                $data = [
                    'kontribusi' => htmlspecialchars($this->input->post('kontribusi', true)),
                    'kategori' => htmlspecialchars($this->input->post('kategori', true)),
                    'nama_forum' => htmlspecialchars($this->input->post('nama_forum', true)),
                    'institusi_penyelenggara' => htmlspecialchars($this->input->post('institusi_penyelenggara', true)),
                    'lokasi' => htmlspecialchars($this->input->post('lokasi', true)),
                    'tgl_pelaksanaan' => htmlspecialchars($this->input->post('tgl_pelaksanaan', true)),
                    'url_web_forum' => htmlspecialchars($this->input->post('url_web_forum', true)),
                    'judul_artikel_proseding' => htmlspecialchars($this->input->post('judul_artikel_proseding', true)),
                    'url_artikel_proseding_publikasi' => htmlspecialchars($this->input->post('url_artikel_proseding_publikasi', true)),
                    'issn' => htmlspecialchars($this->input->post('issn', true)),
                    'tahun_publikasi_proseding' => htmlspecialchars($this->input->post('tahun_publikasi_proseding', true)),
                    'lembaga_pengindeks' => htmlspecialchars($this->input->post('lembaga_pengindeks', true)),
                    'nama_program' => htmlspecialchars($this->input->post('nama_program', true)),
                    'tahun_penelitian' => htmlspecialchars($this->input->post('tahun_penelitian', true)),
                    'email' => $email,
                    'sts' => 1,
                    'batch' => 6,
                    'aktif' => 1,
                    'upload_artikel_proseding' => $upload1,
                    'upload_loa' => $upload2,
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Membuat Insentif Forum Ilmiah (Proceeding)",
                    'email' => $email,
                    'date_created' => time()
                ];

                $this->db->insert('ins_ifip', $data);
                $this->db->insert('occ_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('ifip');
            }
        }
    }



    public function detailIfip($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Usulan Insentif Forum Ilmiah (Proceeding)',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_ifip' => 'active',
            'ifip' => $this->mif->getIfip(),
            'akun' => $this->mu->getUser(),
            'd' => $this->mif->getDetail($id)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/ifip/d_ifip', $data);
        $this->load->view('layouts/footer');
    }


    public function nonaktif($id)
    {

        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('ins_aji');


        $log = [
            'log' => "Menghapus ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('aji');
    }



    public function perbaikan()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detata Usulan Insentif Forum Ilmiah (Proceeding)',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_ifip' => 'active',
            'ifip' => $this->mif->getIfip(),
            'd' => $this->mif->getDetail($id),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/ifip/u_ifip', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanGo()
    {
        $data = array(
            'title' => 'Detata Usulan Insentif Forum Ilmiah (Proceeding)',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_ifip' => 'active',
            'ifip' => $this->mif->getIfip(),
            'akun' => $this->mu->getUser()
        );
        $kontribusi = $this->input->post('kontribusi', true);
        $kategori = $this->input->post('kategori', true);
        $nama_forum = $this->input->post('nama_forum', true);
        $institusi_penyelenggara = $this->input->post('institusi_penyelenggara', true);
        $lokasi = $this->input->post('lokasi', true);
        $tgl_pelaksanaan = $this->input->post('tgl_pelaksanaan', true);
        $url_web_forum = $this->input->post('url_web_forum', true);
        $judul_artikel_proseding = $this->input->post('judul_artikel_proseding', true);
        $url_artikel_proseding_publikasi = $this->input->post('url_artikel_proseding_publikasi', true);
        $issn = $this->input->post('issn', true);
        $tahun_publikasi_proseding = $this->input->post('tahun_publikasi_proseding', true);
        $lembaga_pengindeks = $this->input->post('lembaga_pengindeks', true);
        $nama_program = $this->input->post('nama_program', true);
        $tahun_penelitian = $this->input->post('tahun_penelitian', true);
        $id = $this->input->post('id', true);
        $sts = 1;
        $batch = 6;
        $email = $this->session->userdata('email');
        $date_created = date('Y-m-d h:i:s');

        $email = $this->session->userdata('email');
        $kategori = htmlspecialchars($this->input->post('kategori'));


        $upload_image = $_FILES['upload_artikel_proseding']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_artikel_proseding')) {
                $old_image = $data['ins_ifip']['upload_artikel_proseding'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('upload_artikel_proseding', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $upload_image = $_FILES['upload_loa']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_loa')) {
                $old_image = $data['ins_ifip']['upload_loa'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload2 = $this->upload->data('file_name');
                $this->db->set('upload_loa', $upload2);
            } else {
                echo $this->upload->display_errors();
            }
        }


        $this->db->set('kontribusi', $kontribusi);
        $this->db->set('kategori', $kategori);
        $this->db->set('sts', $sts);
        $this->db->set('nama_forum', $nama_forum);
        $this->db->set('institusi_penyelenggara', $institusi_penyelenggara);
        $this->db->set('lokasi', $lokasi);
        $this->db->set('tgl_pelaksanaan', $tgl_pelaksanaan);
        $this->db->set('url_web_forum', $url_web_forum);
        $this->db->set('judul_artikel_proseding', $judul_artikel_proseding);
        $this->db->set('url_artikel_proseding_publikasi', $url_artikel_proseding_publikasi);
        $this->db->set('issn', $issn);
        $this->db->set('batch', $batch);
        $this->db->set('date_created', $date_created);
        $this->db->set('tahun_publikasi_proseding', $tahun_publikasi_proseding);
        $this->db->set('lembaga_pengindeks', $lembaga_pengindeks);
        $this->db->set('nama_program', $nama_program);
        $this->db->set('tahun_penelitian', $tahun_penelitian);
        $this->db->where('id', $id);
        $this->db->update('ins_ifip');

        $log = [
            'log' => "Memngubah Data Insentif Forum Ilmiah Proseeding ID $id",
            'email' => $email,
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('ifip');
    }

    public function cetak($id)
    {
        $data['title'] = 'Cetak Kwitansi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['iden'] = $this->db->get_where('identitas', ['email' => $this->session->userdata('email')])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('ins_aji', ['id' => $id])->row_array();
        $this->load->view('cetak/c_iaj', $data);
    }

    public function history($email)
    {

        $data['title'] = 'Dashboard | History Usulan Insentif Artikel Dijurnal';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['history'] = $this->db->get_where('ins_aji', ['email' => $email])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('user', ['email' => $email])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('iaj/history', $data);
        $this->load->view('templates/footer');
    }
}
