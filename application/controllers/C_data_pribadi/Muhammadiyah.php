<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Muhammadiyah extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_data_pribadi/Model_muhammadiyah', 'mktam');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'ktam' => $this->mktam->getView(),
            'd' => $this->mktam->getDetail(),
            'draft' => $this->mktam->viewDraft(),
            'diajukan' => $this->mktam->viewDiajukan(),
            'disetujui' => $this->mktam->viewDisetujui(),
            'ditolak' => $this->mktam->viewTolak(),
            'ditangguhkan' => $this->mktam->viewDitangguhkan(),
            'nDraft' => $this->mktam->numsDraft(),
            'nDiajukan' => $this->mktam->numsDiajukan(),
            'nDisetujui' => $this->mktam->numsDisetujui(),
            'nDitangguhkan' => $this->mktam->numsDitangguhkan(),
            'nDitolak' => $this->mktam->numsTolak()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/muhammadiyah/c_muhammadiyah', $data);
        $this->load->view('layouts/footer');
    }

    public function updateMuhammadiyah()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'ktam' => $this->mktam->getView(),
            'd' => $this->mktam->getDetail(),
            'draft' => $this->mktam->viewDraft(),
            'diajukan' => $this->mktam->viewDiajukan(),
            'disetujui' => $this->mktam->viewDisetujui(),
            'ditolak' => $this->mktam->viewTolak(),
            'ditangguhkan' => $this->mktam->viewDitangguhkan(),
            'nDraft' => $this->mktam->numsDraft(),
            'nDiajukan' => $this->mktam->numsDiajukan(),
            'nDisetujui' => $this->mktam->numsDisetujui(),
            'nDitangguhkan' => $this->mktam->numsDitangguhkan(),
            'nDitolak' => $this->mktam->numsTolak()
        );


        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/muhammadiyah/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_muhammadiyah']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/muhammadiyah/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $id = $this->uuid->v4();
        $reff_km = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADKTAM";
        $jenis_ajuan = "Ajuan Data Keanggotaan Muhammadiyah";

        $data = array(
            'reff_km' => $reff_km,
            'nidn' => $nidn,
            'nktam' => htmlspecialchars($this->input->post('nktam', true)),
            'ranting' => htmlspecialchars($this->input->post('ranting', true)),
            'cabang' => htmlspecialchars($this->input->post('cabang', true)),
            'daerah' => htmlspecialchars($this->input->post('daerah', true)),
            'wilayah' => htmlspecialchars($this->input->post('wilayah', true)),
            'sts' => $sts,
            'file' => $upload1,
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_km,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->mktam->storeMuhammadiyah($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('umuhammadiyah');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('umuhammadiyah');
        }
    }



    public function detailRiwayatMuhammadiyah()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'ktam' => $this->mktam->getView(),
            'd' => $this->mktam->getDetail(),
            'draft' => $this->mktam->viewDraft(),
            'diajukan' => $this->mktam->viewDiajukan(),
            'disetujui' => $this->mktam->viewDisetujui(),
            'ditolak' => $this->mktam->viewTolak(),
            'ditangguhkan' => $this->mktam->viewDitangguhkan(),
            'nDraft' => $this->mktam->numsDraft(),
            'nDiajukan' => $this->mktam->numsDiajukan(),
            'nDisetujui' => $this->mktam->numsDisetujui(),
            'nDitangguhkan' => $this->mktam->numsDitangguhkan(),
            'nDitolak' => $this->mktam->numsTolak(),
            'dr' => $this->mktam->detailRiwayatMuhammadiyah($reff),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/muhammadiyah/d_muhammadiyah', $data);
        $this->load->view('layouts/footer');
    }
}
