


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keluarga extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_data_pribadi/Model_keluarga', 'mkl');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'klg' => $this->mkl->getView(),
            'd' => $this->mkl->getDetail(),
            'draft' => $this->mkl->viewDraft(),
            'diajukan' => $this->mkl->viewDiajukan(),
            'disetujui' => $this->mkl->viewDisetujui(),
            'ditolak' => $this->mkl->viewTolak(),
            'ditangguhkan' => $this->mkl->viewDitangguhkan(),
            'nDraft' => $this->mkl->numsDraft(),
            'nDiajukan' => $this->mkl->numsDiajukan(),
            'nDisetujui' => $this->mkl->numsDisetujui(),
            'nDitangguhkan' => $this->mkl->numsDitangguhkan(),
            'nDitolak' => $this->mkl->numsTolak()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/keluarga/c_keluarga', $data);
        $this->load->view('layouts/footer');
    }

    public function updateKeluarga()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'klg' => $this->mkl->getView(),
            'd' => $this->mkl->getDetail(),
            'draft' => $this->mkl->viewDraft(),
            'diajukan' => $this->mkl->viewDiajukan(),
            'disetujui' => $this->mkl->viewDisetujui(),
            'ditolak' => $this->mkl->viewTolak(),
            'ditangguhkan' => $this->mkl->viewDitangguhkan(),
            'nDraft' => $this->mkl->numsDraft(),
            'nDiajukan' => $this->mkl->numsDiajukan(),
            'nDisetujui' => $this->mkl->numsDisetujui(),
            'nDitangguhkan' => $this->mkl->numsDitangguhkan(),
            'nDitolak' => $this->mkl->numsTolak()
        );


        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/keluarga/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_keluarga']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/keluarga/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $id = $this->uuid->v4();
        $reff_keluarga = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADKLG";
        $jenis_ajuan = "Ajuan Data keluarga";

        $data = array(
            'reff_keluarga' => $reff_keluarga,
            'nidn' => $nidn,
            'status_perkawinan' => htmlspecialchars($this->input->post('status_perkawinan', true)),
            'nama_pasangan' => htmlspecialchars($this->input->post('nama_pasangan', true)),
            'nik' => htmlspecialchars($this->input->post('nik', true)),
            'nip' => htmlspecialchars($this->input->post('nip', true)),
            'pekerjaan_pasangan' => htmlspecialchars($this->input->post('pekerjaan_pasangan', true)),
            'tgl_pns' => htmlspecialchars($this->input->post('tgl_pns', true)),
            'sts' => $sts,
            'file' => $upload1,
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_keluarga,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->mkl->storeKeluarga($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('ukeluarga');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('ukeluarga');
        }
    }



    public function detailRiwayatKeluarga()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->mkl->getView(),
            'd' => $this->mkl->getDetail(),
            'draft' => $this->mkl->viewDraft(),
            'diajukan' => $this->mkl->viewDiajukan(),
            'disetujui' => $this->mkl->viewDisetujui(),
            'ditolak' => $this->mkl->viewTolak(),
            'ditangguhkan' => $this->mkl->viewDitangguhkan(),
            'nDraft' => $this->mkl->numsDraft(),
            'nDiajukan' => $this->mkl->numsDiajukan(),
            'nDisetujui' => $this->mkl->numsDisetujui(),
            'nDitangguhkan' => $this->mkl->numsDitangguhkan(),
            'nDitolak' => $this->mkl->numsTolak(),
            'dr' => $this->mkl->detailRiwayatKeluarga($reff),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/keluarga/d_keluarga', $data);
        $this->load->view('layouts/footer');
    }
}
