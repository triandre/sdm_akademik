<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengajaran extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pel_pendidikan/Model_pengajaran', 'mpp');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Pengajaran',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_pengajaran' => 'active',
            'v' => $this->mpp->getPengajaran()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/pengajaran/v_pengajaran', $data);
        $this->load->view('layouts/footer');
    }

    public function createPengajaran()
    {
        $data = array(
            'title' => 'Data Pengajaran',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_pengajaran' => 'active',
            'v' => $this->mpp->getPengajaran(),
            'prodi' => $this->mpp->getProdi()
        );


        $this->form_validation->set_rules('pt', 'Perguruan Tinggi Sasaran', 'required|trim');
        $this->form_validation->set_rules('kategori_kgt', 'Kategori Kegiatan', 'required|trim');
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'required|trim');
        $this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'required|trim');
        $this->form_validation->set_rules('bidang_tugas', 'Bidang TUgas', 'required|trim');
        $this->form_validation->set_rules('des', 'Deskripsi', 'required|trim');
        $this->form_validation->set_rules('metode', 'Metode', 'required|trim');
        $this->form_validation->set_rules('no_sk', 'No. SK', 'required|trim');
        $this->form_validation->set_rules('tgl_sk', 'Tanggal SK', 'required|trim');


        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/pel_pendidikan/pengajaran/c_pengajaran', $data);
            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();
            $reff = str_replace('-', '', $id);
            $nidn = $this->session->userdata('nidn');
            //UPLOAD
            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/detasering/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['occ_detasering']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/detasering/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $data = array(
                'reff' => $reff,
                'nidn' => $nidn,
                'pt' => htmlspecialchars($this->input->post('pt', true)),
                'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
                'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
                'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
                'metode' => htmlspecialchars($this->input->post('metode', true)),
                'des' => htmlspecialchars($this->input->post('des', true)),
                'bidang_tugas' => htmlspecialchars($this->input->post('bidang_tugas', true)),
                'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
                'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );
            $result = $this->dt->storeDetasering($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('detasering');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createDetasering');
            }
        }
    }
}
