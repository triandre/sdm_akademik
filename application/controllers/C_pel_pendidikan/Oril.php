<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Oril extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pel_pendidikan/Model_oril', 'mo');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Riwayat Orasi Ilmiah',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_oi' => 'active',
            'v' => $this->mo->getView(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/oril/v_oril', $data);
        $this->load->view('layouts/footer');
    }

    public function createOril()
    {
        $data = array(
            'title' => 'Tambah Riwayat Orasi Ilmiah',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_oi' => 'active',
            'v' => $this->mo->getView(),
            'akun' => $this->mu->getUser()
        );


        $this->form_validation->set_rules('kategori_kgt', 'Kategori Kegiatan', 'required|trim');
        $this->form_validation->set_rules('kategori_cpl', 'Kategori Capaian Luaran', 'required|trim');
        $this->form_validation->set_rules('kategori_pembicara', 'Kategori Pembicara', 'required|trim');
        $this->form_validation->set_rules('judul', 'Judul Makalah', 'required|trim');
        $this->form_validation->set_rules('nama_pi', 'Nama pertemuan Ilmiah', 'required|trim');
        $this->form_validation->set_rules('tingkat_temuan', 'Tingkat Pertemuan Ilmiah', 'required|trim');
        $this->form_validation->set_rules('penyelenggara', 'Penyelenggara', 'required|trim');
        $this->form_validation->set_rules('tgl_pelaksanaan', 'Tanggal Pelaksanaan', 'required|trim');


        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/pel_pendidikan/oril/c_oril', $data);
            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();
            $reff = str_replace('-', '', $id);
            $nidn = $this->session->userdata('nidn');

            //UPLOAD
            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/oril/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['occ_oril']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/oril/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $data = array(
                'reff_oril' => $reff,
                'nidn' => $nidn,
                'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
                'kategori_cpl' => htmlspecialchars($this->input->post('kategori_cpl', true)),
                'litabmas' => htmlspecialchars($this->input->post('litabmas', true)),
                'kategori_pembicara' => htmlspecialchars($this->input->post('kategori_pembicara', true)),
                'judul' => htmlspecialchars($this->input->post('judul', true)),
                'nama_pi' => htmlspecialchars($this->input->post('nama_pi', true)),
                'tingkat_temuan' => htmlspecialchars($this->input->post('tingkat_temuan', true)),
                'penyelenggara' => htmlspecialchars($this->input->post('penyelenggara', true)),
                'tgl_pelaksanaan' => htmlspecialchars($this->input->post('tgl_pelaksanaan', true)),
                'bahasa' => htmlspecialchars($this->input->post('bahasa', true)),
                'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
                'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );
            $result = $this->mo->storeOril($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('oril');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createOril');
            }
        }
    }

    public function detailOril()
    {
        $reff_oril = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Orasi Ilmiah',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_oi' => 'active',
            'v' => $this->mo->getView(),
            'akun' => $this->mu->getUser(),
            'd' => $this->mo->getDetail($reff_oril)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/oril/d_oril', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanOril()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Update Orasi Ilmiah',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_oi' => 'active',
            'v' => $this->mo->getView(),
            'akun' => $this->mu->getUser(),
            'd' => $this->mo->getDetail($reff)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/oril/u_oril', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanOrilGo()
    {

        $data = array(
            'title' => 'Data',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_oi' => 'active',
            'v' => $this->mo->getView(),
            'akun' => $this->mu->getUser()
        );

        $reff_oril = htmlspecialchars($this->input->post('reff_oril', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/oril/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_oril']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/oril/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'kategori_cpl' => htmlspecialchars($this->input->post('kategori_cpl', true)),
            'litabmas' => htmlspecialchars($this->input->post('litabmas', true)),
            'kategori_pembicara' => htmlspecialchars($this->input->post('kategori_pembicara', true)),
            'judul' => htmlspecialchars($this->input->post('judul', true)),
            'nama_pi' => htmlspecialchars($this->input->post('nama_pi', true)),
            'tingkat_temuan' => htmlspecialchars($this->input->post('tingkat_temuan', true)),
            'penyelenggara' => htmlspecialchars($this->input->post('penyelenggara', true)),
            'tgl_pelaksanaan' => htmlspecialchars($this->input->post('tgl_pelaksanaan', true)),
            'bahasa' => htmlspecialchars($this->input->post('bahasa', true)),
            'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
            'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
            'file' => $upload1
        );


        $result = $this->mo->updateOril($reff_oril, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('oril');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('oril/perbaikanOril/' . $reff_oril);
        }
    }

    public function hapusOril($reff_oril)
    {
        $reff_oril = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('reff_oril', $reff_oril);
        $this->db->update('occ_oril');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('oril');
    }
}
