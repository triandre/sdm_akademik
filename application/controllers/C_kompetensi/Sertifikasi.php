<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sertifikasi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/ModelSertifikasi', 'ms');
        $this->load->model('m_master/ModelPangkat', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Sertifikasi',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_sertifikasi' => 'active',
            'sertifikasi' => $this->ms->getSertifikasi()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/sertifikasi/v_sertifikasi', $data);
        $this->load->view('layouts/footer');
    }

    public function createSertifikasi()
    {
        $data = array(
            'title' => 'Form Tambah Riwayat Sertifikasi',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_sertifikasi' => 'active',
            'sertifikasi' => $this->ms->getSertifikasi()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/sertifikasi/c_sertifikasi', $data);
        $this->load->view('layouts/footer');
    }

    public function saveSertifikasi()
    {
        $data = array(
            'title' => 'Form Tambah Riwayat Sertifikasi',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_sertifikasi' => 'active',
            'sertifikasi' => $this->ms->getSertifikasi()

        );
        $id = $this->uuid->v4();
        $reff_sertifikasi = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 2;
        $kode_ajuan = "ADSR";
        $jenis_ajuan = "Ajuan Data Sertifikasi";

        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/sertifikasi/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_sertifikasi']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/sertifikasi/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_sertifikasi' => $reff_sertifikasi,
            'nidn' => $nidn,
            'jenis_sertifikasi' => htmlspecialchars($this->input->post('jenis_sertifikasi', true)),
            'bidang_studi' => htmlspecialchars($this->input->post('bidang_studi', true)),
            'no_sk_sertifikasi' => htmlspecialchars($this->input->post('no_sk_sertifikasi', true)),
            'tahun_sertifikasi' => htmlspecialchars($this->input->post('tahun_sertifikasi', true)),
            'no_peserta' => htmlspecialchars($this->input->post('no_peserta', true)),
            'no_registrasi' => htmlspecialchars($this->input->post('no_registrasi', true)),
            'sts' => $sts,
            'file' => $upload1,
            'active' => 1,
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_sertifikasi,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->ms->storeSertifikasi($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('sertifikasi');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createSertifikasi');
        }
    }

    public function hapusSertifikasi($id_sertifikasi)
    {
        $id_sertifikasi = $this->uri->segment(2);
        $active = 0;

        $this->db->set('active', $active);
        $this->db->where('id_sertifikasi', $id_sertifikasi);
        $this->db->update('occ_sertifikasi');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('sertifikasi');
    }

    public function detailSertifikasi()
    {
        $id_sertifikasi = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Sertifikasi',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_sertifikasi' => 'active',
            'd' => $this->ms->detailSertifikasi($id_sertifikasi),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/sertifikasi/d_sertifikasi', $data);
        $this->load->view('layouts/footer');
    }


    public function riwayatSertifikasi()
    {
        $data = array(
            'title' => 'Riwayat Sertifikasi',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_sertifikasi' => 'active',
            'sertifikasi' => $this->ms->getSertifikasi(),
            'draft' => $this->ms->viewDraft(),
            'diajukan' => $this->ms->viewDiajukan(),
            'disetujui' => $this->ms->viewDisetujui(),
            'ditolak' => $this->ms->viewTolak(),
            'ditangguhkan' => $this->ms->viewDitangguhkan(),
            'nDraft' => $this->ms->numsDraft(),
            'nDiajukan' => $this->ms->numsDiajukan(),
            'nDisetujui' => $this->ms->numsDisetujui(),
            'nDitangguhkan' => $this->ms->numsDitangguhkan(),
            'nDitolak' => $this->ms->numsTolak()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/sertifikasi/riw_sertifikasi', $data);
        $this->load->view('layouts/footer');
    }

    public function detailRiwSertifikasi()
    {
        $reff = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Pendidikan Formal',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_sertifikasi' => 'active',
            'd' => $this->ms->detailRiwSertifikasi($reff),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/sertifikasi/d_sertifikasi', $data);
        $this->load->view('layouts/footer');
    }

    public function createSertifikasiProfesi()
    {
        $data = array(
            'title' => 'Form Tambah Riwayat Sertifikasi Profesi',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_sertifikasi' => 'active',
            'sertifikasi' => $this->ms->getSertifikasi()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/sertifikasi/c_sertifikasiProfesi', $data);
        $this->load->view('layouts/footer');
    }

    public function saveSertifikasiProfesi()
    {
        $data = array(
            'title' => 'Form Tambah Riwayat Sertifikasi',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_sertifikasi' => 'active',
            'sertifikasi' => $this->ms->getSertifikasi()

        );
        $id = $this->uuid->v4();
        $reff_sertifikasi = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADSR";
        $jenis_ajuan = "Ajuan Data Sertifikasi";

        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/sertifikasi/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_sertifikasi']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/sertifikasi/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_sertifikasi' => $reff_sertifikasi,
            'nidn' => $nidn,
            'jenis_sertifikasi' => htmlspecialchars($this->input->post('jenis_sertifikasi', true)),
            'bidang_studi' => htmlspecialchars($this->input->post('bidang_studi', true)),
            'no_sk_sertifikasi' => htmlspecialchars($this->input->post('no_sk_sertifikasi', true)),
            'tahun_sertifikasi' => htmlspecialchars($this->input->post('tahun_sertifikasi', true)),
            'no_registrasi' => htmlspecialchars($this->input->post('no_registrasi', true)),
            'sts' => $sts,
            'file' => $upload1,
            'active' => 1,
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_sertifikasi,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->ms->storeSertifikasi($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('sertifikasi');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createSertifikasi');
        }
    }
}
