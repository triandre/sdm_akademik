<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/ModelTes', 'mt');
        $this->load->model('m_master/ModelPangkat', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Tes',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_tes' => 'active',
            'tes' => $this->mt->getTes()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/tes/v_tes', $data);
        $this->load->view('layouts/footer');
    }

    public function createTes()
    {
        $data = array(
            'title' => 'Form Tambah Riwayat Tes',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_tes' => 'active',
            'tes' => $this->mt->getTes()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/tes/c_tes', $data);
        $this->load->view('layouts/footer');
    }

    public function saveTes()
    {
        $data = array(
            'title' => 'Form Tambah Riwayat Tes',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_tes' => 'active',
            'tes' => $this->mt->getTes()

        );
        $id = $this->uuid->v4();
        $reff_tes = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 2;
        $kode_ajuan = "ADTS";
        $jenis_ajuan = "Ajuan Data Tes";

        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/tes/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_tes']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/tes/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_tes' => $reff_tes,
            'nidn' => $nidn,
            'nama_tes' => htmlspecialchars($this->input->post('nama_tes', true)),
            'jenis_tes' => htmlspecialchars($this->input->post('jenis_tes', true)),
            'penyelenggara' => htmlspecialchars($this->input->post('penyelenggara', true)),
            'tgl_tes' => htmlspecialchars($this->input->post('tgl_tes', true)),
            'tahun' => htmlspecialchars($this->input->post('tahun', true)),
            'skor_tes' => htmlspecialchars($this->input->post('skor_tes', true)),
            'sts' => $sts,
            'file' => $upload1,
            'active' => 1,
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_tes,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->mt->storeTes($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('tes');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createTes');
        }
    }

    public function hapusTes($id_tes)
    {
        $id_tes = $this->uri->segment(2);
        $active = 0;

        $this->db->set('active', $active);
        $this->db->where('id_tes', $id_tes);
        $this->db->update('occ_Tes');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('tes');
    }

    public function detailTes()
    {
        $id_tes = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Tes',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_tes' => 'active',
            'd' => $this->mt->detailTes($id_tes),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/tes/d_tes', $data);
        $this->load->view('layouts/footer');
    }


    public function riwayatTes()
    {
        $data = array(
            'title' => 'Riwayat Tes',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_tes' => 'active',
            'Tes' => $this->mt->getTes(),
            'draft' => $this->mt->viewDraft(),
            'diajukan' => $this->mt->viewDiajukan(),
            'disetujui' => $this->mt->viewDisetujui(),
            'ditolak' => $this->mt->viewTolak(),
            'ditangguhkan' => $this->mt->viewDitangguhkan(),
            'nDraft' => $this->mt->numsDraft(),
            'nDiajukan' => $this->mt->numsDiajukan(),
            'nDisetujui' => $this->mt->numsDisetujui(),
            'nDitangguhkan' => $this->mt->numsDitangguhkan(),
            'nDitolak' => $this->mt->numsTolak()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/Tes/riw_Tes', $data);
        $this->load->view('layouts/footer');
    }

    public function detailRiwTes()
    {
        $reff = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Riwayat Tes',
            'active_menu_kompetensi' => 'menu-open',
            'active_menu_kp' => 'active',
            'active_menu_tes' => 'active',
            'd' => $this->mt->detailRiwTes($reff),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kompetensi/tes/d_tes', $data);
        $this->load->view('layouts/footer');
    }
}
