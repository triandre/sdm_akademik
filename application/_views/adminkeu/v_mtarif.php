<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="#">Manajemen Tarif</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                <h3 class="card-title">

                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>NIDN</th>
                                <th>NAMA</th>
                                <th>Status</th>
                                <th>Tarif/SKS</th>
                                <th>P. Sarana Dakwah</th>
                                <th>Pinjaman LKK</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($v as $row) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row['nidn']; ?></td>
                                    <td><?= $row['name']; ?></td>
                                    <td><?php
                                        if ($row['role_id'] == 2) {
                                            echo '<span class="badge badge-info">Dosen Tetap</span> ';
                                        } elseif ($row['sts'] == 10) {
                                            echo '<span class="badge badge-primary">Dosen Tidak Tetap</span>';
                                        }
                                        ?></td>
                                    <td><?= number_format($row['tarif']); ?></td>
                                    <td><?= number_format($row['sarana_dakwah']); ?></td>
                                    <td><?= number_format($row['pinjaman_lkk']); ?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('adminkeu/mtarifDosen/' . $row['nidn']) ?>" class="dropdown-item">Edit Tarif</a>
                                                <a href="<?= base_url('adminkeu/detaiPengajaran/' . $row['nidn']) ?>" class="dropdown-item">Detail</a>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>

                    </table>

                </div>
            </div>
    </section>
    <!-- /.content -->

</div>
<script src=" <?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js">
</script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            },
            "pageLength": 100
        });
    });
</script>