<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="#">Kehadiran Dosen</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php

                use PhpParser\Node\Stmt\Echo_;

                if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                <h3 class="card-title">

                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Matakuliah</th>
                                <th>Kelas</th>
                                <th>Sks</th>
                                <th>Jumlah Kehadiran</th>
                                <th>Tarif</th>
                                <th>Trsanport</th>
                                <th>Total Tarif</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($v as $row) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row['name']; ?></td>
                                    <td><?= $row['matakuliah']; ?></td>
                                    <td><?= $row['kelas']; ?> - <?= $row['uraian']; ?></td>
                                    <td><?= $row['sks']; ?></td>
                                    <td><?= $row['jumlah_kehadiran']; ?></td>
                                    <td><?= number_format($row['tarif']); ?></td>
                                    <td><?= number_format($row['transport']); ?></td>
                                    <td><?= number_format($row['total_tarif']); ?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('adminkeu/tarifDosen/' . $row['id_reff'] . '/' . $row['nidn']) ?>" class="dropdown-item">Tarif</a>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>

                    </table>
                    <b>
                        <center>POTONGAN</center>
                    </b>
                    <?php
                    $tarifDosen = $total_tarif['jtotal_tarif'];
                    $sd = $potongan['sarana_dakwah'];
                    $pl = $potongan['pinjaman_lkk'];
                    $pph =  $tarifDosen * 0.025;
                    if ($tarifDosen < 1000000) {
                        $zis = 0;
                    } else {
                        $zis = $tarifDosen * 0.025;
                    }
                    $hasil = $sd + $pl + $pph  + $zis;
                    ?>

                    <table class="table table-bordered table-striped table-sm">
                        <tr>
                            <td align="right">SARANA DAKWAH</td>
                            <td><?= number_format($potongan['sarana_dakwah']); ?></td>
                        </tr>
                        <tr>
                            <td align="right">PINJAMAN LKK</td>
                            <td><?= number_format($potongan['pinjaman_lkk']); ?></td>
                        </tr>
                        <tr>
                            <td align="right">PPH PSL 21</td>
                            <td><?= number_format($pph); ?></td>
                        </tr>

                        <tr>
                            <td align="right">Z I S</td>
                            <td><?= number_format($zis); ?></td>
                        </tr>
                        <tr>
                            <td align="right"><b>JUMLAH POTONGAN</b></td>
                            <td><b><?= number_format($hasil); ?></b></td>
                        </tr>

                        <tr>
                            <td align="right"><b>TOTAL TARIF</b></td>
                            <td><b><?= number_format($total_tarif['jtotal_tarif']); ?></b></td>
                        </tr>

                        <tr>
                            <td align="right"><b>TOTAL BERSIH</b></td>
                            <td><?php $total = $tarifDosen - $hasil ?>
                                <b><?= number_format($total); ?></b>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
    </section>
    <!-- /.content -->

</div>
<script src=" <?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js">
</script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>