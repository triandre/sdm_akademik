<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">UI. Forum Ilmiah (Proceeding)</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <h3 class="card-title">
                    <a href="javascript:history.back()" class="btn btn-block bg-gradient-danger"><i class="fa fa-backward"></i> Kembali</a>
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <div class="table-responsive">
                    <table class="table table-bordered" id="example1" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Permohonan Insentif Jurnal Ilmiah Procceding</th>
                                <?php if ($akun['role_id'] == '4') { ?>
                                    <th>Catatan Validator</th>
                                <?php } ?>
                                <?php if ($akun['role_id'] == '5') { ?>
                                    <th>Catatan WR I</th>
                                <?php } ?>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($h as $fip) : ?>
                                <tr>
                                    <td style="width:5%" scope="row"><?= $i; ?></td>
                                    <td style="width:70%">
                                        <small>Tanggal Permohonan, <?= date("d F Y", strtotime($fip['date_created'])) ?></small>
                                        <br>
                                        <?= $fip['judul_artikel_proseding']; ?>
                                        <br>
                                        <small><?= $fip['nama_forum']; ?> - <i>Penyelenggara: <?= $fip['institusi_penyelenggara']; ?></i></small>
                                        <br>
                                        <small><i>
                                                <?php
                                                if ($fip['sts'] == 1) {
                                                    echo '<span class="badge badge-info">Proses LP2M</span> ';
                                                } elseif ($fip['sts'] == 2) {
                                                    echo '<span class="badge badge-primary">Proses Wakil Rektor I</span>';
                                                } elseif ($fip['sts'] == 3) {
                                                    echo '<span class="badge badge-warning">Proses Wakil Rektor II</span>';
                                                } elseif ($fip['sts'] == 4) {
                                                    echo '<span class="badge badge-success">Di Terima</span>';
                                                } elseif ($fip['sts'] == 6) {
                                                    echo '<span class="badge badge-danger">Berkas DiTolak</span>';
                                                }
                                                ?>
                                            </i></small>
                                        <br>
                                        <small><i><?php
                                                    if ($akun['role_id'] >= '3') { ?>
                                                    <a href="<?= base_url('user/detail/'); ?><?= $fip['email']; ?>">Pemohon : <?= $fip['email']; ?></a>
                                                <?php } ?></i></small>

                                        <?php
                                        if ($fip['sts'] == '6') { ?>
                                            Catatan : <?= $fip['ket']; ?>
                                        <?php } ?>
                                    </td>

                                    <?php
                                    if ($akun['role_id'] == '4') { ?>
                                        <td> <?= $fip['ket']; ?> </td>
                                    <?php } ?>
                                    <?php
                                    if ($akun['role_id'] == '5') { ?>
                                        <td> <?= $fip['catatan1']; ?> </td>
                                    <?php } ?>


                                    <td style="width:5%">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('vifip/detail/'); ?><?= $fip['id']; ?>" class="dropdown-item">Lihat Detail</a>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>