<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Beasiswa</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Reward</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Beasiswa</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Beasiswa</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Jenis Beasiswa</td>
                            <td width="50px">:</td>
                            <td><?= $d['jenis_beasiswa']; ?> </td>
                        </tr>
                        <tr>
                            <td width="100px">Nama Beasiswa</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_beasiswa']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Tahun Mulai</td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun_mulai'] ?></td>
                        </tr>
                        <tr>
                            <td width="200px">Tahun Selesai</td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun_selesai'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Masih Terima</td>
                            <td width="50px">:</td>
                            <td><?= $d['masih_terima'] ?></td>
                        </tr>


                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="<?= base_url('beasiswa') ?>">
                    <button type="button" class="btn btn-danger"><i class="fa fa-backward"></i> Kembali</button>
                </a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->