<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form Usulan Insentif Buku Ajar</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">UI. Buku Ajar</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Form Usulan Insentif Buku Ajar</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <form action="<?= base_url('createIba'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <h4>INSENTIF BUKU AJAR</h4>
                        <small>Isi data-data Berikut</small>
                        <hr>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Jenis Buku: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="jenis_buku" name="jenis_buku" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Buku Ajar">Buku Ajar</option>
                                    <option value="Refrensi">Refrensi</option>
                                    <option value="Chapter">Chapter</option>
                                    <option value="monograf">monograf</option>

                                </select>
                                <?= form_error('jenis_buku', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Kontribusi Pengusul: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="kontribusi_pengusul" name="kontribusi_pengusul" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Penulis Utama">Penulis Utama</option>
                                    <option value="Penulis pENDAMPING">Penulis Pendamping</option>
                                </select>
                                <?= form_error('kontribusi_pengusul', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Kategori Buku Ajar: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="kategori_ba" name="kategori_ba" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Internasional">Internasional</option>
                                    <option value="Nasional">Nasional</option>
                                </select>
                                <?= form_error('kategori_ba', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Judul Buku : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="judul_buku" name="judul_buku" required value="<?= set_value('judul_buku'); ?>">
                                <?= form_error('judul_buku', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Penerbit : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="penerbit" name="penerbit" required value="<?= set_value('penerbit'); ?>">
                                <?= form_error('penerbit', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Tahun Buku Terbit : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="tahun_terbit" name="tahun_terbit" required value="<?= set_value('tahun_terbit'); ?>">
                                <?= form_error('tahun_terbit', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>ISBS/E-ISBN: <small class="text-danger">(*)Wajib Diisi.</small> </label>
                                <input type="text" class="form-control" id="isbn" name="isbn" required value="<?= set_value('isbn'); ?>">
                                <?= form_error('isbn', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Jumlah Halaman (tidak termasuk Cover) : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="jumlah_halaman" name="jumlah_halaman" required value="<?= set_value('jumlah_halaman'); ?>">
                                <?= form_error('jumlah_halaman', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>URL Website Penerbit : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="web_penerbit" name="web_penerbit" required value="<?= set_value('web_penerbit'); ?>">
                                <?= form_error('web_penerbit', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Apakah Buku Sudah Diinput di SINTA Pengusul : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="pertanyaan1" name="pertanyaan1" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Sudah">Sudah</option>
                                    <option value="Belum">Belum</option>
                                </select>
                                <?= form_error('pertanyaan1', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Mata Kuliah Yang Diampu: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="pertanyaan2" name="pertanyaan2" required value="<?= set_value('pertanyaan2'); ?>">
                                <?= form_error('pertanyaan2', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Buku tersebut digunakan Untuk Mata Kuliah : <small class="text-danger">(*)Wajib Diisi.</small> </label>
                                <input type="text" class="form-control" id="pertanyaan3" name="pertanyaan3" required value="<?= set_value('pertanyaan3'); ?>">
                                <?= form_error('pertanyaan3', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>UPLOAD BERKAS (Cover Depan, Halaman Balik Judul yang menerangkan Penerbit, Cetakan dan ISBN, Daftar Isi, Halaman Pertama Isi Buku (Dalam 1 File PDF Maksimal 3 MB). Hard Copy BUKU diserahkan ke Kantor LPPM. Untuk Buku Terbitan UMSU Press 3 Eksp. Untuk terbitan Diluar UMSU Press 3 Eksp: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="file" class="form-control" id="upload_cover" name="upload_cover" accept="application/pdf" required>
                                <?= form_error('upload_cover', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>UPLOAD SOFT COPY FULL BUKU (File PDF) : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="file" class="form-control" id="upload_buku" name="upload_buku" accept="application/pdf" required>
                                <?= form_error('upload_buku', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                    </div>



                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>

        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>