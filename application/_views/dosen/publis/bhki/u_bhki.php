<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Perbaikan Bantuan Bantuan Hki/Paten</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Bantuan</a></li>
                        <li class="breadcrumb-item active">UI. Hki/Paten</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Perbaikan Bantuan Bantuan Hki/Paten</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <form action="<?= base_url('bhki/perbaikanGo'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <h4>PERBAIKAN BANTUAN HKI</h4>
                        <small>Isi data-data Identitas HKI</small>
                        <hr>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Judul HKI :</label>
                                <input type="text" class="form-control" id="id" name="id" value="<?= $d['id']; ?>" required readonly hidden>
                                <input type="text" class="form-control" id="judul_hki" name="judul_hki" value="<?= $d['judul_hki']; ?>" required>
                                <?= form_error('judul_hki', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Nomor Registrasi :</label>
                                <input type="text" class="form-control" id="no_reg" name="no_reg" required value="<?= $d['no_reg']; ?>">
                                <?= form_error('no_reg', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Tanggal Pengajuan HKI :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="date" class="form-control" id="tgl_pengajuan_hki" name="tgl_pengajuan_hki" required value="<?= $d['tgl_pengajuan_hki']; ?>">
                                <?= form_error('tgl_pengajuan_hki', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                <label>Jenis HKI :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="jenis_hki" name="jenis_hki" required>
                                    <option value="<?= $d['jenis_hki']; ?>"><?= $d['jenis_hki']; ?></option>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="HAK CIPTA UMUM">HAK CIPTA UMUM</option>
                                    <option value="HAK CIPTA UMKM">HAK CIPTA UMKM</option>
                                    <option value="HAK CIPTA KHUSUS UMUM(JENIS BASIS DATA, KOMPILASI CIPTAAN/DATA, PERMAINAN VIDEO & PROGRAM KOMPUTER)">HAK CIPTA KHUSUS (JENIS BASIS DATA, KOMPILASI CIPTAAN/DATA, PERMAINAN VIDEO & PROGRAM KOMPUTER)</option>
                                    <option value="HAK CIPTA KHUSUS UMKM(JENIS BASIS DATA, KOMPILASI CIPTAAN/DATA, PERMAINAN VIDEO & PROGRAM KOMPUTER)">HAK CIPTA KHUSUS (JENIS BASIS DATA, KOMPILASI CIPTAAN/DATA, PERMAINAN VIDEO & PROGRAM KOMPUTER)</option>
                                    <option value="PATEN UMUM">PATEN UMUM</option>
                                    <option value="PATEN UMKM">PATEN UMKM</option>
                                    <option value="PATEN SEDERHANA UMUM">PATEN SEDERHANA UMUM</option>
                                    <option value="PATEN SEDERHANA UMKM">PATEN SEDERHANA UMKM</option>
                                    <option value="MEREK">MEREK</option>
                                    <option value="DESAIN INDUSTRI">DESAIN INDUSTRI</option>
                                    <option value="DESAIN INDUSTRI UMUM (SATU DESAIN INDUSTRI)">DESAIN INDUSTRI UMUM (SATU DESAIN INDUSTRI)</option>
                                    <option value="DESAIN INDUSTRI UMKM (SATU DESAIN INDUSTRI)">DESAIN INDUSTRI UMKM (SATU DESAIN INDUSTRI)</option>
                                    <option value="DESAIN INDUSTRI (SET) UMUM">DESAIN INDUSTRI (SET) UMUM</option>
                                    <option value="DESAIN INDUSTRI (SET) UMKM">DESAIN INDUSTRI (SET) UMKM</option>
                                    <option value="RAHASIA DAGANG UMUM (PENCATAAN LISENSI)">RAHASIA DAGANG UMUM (PENCATAAN LISENSI)</option>
                                    <option value="RAHASIA DAGANG UMKM (PENCATAAN LISENSI)">RAHASIA DAGANG UMKM (PENCATAAN LISENSI)</option>
                                    <option value="RAHASIA DAGANG UMUM (PENGALIH RAHASIA DAGANG)">RAHASIA DAGANG UMUM (PENGALIH RAHASIA DAGANG)</option>
                                    <option value="RAHASIA DAGANG UMKM (PENGALIH RAHASIA DAGANG)">RAHASIA DAGANG UMKM (PENGALIH RAHASIA DAGANG)</option>
                                </select>
                                <?= form_error('jenis_hki', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Pemegang HKI :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="pemegang_hki" name="pemegang_hki" required>
                                    <option value="<?= $d['pemegang_hki']; ?>"><?= $d['pemegang_hki']; ?></option>
                                    <option value="Universitas Muhammadiyah Sumatera Utara">Universitas Muhammadiyah Sumatera Utara</option>

                                </select>
                                <?= form_error('pemegang_hki', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                <label>Apakah HKI Merupakan Hasil Penelitian :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="pertanyaan1" name="pertanyaan1" required>
                                    <option value="<?= $d['pertanyaan1']; ?>"><?= $d['pertanyaan1']; ?></option>
                                    <option value="Iya">Iya</option>
                                    <option value="Tidak">Tidak</option>
                                </select>
                                <?= form_error('pertanyaan1', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Kategori HKI :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="kategori_hki" name="kategori_hki" required>
                                    <option value="<?= $d['kategori_hki']; ?>"><?= $d['kategori_hki']; ?></option>
                                    <option value="Internasional">Internasional</option>
                                    <option value="Nasional">Nasional</option>
                                </select>
                                <?= form_error('kategori_hki', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>


                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>UPLOAD (Scan Bukti Bayar, Formulir Pendaftaran dan Dijadikan 1 File Pdf. Maks File 2 MB) :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="file" class="form-control" id="upload_file" name="upload_file" accept="application/pdf" required>
                                <?= form_error('upload_file', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>

        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>