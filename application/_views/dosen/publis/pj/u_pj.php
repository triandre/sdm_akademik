<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Perbaikan Usulan Insentif Pengelola Jurnal</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">UI. Pengelola Jurnal</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Perbaikan Usulan Insentif Pengelola Jurnal</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <form action="<?= base_url('pj/perbaikanGo'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <h4>PERBAIKAN USULAN PENGELOLA JURNAL</h4>
                        <small>Isi data-data Berikut</small>
                        <hr>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input hidden readonly type="text" class="form-control" id="id" name="id" value="<?= $d['id']; ?>">
                                <label>Nama Jurnal : <small class="text-danger">(*).</small></label>
                                <input type="text" class="form-control" id="nama_jurnal" name="nama_jurnal" value="<?= $d['nama_jurnal']; ?>">
                                <?= form_error('nama_jurnal', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>URL Jurnal : <small class="text-danger">(*).</small></label>
                                <input type="text" class="form-control" id="url_jurnal" name="url_jurnal" required value="<?= $d['url_jurnal']; ?>">
                                <?= form_error('url_jurnal', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label>Peringkat Jurnal : <small class="text-danger">(*).</small></label>
                                <select class="form-control" id="peringkat_jurnal" name="peringkat_jurnal" required>
                                    <option value="<?= $d['peringkat_jurnal']; ?>"><?= $d['peringkat_jurnal']; ?></option>
                                    <option value="Sinta 1">Sinta 1</option>
                                    <option value="Sinta 2">Sinta 2</option>
                                    <option value="Sinta 3">Sinta 3</option>
                                    <option value="Sinta 4">Sinta 4</option>
                                    <option value="Sinta 5">Sinta 5</option>
                                    <option value="Sinta 6">Sinta 6</option>
                                    <option value="Belum Terakreditasi">Belum Terakreditasi</option>
                                </select>
                                <?= form_error('peringkat_jurnal', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label>Volume/Nomor/Tahun yang Diajukan: </label>
                                <input type="text" class="form-control" id="vnt" name="vnt" required value="<?= $d['vnt']; ?>">
                                <?= form_error('vnt', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>

                            <div class="col-sm-4">
                                <label>Kategori : <small class="text-danger">(*).</small></label>
                                <select class="form-control" id="kategori_pj" name="kategori_pj" required>
                                    <option value="<?= $d['kategori_pj']; ?>"><?= $d['kategori_pj']; ?></option>
                                    <option value="Internasional">Internasional</option>
                                    <option value="Nasional">Nasional</option>
                                </select>
                                <?= form_error('kategori_pj', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label>Issue URL/Table of Content yang diajukan: <small class="text-danger">(*).</small></label>
                                <input type="text" class="form-control" id="url_issue" name="url_issue" required value="<?= $d['url_issue']; ?>">
                                <?= form_error('url_issue', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label>URL Google Schoolar Jurnal: <small class="text-danger">(*).</small></label>
                                <input type="text" class="form-control" id="url_schoolar" name="url_schoolar" required value="<?= $d['url_schoolar']; ?>">
                                <?= form_error('url_schoolar', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>

                            <div class="col-sm-4">
                                <label>URL Portal Garuda Jurnal : <small class="text-danger">(*).</small> </label>
                                <input type="text" class="form-control" id="url_garuda" name="url_garuda" required value="<?= $d['url_garuda']; ?>">
                                <?= form_error('url_garuda', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>UPLOAD SURAT PERMOHONAN (Lamp. Surat Permohonan Diketahui Pimpinan Fakultas, Daftar Isi Artikel -Satu File Pdf): <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="file" class="form-control" id="upload_permohonan" name="upload_permohonan" accept="application/pdf" required>
                                <?= form_error('upload_permohonan', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>

        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>