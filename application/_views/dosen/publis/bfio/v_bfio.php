<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><?= $title; ?></h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">UB. Seminar Nasional/Internasional</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <h3 class="card-title">
                    <a href="<?= base_url('createBfio'); ?>" class="btn btn-block bg-gradient-primary"><i class="fa fa-plus"></i> Tambah</a>
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <div class="table-responsive">
                    <table class="table table-bordered" id="example1" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Permohonan Bantuan Mengikuti Seminar</th>
                                <?php if ($akun['role_id'] == '4') { ?>
                                    <th>Catatan Validator</th>
                                <?php } ?>
                                <?php if ($akun['role_id'] == '5') { ?>
                                    <th>Catatan WR I</th>
                                <?php } ?>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($bfio as $bp) : ?>
                                <tr>
                                    <th scope="row"><?= $i; ?></th>
                                    <td><small>Tanggal Permohonan, <?= date("d F Y", strtotime($bp['date_created'])) ?></small>
                                        <br>
                                        <?= $bp['judul_artikel']; ?> - <?= $bp['nama_fi']; ?>
                                        <br>
                                        <small> Web Forum Ilmiah : <?= $bp['web_fi']; ?></small>
                                        <br>
                                        <small><i>Institusi Penyelenggara : <?= $bp['institusi_penyelenggara']; ?></i></small>
                                        <br>
                                        <small><i>Lokasi Kegiatan : <?= $bp['lokasi']; ?></i></small>
                                        <br>
                                        <small><i>Tanggal Penyelenggara : <?= date("d F Y", strtotime($bp['tgl_penyelenggara'])) ?></i></small>
                                        <br>
                                        <small>Biaya Pendaftaran : Rp. <?= number_format($bp['biaya_pendaftaran']); ?> | Berkas :<a href="<?= base_url('file/'); ?><?= $bp['upload_laporan']; ?>" target="_blank"> Lihat</small></a>
                                        <br>
                                        <small><i>Status : <?php
                                                            if ($bp['sts'] == 1) {
                                                                echo '<span class="badge badge-info">Proses LP2M</span> ';
                                                            } elseif ($bp['sts'] == 2) {
                                                                echo '<span class="badge badge-primary">Proses Wakil Rektor I</span>';
                                                            } elseif ($bp['sts'] == 3) {
                                                                echo '<span class="badge badge-warning">Proses Wakil Rektor II</span>';
                                                            } elseif ($bp['sts'] == 4) {
                                                                echo '<span class="badge badge-success">Di Terima</span>';
                                                            } elseif ($bp['sts'] == 6) {
                                                                echo '<span class="badge badge-danger">Berkas DiTolak</span>';
                                                            }  ?></i></small>
                                        <br>
                                        <small>
                                            Catatan Persetujuan :
                                            <?php
                                            if ($bp['sts'] == '4') { ?>
                                                <?= $bp['catatan2']; ?>
                                            <?php } ?>
                                        </small>




                                        <?php
                                        if ($akun['role_id'] >= '3') { ?>
                                            <a href="<?= base_url('user/detail/'); ?><?= $bp['email']; ?>"><small><i>Pemohon:<?= $bp['email']; ?></i></small></a>
                                        <?php } ?>
                                        <br>
                                        <?php
                                        if ($bp['sts'] == '6') { ?>
                                            <small style="color:red;">Catatan : <?= $bp['ket']; ?></small>
                                        <?php } ?>
                                    </td>

                                    <?php
                                    if ($akun['role_id'] == '4') { ?>
                                        <td> <?= $bp['ket']; ?> </td>
                                    <?php } ?>
                                    <?php
                                    if ($akun['role_id'] == '5') { ?>
                                        <td> <?= $bp['catatan1']; ?> </td>
                                    <?php } ?>




                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('bfio/detail/'); ?><?= $bp['id']; ?>" class="dropdown-item">Lihat Detail</a>



                                                <?php
                                                if ($akun['role_id'] == '2') { ?>
                                                    <?php
                                                    if ($bp['sts'] == '6') { ?>
                                                        <a href="<?= base_url('bfio/perbaikan/'); ?><?= $bp['id']; ?>" class="dropdown-item">Perbaiki Berkas</a>
                                                    <?php } ?>


                                                <?php } ?>

                                                <?php
                                                if ($akun['role_id'] == '3') { ?>
                                                    <a href="<?= base_url('insentif/bfio/setuju/'); ?><?= $bp['id']; ?>" class="dropdown-item">Setuju Berkas</i></a>
                                                    <a href="<?= base_url('insentif/bfio/tolak/'); ?><?= $bp['id']; ?>" class="dropdown-item">Tolak Berkas</i></a>
                                                    <a href="<?= base_url('bfio/history/'); ?><?= $bp['email']; ?>" class="dropdown-item">History</a>
                                                <?php } ?>

                                                <?php
                                                if ($akun['role_id'] == '4') { ?>
                                                    <a href="<?= base_url('wr1/bfio/fee/'); ?><?= $bp['id']; ?>" class="dropdown-item">Setuju</i></a>
                                                    <a href="<?= base_url('wr1/bfio/tolak/'); ?><?= $bp['id']; ?>" class="dropdown-item">Tolak</i></a>
                                                    <a href="<?= base_url('bfio/history/'); ?><?= $bp['email']; ?>" class="dropdown-item">History</a>
                                                <?php } ?>

                                                <?php
                                                if ($akun['role_id'] == '5') { ?>
                                                    <a href="<?= base_url('wr2/bfio/fee/'); ?><?= $bp['id']; ?>" class="dropdown-item">Setuju</i></a>
                                                    <a href="<?= base_url('wr2/bfio/tolak/'); ?><?= $bp['id']; ?>" class="dropdown-item">Tolak</i></a>
                                                    <a href="<?= base_url('bfio/history/'); ?><?= $bp['email']; ?>" class="dropdown-item">History</a>
                                                <?php } ?>

                                                <?php
                                                if ($akun['role_id'] == '2') { ?>
                                                    <?php
                                                    if ($bp['sts'] == '4') { ?>
                                                        <a href="<?= base_url('bfio/cetak/'); ?><?= $bp['id']; ?>" target="_blank" class="dropdown-item">Cetak Kwitansi</a>
                                                    <?php } ?>
                                                <?php } ?>


                                            </div>
                                        </div>


                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.card-body -->

</div>
<!-- /.card -->

</section>
<!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>