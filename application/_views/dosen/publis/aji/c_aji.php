<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form Usulan Insentif Artikel Dijurnal</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">UI. Publikasi Jurnal</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Form Usulan Insentif Artikel Dijurnal</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form action="<?= base_url('createAji'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <h4>IDENTITAS ARTIKEL</h4>
                        <small>Isi data-data identitas Artikel</small>
                        <hr>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Judul Artikel :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="judul" name="judul" required value="<?= set_value('judul'); ?>">
                                <?= form_error('judul', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Nama Penulis Artikel (dari mulai penulis 1) :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="nama_penulis" name="nama_penulis" required value="<?= set_value('nama_penulis'); ?>">
                                <?= form_error('nama_penulis', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Volume/Nomor/Tahun :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="vnt" name="vnt" required value="<?= set_value('vnt'); ?>">
                                <?= form_error('vnt', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                <label>Halaman Jurnal :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="halaman" name="halaman" required value="<?= set_value('halaman'); ?>">
                                <?= form_error('halaman', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Nama Jurnal :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="nama_jurnal" name="nama_jurnal" required value="<?= set_value('nama_jurnal'); ?>">
                                <?= form_error('nama_jurnal', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                <label>EISSN :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="eissn" name="eissn" required value="<?= set_value('eissn'); ?>">
                                <?= form_error('eissn', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label>Kontribusi:<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control selecty" id="kontribusi" name="kontribusi" required>
                                    <option selected disabled value="">Pilih</option>
                                    <?php foreach ($kontribusi as $row2) : ?>
                                        <option value="<?= $row2['kontribusi']; ?>"><?= $row2['kontribusi']; ?></option>
                                    <?php endforeach ?>
                                </select>
                                <?= form_error('kontribusi', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label>Kategori Jurnal :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control selectz" id="kategori_jurnal" name="kategori_jurnal" required>
                                    <option selected disabled value="">Pilih</option>
                                    <?php foreach ($kategori as $row3) : ?>
                                        <option value="<?= $row3['kategori']; ?>"><?= $row3['kategori']; ?></option>
                                    <?php endforeach ?>
                                </select>
                                <?= form_error('kategori_jurnal', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>

                            <div class="col-sm-4">
                                <label>Lembaga Pengindeks :<small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control selecty" id="lembaga_pengindeks" name="lembaga_pengindeks" required>
                                    <option selected disabled value=""> Pilih</option>
                                    <?php foreach ($lembaga as $row4) : ?>
                                        <option value="<?= $row4['lembaga']; ?>"><?= $row4['lembaga']; ?></option>
                                    <?php endforeach ?>
                                </select>
                                <?= form_error('lembaga_pengindeks', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>URL Jurnal (Contoh: http://jurnal.umsu.ac.id/index.php/ijbe/index) * : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="url_jurnal" name="url_jurnal" required value="<?= set_value('url_jurnal'); ?>">
                                <?= form_error('url_jurnal', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <hr>
                        <h4>Faktor dampak/Impact Factor (IF)/SJR Jurnal (Untuk jurnal bereputasi Scopus)</h4>
                        <small>Khusus untuk pengajuan Jurnal yang terindeks di Scopus</small>
                        <hr>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>URL Impact Factor/SJR:</label>
                                <input type="text" class="form-control" id="url_sjr" name="url_sjr">
                            </div>
                            <div class="col-sm-6">
                                <label>Nilai Impact Faktor/SJR:</label>
                                <input type="text" class="form-control" id="nilai_sjr" name="nilai_sjr">
                            </div>
                        </div>

                        <hr>
                        <h4>Identitas Penelitian yang Menghasilkan artikel</h4>
                        <hr>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label>Nama Program:</label>
                                <select class="form-control" id="nama_program" name="nama_program">
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Penelitian Internal">Penelitian Internal</option>
                                    <option value="Penelitian Dikti">Penelitian Dikti</option>
                                    <option value="Penelitian Mandiri">Penelitian Mandiri</option>
                                    <option value="Penelitian Hibah Lain">Penelitian Hibah Lain</option>

                                </select>
                                <?= form_error('nama_program', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label>Nomor Kontrak:</label>
                                <input type="text" class="form-control" id="nomor_kontrak" name="nomor_kontrak" value="<?= set_value('nomor_kontrak'); ?>">
                                <?= form_error('nomor_kontrak', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label>Apakah Artikel yang diterbitkan Direview: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="pertanyaan1" name="pertanyaan1" required>
                                    <option selected disabled value="">Silahkan Pilih</option>
                                    <option value="Iya">Iya</option>
                                    <option value="Tidak">Tidak</option>
                                </select>
                                <?= form_error('pertanyaan1', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label>Peringkat Jurnal:</label>
                                <select class="form-control" id="peringkat_jurnal" name="peringkat_jurnal">
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Internasional">Internasional</option>
                                    <option value="Nasional">Nasional</option>

                                </select>
                                <?= form_error('peringkat_jurnal', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>UPLOAD BUKTI REVIEW JIKA ADA (Lampirkan Semua Artikel Direviw, Bukti Korespondensi melalui email atau Open Journal System dan Catatan (coretan Reviewer) artikel yang direview, dalam 1 file PDF) Khusus untuk Jurnal Internasional dan Jurnal Nasional Terakreditasi Sinta 3-6</label>
                                <input type="file" accept="application/pdf" class="form-control" id="upload_bukti_review" name="upload_bukti_review">
                                <?= form_error('upload_bukti_review', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>UPLOAD ARTIKEL ILMIAH (Lampirkan Cover Jurnal, Daftar Isi yang ada Nama Pengusul, Artikel Ilmiah, (Dalam 1 (satu) file PDF Maksimal 3 MB): <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="file" class="form-control" id="upload_artikel" name="upload_artikel" accept="application/pdf" required>
                                <?= form_error('upload_artikel', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>