<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">
                        <a href="javascript:history.back()" class="btn bg-gradient-danger"><i class="fa fa-backward"></i> Kembali</a>
                    </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Profile</a></li>
                        <li class="breadcrumb-item active">Data Pribadi</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Profil</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                                <img class="mx-auto d-block" src="<?= base_url('assets/images/profil/') ?><?= $ss['image']; ?>" style="width: 200px;">

                                <br> <br>
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>NIDN/NIDK/NITK</td>
                                        <td>:</td>
                                        <td><?= $dp['nidn']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>NPP</td>
                                        <td>:</td>
                                        <td><?= $du['npp']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>NKTAM</td>
                                        <td>:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td><?= $du['name']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        <td><?= $dp['jk']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tempat Lahir</td>
                                        <td>:</td>
                                        <td><?= $dp['t_lahir']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td><?= $dp['tgl_lahir']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Ibu Kandung</td>
                                        <td>:</td>
                                        <td><?= $dp['nama_ibu']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->

                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Kependudukan</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>NIK</td>
                                        <td>:</td>
                                        <td><?= $kdd['nik']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Agama</td>
                                        <td>:</td>
                                        <td><?= $kdd['agama']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kewarganegaraan</td>
                                        <td>:</td>
                                        <td><?= $kdd['kewarganegaraan']; ?></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                    <!-- /.card -->


                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Keluarga</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Status Perkawinan</td>
                                        <td>:</td>
                                        <td><?= $klg['status_perkawinan']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Suami/Istri</td>
                                        <td>:</td>
                                        <td><?= $klg['nama_pasangan']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>NIK Suami/Istri</td>
                                        <td>:</td>
                                        <td><?= $klg['nik']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>NIP Suami/Istri</td>
                                        <td>:</td>
                                        <td><?= $klg['nip']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pekerjaan Suami/Istri</td>
                                        <td>:</td>
                                        <td><?= $klg['pekerjaan_pasangan']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Terhitung Mulai Tanggal PNS Suami/Istri</td>
                                        <td>:</td>
                                        <td><?= $klg['tgl_pns']; ?></td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->



                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Keanggotaan Muhammadiyah</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>NKTAM</td>
                                        <td>:</td>
                                        <td><?= $ktam['nktam']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Ranting</td>
                                        <td>:</td>
                                        <td><?= $ktam['ranting']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Cabang</td>
                                        <td>:</td>
                                        <td><?= $ktam['cabang']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Daerah</td>
                                        <td>:</td>
                                        <td><?= $ktam['daerah']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Wilayah</td>
                                        <td>:</td>
                                        <td><?= $ktam['wilayah']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->



                </div>
                <!-- /.col (LEFT) -->


                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Bidang Ilmu</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Kelompok Bidang</td>
                                        <td>:</td>
                                        <td><?= $bid['bidang_ilmu']; ?></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Alamat dan Kontrak</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td><?= $alm['alamat']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>RT</td>
                                        <td>:</td>
                                        <td><?= $alm['rt']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>RW</td>
                                        <td>:</td>
                                        <td><?= $alm['rw']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Dusun</td>
                                        <td>:</td>
                                        <td><?= $alm['dusun']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Desa/Kelurahan</td>
                                        <td>:</td>
                                        <td><?= $alm['kelurahan']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td><?= $alm['kecamatan']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kota/Kabupaten</td>
                                        <td>:</td>
                                        <td><?= $alm['kota']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Provinsi</td>
                                        <td>:</td>
                                        <td><?= $alm['provinsi']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kode Pos</td>
                                        <td>:</td>
                                        <td><?= $alm['kode_pos']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>No. Telp Rumah</td>
                                        <td>:</td>
                                        <td><?= $alm['no_telpon']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>No. Hp</td>
                                        <td>:</td>
                                        <td><?= $alm['no_hp']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->

                    </div>
                    <!-- /.card -->


                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Kepegawaian</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">

                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Program Studi</td>
                                        <td>:</td>
                                        <td><?= $kpg['prodi']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>NIP (Khusus PNS)</td>
                                        <td>:</td>
                                        <td><?= $kpg['nip_pns']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Status Kepegawaian</td>
                                        <td>:</td>
                                        <td><?= $kpg['status_pegawai']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Status Keaktifan</td>
                                        <td>:</td>
                                        <td><?= $kpg['status_keaktifan']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>No SK CPNS</td>
                                        <td>:</td>
                                        <td><?= $kpg['no_sk_pns']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>SK CPNS TMT</td>
                                        <td>:</td>
                                        <td><?= $kpg['tgl_sk_pns']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nomor SK TMMD</td>
                                        <td>:</td>
                                        <td><?= $kpg['no_sk_tmmd']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Mulai Menjadi Dosen (TMMD)</td>
                                        <td>:</td>
                                        <td><?= $kpg['tgl_sk_tmmd']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pangkat/Golongan</td>
                                        <td>:</td>
                                        <td><?= $kpg['pangkat']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Sumber Gaji</td>
                                        <td>:</td>
                                        <td><?= $kpg['sumber_gaji']; ?></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->



                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Lain-lain</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>NPWP</td>
                                        <td>:</td>
                                        <td><?= $lln['npwp']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Wajib Pajak</td>
                                        <td>:</td>
                                        <td><?= $lln['nama_wajib_pajak']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>SINTA ID</td>
                                        <td>:</td>
                                        <td><?= $lln['sinta_id']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Scoupus ID</td>
                                        <td>:</td>
                                        <td><?= $lln['scopus_id']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Google Scholar</td>
                                        <td>:</td>
                                        <td><?= $lln['gs']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col (LEFT) -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- The Modal -->