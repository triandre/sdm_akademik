<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Tracking</a></li>
                        <li class="breadcrumb-item active">Usulan Insentif Buku Ajar</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <div class="table-responsive">
                    <table class="table table-bordered" id="example1" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center" style="width:5% ;">No</th>
                                <th style="width:70% ;">Permohonan Buku Ajar</th>
                                <th class="text-center">Batch</th>
                                <th class="text-center" style="width:10% ;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($iba as $ba) : ?>
                                <tr>
                                    <td style="width:5%;" class="text-center" scope="row"><?= $i; ?></td>
                                    <td style="width:70%;"><small> Tanggal Permohonan, <?= date("d F Y", strtotime($ba['date_created'])) ?></small>
                                        <br>
                                        <?= $ba['jenis_buku']; ?>
                                        <br>
                                        <?= $ba['judul_buku']; ?> - <?= $ba['penerbit']; ?>
                                        <br>
                                        <small><i><?= $ba['tahun_terbit']; ?>, ISBN : <?= $ba['isbn']; ?> </i></small>
                                        <br>
                                        <small><i>Status : <?php
                                                            if ($ba['sts'] == 1) {
                                                                echo '<span class="badge badge-info">Proses LP2M</span> ';
                                                            } elseif ($ba['sts'] == 2) {
                                                                echo '<span class="badge badge-primary">Proses Wakil Rektor I</span>';
                                                            } elseif ($ba['sts'] == 3) {
                                                                echo '<span class="badge badge-warning">Proses Wakil Rektor II</span>';
                                                            } elseif ($ba['sts'] == 4) {
                                                                echo '<span class="badge badge-success">Di Terima</span>';
                                                            } elseif ($ba['sts'] == 6) {
                                                                echo '<span class="badge badge-danger">Berkas DiTolak</span>';
                                                            }  ?></i></small>
                                        <br>
                                        <small>
                                            <?php
                                            if ($akun['role_id'] == '3') { ?>
                                                <a href="<?= base_url('user/detail/'); ?><?= $ba['email']; ?>">Pemohon, <?= $ba['email']; ?></a>
                                            <?php } ?>
                                        </small>
                                        <br>
                                        <small> <?php
                                                if ($ba['sts'] == '6') { ?>
                                                Catatan : <?= $ba['ket']; ?>
                                            <?php } ?></small>
                                        <br>
                                    </td>
                                    <td style="width:5%;" class="text-center"><?= $ba['batch']; ?></td>

                                    <td style="width:5%;" class="text-center">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('viba/detail/'); ?><?= $ba['id']; ?>" class="dropdown-item">Detail</a>
                                                <a href="<?= base_url('viba/history/'); ?><?= $ba['email']; ?>" class="dropdown-item">History</a>


                                            </div>
                                        </div>



                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->

        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>