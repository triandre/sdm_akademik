<?php

$mpdf = new \Mpdf\Mpdf([
  'mode' => 'utf-8',
  'format' => [210, 330],
  'orientation' => 'L'
]);

$isi ='<!DOCTYPE html>
<html>
<head>
    <title>Report Insentif Buku Ajar </title>
</head>
<style>
table {
  font-family: arial, sans-serif;
  font-size: 10px;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<body>
<h4 align="center">Laporan Insentif Buku Ajar</h4>
<table>
<thead>
<tr>
<th>No</th>
<th>Nama Pengusul</th>
<th>Program Studi</th>
<th>Kontribusi Pengusul</th>
<th>Judul Buku</th>
<th>Penerbit</th>
<th>ISBN</th>
<th>Tahun Terbit</th>
<th>Hasil Pemeriksaan</th>
<th>Insentif</th>
</tr>
</thead>
<tbody>'; 

$i = 1; 
foreach ($gas as $gs) :

$isi .='<tr>
<td>' . $i . '</td>
<td>' . $gs["name"] . '</td>
<td>' . $gs["prodi"] . '</td>
<td>' . $gs["kontribusi_pengusul"] . '</td>
<td>' . $gs["judul_buku"] . '</td>
<td>' . $gs["penerbit"] . '</td>
<td>' . $gs["isbn"] . '</td>
<td>' . $gs["tahun_terbit"] . '</td>
<td>' . $gs["ket"] . '</td>
<td>Rp. ' . number_format($gs['insentif_disetujui']) . '</td>
</tr>';

$i++;
endforeach;
$isi .='
<tr>
<td colspan="9"><b>TOTAL KESELURUHAN</b></td>
<td><b>Rp. '.number_format($zat["total"]).'</b></td>
</tr>
</tbody>
</table> 
</body>
</html>';

$mpdf->WriteHTML($isi);
$mpdf->Output();
