<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('pimpinan1') ?>">Dashboard</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <?php
    $q_aji = $this->db->query("SELECT * FROM ins_aji where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_hki = $this->db->query("SELECT * FROM ins_hki where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_fip = $this->db->query("SELECT * FROM ins_ifip where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_bbp = $this->db->query("SELECT * FROM ins_bbp where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_iba = $this->db->query("SELECT * FROM ins_iba where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_pj = $this->db->query("SELECT * FROM ins_pj where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_kpfi = $this->db->query("SELECT * FROM ins_kpfi where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_bfio = $this->db->query("SELECT * FROM ins_bfio where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_ks = $this->db->query("SELECT * FROM ins_ks where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_ubpjs = $this->db->query("SELECT * FROM ins_ubpjs where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    $q_uipjs = $this->db->query("SELECT * FROM ins_uipjs where sts='2' AND aktif='1' AND batch BETWEEN 4 AND 10");
    ?>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_aji->num_rows(); ?></h3>

                            <p>Usulan Insentif <br> Artikel Dijurnal</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/vaji'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_hki->num_rows(); ?></h3>

                            <p>Usulan Insentif <br> HKI</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/vhki'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_fip->num_rows(); ?></h3>

                            <p>Usulan Insentif <br> Prosiding</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/vifip'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_iba->num_rows(); ?></h3>

                            <p>Usulan Insentif <br> Buku Ajar</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/viba'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_pj->num_rows(); ?></h3>

                            <p>Usulan Insentif <br> Pengelolah Jurnal</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/vpj'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_uipjs->num_rows(); ?></h3>

                            <p>Usulan Insentif <br> Publikasi Dari Skripsi/Thesis/Disertasi</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/vuipjs'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_ks->num_rows(); ?></h3>

                            <p>Usulan Insentif <br>Keynote Spreaker</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/vks'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_bfio->num_rows(); ?></h3>

                            <p>Usulan Bantuan <br> Mengikiuti Seminar</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/vbfio'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_bbp->num_rows(); ?></h3>

                            <p>Usulan Bantuan <br>Biaya Publikasi</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/vbbp'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $q_ubpjs->num_rows(); ?></h3>

                            <p>Usulan Bantuan <br> Publikasi Dari Skripsi/Thesis/Diserasi </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('pimpinan1/vubpjs'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>

        </div>
    </section>
    <!-- Main content -->

</div>
<!-- /.content-wrapper -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            },
            pageLength: 5,
        });
    });
</script>