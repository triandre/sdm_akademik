<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Pengumuman</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Pengumuman</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="card">
                            <img class="card-img-top" src="<?= base_url('info/1.jpg') ?>" alt="Card image" style="width:100%">
                            <div class="card-body">
                                <h4 class="card-title">Pengumuman</h4>
                                <p class="card-text">Insentif Artikel Di Jurnal Batch II Tahun 2022</p>
                                <a href="#" class="btn btn-primary"> <i class="fa fa-download"></i> Download</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">

                        <div class="card">
                            <img class="card-img-top" src="<?= base_url('info/2.jpg') ?>" alt="Card image" style="width:100%">
                            <div class="card-body">
                                <h4 class="card-title">Pengumuman</h4>
                                <p class="card-text">Insentif Hak Kekayaan Intelektual Batch II Tahun 2022</p>
                                <a href="#" class="btn btn-primary"> <i class="fa fa-download"></i> Download</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card">
                            <img class="card-img-top" src="<?= base_url('info/3.jpg') ?>" alt="Card image" style="width:100%">
                            <div class="card-body">
                                <h4 class="card-title">Pengumuman</h4>
                                <p class="card-text">Insentif Buku Ajar <br> Batch II Tahun 2022</p>
                                <a href="#" class="btn btn-primary"> <i class="fa fa-download"></i> Download</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card">
                            <img class="card-img-top" src="<?= base_url('info/4.jpg') ?>" alt="Card image" style="width:100%">
                            <div class="card-body">
                                <h4 class="card-title">Pengumuman</h4>
                                <p class="card-text">Insentif Keynote Speaker <br> Batch II Tahun 2022</p>
                                <a href="#" class="btn btn-primary"> <i class="fa fa-download"></i> Download</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card">
                            <img class="card-img-top" src="<?= base_url('info/5.jpg') ?>" alt="Card image" style="width:100%">
                            <div class="card-body">
                                <h4 class="card-title">Pengumuman</h4>
                                <p class="card-text">Insentif Proseding <br> Batch II Tahun 2022</p>
                                <a href="#" class="btn btn-primary"> <i class="fa fa-download"></i> Download</a>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-3">
                        <div class="card">
                            <img class="card-img-top" src="<?= base_url('info/6.jpg') ?>" alt="Card image" style="width:100%">
                            <div class="card-body">
                                <h4 class="card-title">Pengumuman</h4>
                                <p class="card-text">Insentif Artikel Di Jurnal dari Skripsi/thesis/Disertasi <br> Batch II Tahun 2022</p>
                                <a href="#" class="btn btn-primary"> <i class="fa fa-download"></i> Download</a>
                            </div>
                        </div>

                    </div>
                </div>





            </div>

            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->




</div>
<!-- /.content-wrapper -->