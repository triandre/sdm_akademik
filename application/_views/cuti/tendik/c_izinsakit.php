<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('tendik') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                        <li class="breadcrumb-item active">Izin Sakit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <?php
    $email = $this->session->userdata('email');
    $permohonan = $this->db->query("SELECT * FROM occ_permohonan_cuti where email='$email'");

    ?>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">


        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Permohonan Izin Sakit</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-12 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" method="post" action="<?= base_url('tendik/izinsakit/create'); ?>" enctype="multipart/form-data">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>

                                    <tr>
                                        <td>Email <span style="color:red;">*</span> </td>
                                        <td><input readonly class="form-control" type="email" id="email" name="email" value="<?php echo $this->session->userdata('email'); ?>" required readonly>
                                            <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?></td>
                                    </tr>


                                    <tr>
                                        <td>Lamanya Tidak Hadir <span style="color:red;">*</span> </td>
                                        <td> <select class="form-control show-tick" id="tidakHadir" name="tidakHadir" required>
                                                <option selected disabled value="">Pilih</option>
                                                <?php
                                                for ($i = 1; $i <= 30; $i++) {
                                                    echo "<option value='$i'>$i</option>";
                                                }
                                                ?>
                                            </select>
                                            <?= form_error('tidakHadir', '<small class="text-danger">', '</small>'); ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Dari Tanggal <span style="color:red;">*</span> </td>
                                        <td>
                                            <input type="date" class="form-control" id="dariTanggal" name="dariTanggal" required>
                                            <?= form_error('dariTanggal', '<small class="text-danger">', '</small>'); ?>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Sampai Tanggal<span style="color:red;">*</span> </td>
                                        <td><input type="date" class="form-control" id="sampaiTanggal" name="sampaiTanggal" value="<?= set_value('sampaiTanggal'); ?>" required>
                                            <?= form_error('sampaiTanggal', '<small class="text-danger pl-3">', '</small>'); ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Tanggal Masuk Kerja <span style="color:red;">*</span> </td>
                                        <td><input type="date" class="form-control" id="tglMasuk" name="tglMasuk" required>
                                            <?= form_error('tglMasuk', '<small class="text-danger">', '</small>'); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Berkas Pendukung <br>
                                            <small>(Izin Sakit di tanda tangani Pimpinan unit anda berada <br> Serta Lampirkan Surat Sakit dari Klinik/RS) Di jadikan 1 File PDF</small> <span style="color:red;">*</span>
                                        </td>
                                        <td><input type="file" class="form-control" id="scan" name="scan" accept="application/pdf" required>
                                            <?= form_error('scan', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Keterangan/Catatan<span style="color:red;">*</span> </td>
                                        <td><textarea class="form-control" type="text" id="ket" name="ket" rows="5" required></textarea> </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>