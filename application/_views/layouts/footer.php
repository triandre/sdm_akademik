<footer class="main-footer">
  <strong>Copyright &copy; <script>
      document.write(new Date().getFullYear());
    </script> <a href="#" target="_blank">IHRIS UMSU </a>.</strong>
  All rights reserved.
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> BETA &trade;
  </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap 4 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/vendor/backend/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>assets/vendor/backend/dist/js/demo.js"></script>
<!-- SweetAlert2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/js/notif.js"></script>
</body>

</html>