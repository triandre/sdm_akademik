<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title; ?> </title>
    <link rel="shortcut icon" type="image/icon" href="<?= base_url('assets/vendor/img/logo.png'); ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/dist/css/adminlte.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- jQuery -->
    <script src="<?= base_url() ?>assets/vendor/backend/plugins/jquery/jquery.min.js"></script>
</head>
<?php
$permohonanIzinSakitAdmin = $this->db->query("SELECT * FROM occ_izin_sakit where sts=1");
$permohonanCutiAdmin = $this->db->query("SELECT * FROM occ_permohonan_cuti where sts=1");
$permohonanCutiPimpinan = $this->db->query("SELECT * FROM occ_permohonan_cuti where sts=2");
// Notif Validasi PDD DATA PRIBADI
$kode1 = "ADPROFIL";
$kode2 = "ADKDD";
$kode3 = "ADKLG";
$kode4 = "ADKTAM";
$kode5 = "ADALAMAT";
$kode6 = "ADKPG";
$kode7 = "ADLLN";
// Notif Ajuan PDD UMUM
$umum = $this->db->query("SELECT * FROM occ_pdd WHERE sts=1");
// Notif Ajuan PDD data Probadi
$datapribadi = $this->db->query("SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan IN ('$kode1', '$kode2', '$kode3', '$kode4', '$kode5', '$kode6', '$kode7')");
// Notif Validasi PDD Pendidikan Formal
$kode004 = "ADPF";
$penfor = $this->db->query("SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode004'");
// Notif Validasi PDD Jafung
$kode002 = "ADJF";
$jafung = $this->db->query("SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode002'");
// Notif Validasi PDD Kepangkatan
$kode003 = "ADI";
$inpasing = $this->db->query("SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode003'");
// Notif Validasi PDD DATA sertifikasi
$kode005 = "ADSR";
$sertifikasi = $this->db->query("SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode005'");
?>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <?php if ($this->session->userdata('role_id') == '2') { ?>
                    <a class="nav-link" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge"></span>
                    </a>
                <?php } ?>
                <li class="nav-item dropdown user-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">

                        <?php if ($this->session->userdata('image') == NULL) : ?>
                            <img src="<?= base_url('assets/images/profil/default.jpg') ?>" class="user-image img-circle elevation-2">
                        <?php else : ?>
                            <img src="<?= base_url('assets/images/profil/') ?><?php echo $this->session->userdata('image'); ?>" class="user-image img-circle elevation-2">
                        <?php endif ?>

                        <span class="d-none d-md-inline">Hi, <?php echo $this->session->userdata('name'); ?></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <!-- User image -->
                        <li class="user-header bg-info">

                            <?php if ($this->session->userdata('image') == NULL) : ?>
                                <img src="<?= base_url('assets/images/profil/default.jpg') ?>" class="img-circle elevation-2">
                            <?php else : ?>
                                <img src="<?= base_url('assets/images/profil/') ?><?php echo $this->session->userdata('image'); ?>" class="img-circle elevation-2">
                            <?php endif ?>

                            <p>
                                <?php echo $this->session->userdata('name'); ?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <a href="<?= base_url('pengaturan') ?>" class="btn btn-info btn-flat">
                                <i class="nav-icon fa fa-cog"></i> Ubah Password
                            </a>
                            <a href="<?= base_url('logout') ?>" class="btn btn-danger btn-flat float-right">
                                <i class="fas fa-sign-out-alt"></i> Log out
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <img src="<?= base_url('assets/vendor/img/logo.png'); ?>" class="brand-image">
                <span class="brand-text"><b>IHRIS</b> UMSU</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu" data-accordion="false">


                        <!-- DOSEN -->
                        <?php if ($this->session->userdata('role_id') == '2') { ?>
                            <li class="nav-item has-treeview">
                                <a href="<?= base_url('dosen') ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>Dashboard</p>
                                </a>
                            </li>


                            <li class="nav-item has-treeview <?= isset($active_menu_profil) ? $active_menu_profil : '' ?>">
                                <a href="#" class="nav-link <?= isset($active_menu_prf) ? $active_menu_prf : '' ?>">
                                    <i class="nav-icon fa fa-user"></i>
                                    <p>Profil
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">

                                    <li class="nav-item">
                                        <a href="<?= base_url('dpribadi') ?>" class="nav-link <?= isset($active_menu_data_pribadi) ? $active_menu_data_pribadi : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Data Pribadi</p>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="<?= base_url('impassing') ?>" class="nav-link <?= isset($active_menu_data_impassing) ? $active_menu_data_impassing : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Inpasing/Kepangkatan</p>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="<?= base_url('jafung') ?>" class="nav-link <?= isset($active_menu_data_jafung) ? $active_menu_data_jafung : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Jabatan Fungsional</p>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="<?= base_url('penempatan') ?>" class="nav-link <?= isset($active_menu_data_penempatan) ? $active_menu_data_penempatan : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Penempatan</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="<?= base_url('pengumuman') ?>" class="nav-link <?= isset($active_menu_pengumuman) ? $active_menu_pengumuman : '' ?>">
                                    <i class="nav-icon fas fa-info"></i>
                                    <p style="color: red;"><b>Pengumuman</b></p>
                                </a>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="<?= base_url('kotakMasuk') ?>" class="nav-link <?= isset($active_menu_kotak_masuk) ? $active_menu_kotak_masuk : '' ?>">
                                    <i class="nav-icon fas fa-envelope"></i>
                                    <p>Kotak Masuk</p>
                                </a>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="<?= base_url('kotakMasuk') ?>" class="nav-link <?= isset($active_menu_kotak_masuk) ? $active_menu_kotak_masuk : '' ?>">
                                    <i class="nav-icon fas fa-book"></i>
                                    <p>PPH Internal</p>
                                </a>
                            </li>

                            <li class="nav-item has-treeview <?= isset($active_menu_insentif) ? $active_menu_insentif : '' ?>">
                                <a href="#" class="nav-link <?= isset($active_menu_ins) ? $active_menu_ins : '' ?>">
                                    <i class="nav-icon fa fa-file"></i>
                                    <p>Permohonan Insentif
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="<?= base_url('aji') ?>" class="nav-link <?= isset($active_menu_aji) ? $active_menu_aji : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UI. Publikasi Jurnal</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('hki') ?>" class="nav-link <?= isset($active_menu_hki) ? $active_menu_hki : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UI. HKI</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('pj') ?>" class="nav-link <?= isset($active_menu_pj) ? $active_menu_pj : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UI. Pengelolaan Jurnal</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('iba') ?>" class="nav-link <?= isset($active_menu_iba) ? $active_menu_iba : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UI. Buku Ajar</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('iks') ?>" class="nav-link <?= isset($active_menu_iks) ? $active_menu_iks : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UI. Keynote Speaker</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('ifip') ?>" class="nav-link <?= isset($active_menu_ifip) ? $active_menu_ifip : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UI. Prosiding</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('uipjs') ?>" class="nav-link <?= isset($active_menu_uipjs) ? $active_menu_uipjs : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UI. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('bbp') ?>" class="nav-link <?= isset($active_menu_bbp) ? $active_menu_bbp : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UB. Biaya Publikasi</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('bfio') ?>" class="nav-link <?= isset($active_menu_bfio) ? $active_menu_bfio : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UB. Seminar Inter/Nas</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('ubpjs') ?>" class="nav-link <?= isset($active_menu_ubpjs) ? $active_menu_ubpjs : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UB. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi</p>
                                        </a>
                                    <li class="nav-item">
                                        <a href="<?= base_url('bhki') ?>" class="nav-link <?= isset($active_menu_bhki) ? $active_menu_bhki : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>UB. HKI</p>
                                        </a>
                                    </li>
                            </li>
                    </ul>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_kualifikasi) ? $active_menu_kualifikasi : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_kl) ? $active_menu_kl : '' ?>">
                            <i class="nav-icon fa fa-graduation-cap"></i>
                            <p>Kualifikasi
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('pendidikan_formal') ?>" class="nav-link <?= isset($active_menu_penfor) ? $active_menu_penfor : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pendidikan Formal</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('diklat') ?>" class="nav-link <?= isset($active_menu_diklat) ? $active_menu_diklat : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Diklat</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('riwpek') ?>" class="nav-link <?= isset($active_menu_riwpek) ? $active_menu_riwpek : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Riwayat Pekerjaan</p>
                                </a>
                            </li>


                        </ul>
                    </li>


                    <li class="nav-item has-treeview <?= isset($active_menu_kompetensi) ? $active_menu_kompetensi : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_kp) ? $active_menu_kp : '' ?>">
                            <i class="nav-icon fa fa-cubes"></i>
                            <p>Kompetensi
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('sertifikasi') ?>" class="nav-link <?= isset($active_menu_sertifikasi) ? $active_menu_sertifikasi : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Sertifikasi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('tes') ?>" class="nav-link <?= isset($active_menu_tes) ? $active_menu_tes : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tes</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_pendidikan) ? $active_menu_pendidikan : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_pdd) ? $active_menu_pdd : '' ?>">
                            <i class="nav-icon fa fa-book"></i>
                            <p>Pelaks. Pendidikan
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('dosenPengajaran') ?>" class="nav-link <?= isset($active_menu_pengajaran) ? $active_menu_pengajaran : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pengajaran</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('dosenBm') ?>" class="nav-link <?= isset($active_menu_bm) ? $active_menu_bm : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Bimbingan Mahasiswa</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('dosenPm') ?>" class="nav-link <?= isset($active_menu_pm) ? $active_menu_pm : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pengujian Mahasiswa</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('dosenVs') ?>" class="nav-link <?= isset($active_menu_vs) ? $active_menu_vs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Visiting Scientist</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('dosenPm2') ?>" class="nav-link <?= isset($active_menu_pm2) ? $active_menu_pm2 : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pembinaan Mahasiswa</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('bahanAjar') ?>" class="nav-link <?= isset($active_menu_bahan) ? $active_menu_bahan : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Bahan Ajar</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('detasering') ?>" class="nav-link <?= isset($active_menu_deta) ? $active_menu_deta : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Detasering</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('oril') ?>" class="nav-link <?= isset($active_menu_oi) ? $active_menu_oi : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Orasi Ilmiah</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pemdos') ?>" class="nav-link <?= isset($active_menu_pemdos) ? $active_menu_pemdos : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pembimbing Dosen</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('tugtam') ?>" class="nav-link <?= isset($active_menu_tugtam) ? $active_menu_tugtam : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tugas Tambahan</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_ppen) ? $active_menu_ppen : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_ppn) ? $active_menu_ppn : '' ?>">
                            <i class="nav-icon fa fa-flask"></i>
                            <p>Pelaks. Penelitian
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('penelitian') ?>" class="nav-link <?= isset($active_menu_penelitian) ? $active_menu_penelitian : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penelitian</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link <?= isset($active_menu_pubkar) ? $active_menu_pubkar : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Publikasi Karya</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('phki') ?>" class="nav-link <?= isset($active_menu_paten) ? $active_menu_paten : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Paten/HKI</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_pengabdian) ? $active_menu_pengabdian : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_pgb) ? $active_menu_pgb : '' ?>">
                            <i class="nav-icon fa fa-link"></i>
                            <p>Pelaks. Pengabdian
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('pengabdian') ?>" class="nav-link <?= isset($active_menu_pengabdian) ? $active_menu_pengabdian : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pengabdian</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pengelola_jurnal'); ?>" class="nav-link <?= isset($active_menu_pengjur) ? $active_menu_pengjur : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pengelolah Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pembicara') ?>" class="nav-link <?= isset($active_menu_pembicara) ? $active_menu_pembicara : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pembicara</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="<?= base_url('jabatan') ?>" class="nav-link <?= isset($active_menu_jabatan) ? $active_menu_jabatan : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Jabatan Struktural</p>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="nav-item has-treeview <?= isset($active_menu_penunjang) ? $active_menu_penunjang : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_pjg) ? $active_menu_pjg : '' ?>">
                            <i class="nav-icon fa fa-podcast"></i>
                            <p>Penunjang
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('aprof') ?>" class="nav-link <?= isset($active_menu_ap) ? $active_menu_ap : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Anggota Profesi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('penghargaan') ?>" class="nav-link <?= isset($active_menu_phg) ? $active_menu_phg : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penghargaan</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('penlain') ?>" class="nav-link <?= isset($active_menu_pl) ? $active_menu_pl : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penunjang Lain</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_reward) ? $active_menu_reward : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_rw) ? $active_menu_rw : '' ?>">
                            <i class="nav-icon fa fa-trophy"></i>
                            <p>Reward
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('beasiswa') ?>" class="nav-link <?= isset($active_menu_beasiswa) ? $active_menu_beasiswa : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Beasiswa</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('kesejahteraan') ?>" class="nav-link <?= isset($active_menu_kesejahteraan) ? $active_menu_kesejahteraan : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Kesejahteraan</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('tunjangan') ?>" class="nav-link <?= isset($active_menu_tunjangan) ? $active_menu_tunjangan : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tunjangan</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('aik') ?>" class="nav-link <?= isset($active_menu_aik) ? $active_menu_aik : '' ?>">
                            <i class="nav-icon fas fa-mosque"></i>
                            <p>AIK</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('track') ?>" class="nav-link <?= isset($active_menu_track) ? $active_menu_track : '' ?>">
                            <i class="nav-icon fas fa-file"></i>
                            <p>Tracking Ajuan</p>
                        </a>
                    </li>


                    <!-- TENDIK -->
                <?php } elseif ($this->session->userdata('role_id') == '1') { ?>
                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('tendik') ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_profil) ? $active_menu_profil : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_prf) ? $active_menu_prf : '' ?>">
                            <i class="nav-icon fa fa-user"></i>
                            <p>Profil
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="<?= base_url('dpribadi') ?>" class="nav-link <?= isset($active_menu_data_pribadi) ? $active_menu_data_pribadi : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Pribadi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('penempatan') ?>" class="nav-link <?= isset($active_menu_data_penempatan) ? $active_menu_data_penempatan : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penempatan</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('tendik/cuti') ?>" class="nav-link <?= isset($active_menu_pcuti) ? $active_menu_pcuti : '' ?>">
                            <i class="nav-icon fas fa-file"></i>
                            <p>Permohonan Cuti</p>
                        </a>
                    </li>


                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('tendik/izinsakit') ?>" class="nav-link <?= isset($active_menu_pizinsakit) ? $active_menu_pizinsakit : '' ?>">
                            <i class="nav-icon fas fa-file"></i>
                            <p>Permohonan Izin Sakit</p>
                        </a>
                    </li>




                    <li class="nav-item has-treeview <?= isset($active_menu_kualifikasi) ? $active_menu_kualifikasi : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_kl) ? $active_menu_kl : '' ?>">
                            <i class="nav-icon fa fa-graduation-cap"></i>
                            <p>Kualifikasi
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('pendidikan_formal') ?>" class="nav-link <?= isset($active_menu_penfor) ? $active_menu_penfor : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pendidikan Formal</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="<?= base_url('riwpek') ?>" class="nav-link <?= isset($active_menu_riwpek) ? $active_menu_riwpek : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Riwayat Pekerjaan</p>
                                </a>
                            </li>


                        </ul>
                    </li>


                    <li class="nav-item has-treeview <?= isset($active_menu_kompetensi) ? $active_menu_kompetensi : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_kp) ? $active_menu_kp : '' ?>">
                            <i class="nav-icon fa fa-cubes"></i>
                            <p>Kompetensi
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('sertifikasi') ?>" class="nav-link <?= isset($active_menu_sertifikasi) ? $active_menu_sertifikasi : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Sertifikasi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('tes') ?>" class="nav-link <?= isset($active_menu_tes) ? $active_menu_tes : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tes</p>
                                </a>
                            </li>

                        </ul>
                    </li>





                    <li class="nav-item has-treeview <?= isset($active_menu_penunjang) ? $active_menu_penunjang : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_pjg) ? $active_menu_pjg : '' ?>">
                            <i class="nav-icon fa fa-podcast"></i>
                            <p>Penunjang
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="<?= base_url('penghargaan') ?>" class="nav-link <?= isset($active_menu_phg) ? $active_menu_phg : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penghargaan</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('penlain') ?>" class="nav-link <?= isset($active_menu_pl) ? $active_menu_pl : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penunjang Lain</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_reward) ? $active_menu_reward : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_rw) ? $active_menu_rw : '' ?>">
                            <i class="nav-icon fa fa-trophy"></i>
                            <p>Reward
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('beasiswa') ?>" class="nav-link <?= isset($active_menu_beasiswa) ? $active_menu_beasiswa : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Beasiswa</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('kesejahteraan') ?>" class="nav-link <?= isset($active_menu_kesejahteraan) ? $active_menu_kesejahteraan : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Kesejahteraan</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('tunjangan') ?>" class="nav-link <?= isset($active_menu_tunjangan) ? $active_menu_tunjangan : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tunjangan</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('aik') ?>" class="nav-link <?= isset($active_menu_aik) ? $active_menu_aik : '' ?>">
                            <i class="nav-icon fas fa-mosque"></i>
                            <p>AIK</p>
                        </a>
                    </li>

                    <!-- BIRO ADMINISTRASI UMUM -->
                <?php } elseif ($this->session->userdata('role_id') == '7') { ?>
                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('superpower') ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_ddt) ? $active_menu_ddt : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_dt) ? $active_menu_dt : '' ?>">
                            <i class="nav-icon fa fa-users"></i>
                            <p>Data Pegawai
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="<?= base_url('seluruhDosen') ?>" class="nav-link <?= isset($active_menu_data_sd) ? $active_menu_data_sd : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Dosen</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('seluruhTendik') ?>" class="nav-link <?= isset($active_menu_data_st) ? $active_menu_data_st : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Tendik</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('dosenFakultas') ?>" class="nav-link <?= isset($active_menu_data_ddpf) ? $active_menu_data_ddpf : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Dosen Per-Fakultas</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('dosenProdi') ?>" class="nav-link <?= isset($active_menu_data_ddpp) ? $active_menu_data_ddpp : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Dosen Per-Prodi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('penempatanAdmin') ?>" class="nav-link <?= isset($active_menu_data_penempatan) ? $active_menu_data_penempatan : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penempatan Pegawai</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('dosenProdi') ?>" class="nav-link <?= isset($active_menu_data_sgt) ? $active_menu_data_sgt : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Skala Gaji Tendik</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('dosenProdi') ?>" class="nav-link <?= isset($active_menu_data_sgd) ? $active_menu_data_sgd : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Skala Gaji Dosen</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="<?= base_url('masaPensiun') ?>" class="nav-link <?= isset($active_menu_data_pensiun) ? $active_menu_data_pensiun : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Masa Pensiun</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pencarian') ?>" class="nav-link <?= isset($active_menu_data_pencarian) ? $active_menu_data_pencarian : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pencarian Dosen & Tendik</p>
                                </a>
                            </li>
                        </ul>
                    </li>



                    <li class="nav-item has-treeview <?= isset($active_menu_vdd) ? $active_menu_vdd : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_vd) ? $active_menu_vd : '' ?>">
                            <i class="nav-icon fa fa-graduation-cap"></i>
                            <p>Validasi Data Dosen
                                <?php $A6 = $umum->num_rows();
                                if ($A6 > 0) { ?>
                                    <small>
                                        <i class="fa fa-info" style="color:red ;"></i>
                                    </small>
                                <?php } ?>
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('vdataPribadi') ?>" class="nav-link <?= isset($active_menu_vdp) ? $active_menu_vdp : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Pribadi
                                        <?php $A1 = $datapribadi->num_rows();
                                        if ($A1 > 0) { ?>
                                            <small>
                                                <i class="fa fa-info" style="color:red ;"></i>
                                            </small>
                                        <?php } ?>
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('vpenfor') ?>" class="nav-link <?= isset($active_menu_vpenvor) ? $active_menu_vpenvor : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pendidikan Formal
                                        <?php $A2 = $penfor->num_rows();
                                        if ($A2 > 0) { ?>
                                            <small>
                                                <i class="fa fa-info" style="color:red ;"></i>
                                            </small>
                                        <?php } ?>
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('vjafung') ?>" class="nav-link <?= isset($active_menu_v_jafung) ? $active_menu_v_jafung : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Jabatan Fungsional
                                        <?php $A3 = $jafung->num_rows();
                                        if ($A3 > 0) { ?>
                                            <small>
                                                <i class="fa fa-info" style="color:red ;"></i>
                                            </small>
                                        <?php } ?>
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('vKepangkatan') ?>" class="nav-link <?= isset($active_menu_vkepangkatan) ? $active_menu_vkepangkatan : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Kepangkatan
                                        <?php $A4 = $inpasing->num_rows();
                                        if ($A4 > 0) { ?>
                                            <small>
                                                <i class="fa fa-info" style="color:red ;"></i>
                                            </small>
                                        <?php } ?>
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('vSertifikasi') ?>" class="nav-link <?= isset($active_menu_vsertifikasi) ? $active_menu_vsertifikasi : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Sertifikasi
                                        <?php $A5 = $sertifikasi->num_rows();
                                        if ($A5 > 0) { ?>
                                            <small>
                                                <i class="fa fa-info" style="color:red ;"></i>
                                            </small>
                                        <?php } ?>
                                    </p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('admin/cuti') ?>" class="nav-link <?= isset($active_menu_cuti) ? $active_menu_cuti : '' ?>">
                            <i class="nav-icon fas fa-server"></i>
                            <p>Validasi Izin Cuti <Span class="badge badge-primary"><?php echo $permohonanCutiAdmin->num_rows(); ?></Span> </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('admin/izinsakit') ?>" class="nav-link <?= isset($active_menu_izinsakit) ? $active_menu_izinsakit : '' ?>">
                            <i class="nav-icon fas fa-server"></i>
                            <p>Validasi Izin Sakit <Span class="badge badge-primary"><?php echo $permohonanIzinSakitAdmin->num_rows(); ?></Span> </p>
                        </a>
                    </li>

                    <!-- <li class="nav-item has-treeview">
                <a href="<?= base_url('aik') ?>" class="nav-link <?= isset($active_menu_aik) ? $active_menu_aik : '' ?>">
                  <i class="nav-icon fas fa-server"></i>
                  <p>Validasi Rubrik BKD</p>
                </a>
              </li> -->

                    <li class="nav-item has-treeview <?= isset($active_menu_stamp) ? $active_menu_stamp : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_stp) ? $active_menu_stp : '' ?>">
                            <i class="nav-icon fa fa-fax"></i>
                            <p>Stamp Bantuan Publikasi
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="<?= base_url('stamp/v_pj') ?>" class="nav-link <?= isset($active_menu_tpj) ? $active_menu_tpj : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Pengelolaan Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('stamp/v_bbp') ?>" class="nav-link <?= isset($active_menu_tbbp) ? $active_menu_tbbp : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Biaya Publikasi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('stamp/v_bhki') ?>" class="nav-link <?= isset($active_menu_tbhki) ? $active_menu_tbhki : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. HKI</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('stamp/v_bfio') ?>" class="nav-link <?= isset($active_menu_tbfio) ? $active_menu_tbfio : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Seminar Inter/Nas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('stamp/v_ubpjs') ?>" class="nav-link <?= isset($active_menu_tubpjs) ? $active_menu_tubpjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                        </ul>
                    </li>



                    <li class="nav-item has-treeview <?= isset($active_menu_manajemen) ? $active_menu_manajemen : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_mm) ? $active_menu_mm : '' ?>">
                            <i class="nav-icon fa fa-server"></i>
                            <p>Manajemen Sistem
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('muser') ?>" class="nav-link <?= isset($active_menu_muser) ? $active_menu_muser : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Man. Pengguna</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('maik') ?>" class="nav-link <?= isset($active_menu_maik) ? $active_menu_maik : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Man. AIK</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('mfak') ?>" class="nav-link <?= isset($active_menu_mfak) ? $active_menu_mfak : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Man. Fakultas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('mprod') ?>" class="nav-link <?= isset($active_menu_mprod) ? $active_menu_mprod : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Man. Program Studi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('mpangk') ?>" class="nav-link <?= isset($active_menu_mpangk) ? $active_menu_mpangk : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Man. Pangkat</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('munit') ?>" class="nav-link <?= isset($active_menu_munit) ? $active_menu_munit : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Man. Unit Kerja</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- PIMPINAN 1 -->
                <?php } elseif ($this->session->userdata('role_id') == '4') { ?>
                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('pimpinan1') ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_validasiInsentif) ? $active_menu_validasiInsentif : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_vi) ? $active_menu_vi : '' ?>">
                            <i class="nav-icon fa fa-graduation-cap"></i>
                            <p>Validasi Publis
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/vaji') ?>" class="nav-link <?= isset($active_menu_vaji) ? $active_menu_vaji : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/vpj') ?>" class="nav-link <?= isset($active_menu_vpj) ? $active_menu_vpj : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Pengelolaan Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/vhki') ?>" class="nav-link <?= isset($active_menu_vhki) ? $active_menu_vhki : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. HKI</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/vks') ?>" class="nav-link <?= isset($active_menu_vks) ? $active_menu_vks : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Keynote Speaker</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/vifip') ?>" class="nav-link <?= isset($active_menu_vifip) ? $active_menu_vifip : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Prosiding</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/viba') ?>" class="nav-link <?= isset($active_menu_viba) ? $active_menu_viba : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Buku Ajar</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/vuipjs') ?>" class="nav-link <?= isset($active_menu_vuipjs) ? $active_menu_vuipjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/vbbp') ?>" class="nav-link <?= isset($active_menu_vbbp) ? $active_menu_vbbp : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Biaya Publikasi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/vbfio') ?>" class="nav-link <?= isset($active_menu_vbfio) ? $active_menu_vbfio : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Seminar Inter/Nas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan1/vubpjs') ?>" class="nav-link <?= isset($active_menu_vubpjs) ? $active_menu_vubpjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_track) ? $active_menu_track : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_tc) ? $active_menu_tc : '' ?>">
                            <i class="nav-icon fa fa-fax"></i>
                            <p>Tracking Publis
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('track/aji') ?>" class="nav-link <?= isset($active_menu_taji) ? $active_menu_taji : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/pj') ?>" class="nav-link <?= isset($active_menu_tpj) ? $active_menu_tpj : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Pengelolaan Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/hki') ?>" class="nav-link <?= isset($active_menu_thki) ? $active_menu_thki : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. HKI</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/ioks') ?>" class="nav-link <?= isset($active_menu_tks) ? $active_menu_tks : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Keynote Speaker</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/ifip') ?>" class="nav-link <?= isset($active_menu_tifip) ? $active_menu_tifip : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Prosiding</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/iba') ?>" class="nav-link <?= isset($active_menu_tiba) ? $active_menu_tiba : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Buku Ajar</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/uipjs') ?>" class="nav-link <?= isset($active_menu_tuipjs) ? $active_menu_tuipjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/bbp') ?>" class="nav-link <?= isset($active_menu_tbbp) ? $active_menu_tbbp : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Biaya Publikasi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/bfio') ?>" class="nav-link <?= isset($active_menu_tbfio) ? $active_menu_tbfio : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Seminar Inter/Nas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/ubpjs') ?>" class="nav-link <?= isset($active_menu_tubpjs) ? $active_menu_tubpjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- PIMPINAN 2 -->
                <?php } elseif ($this->session->userdata('role_id') == '5') { ?>
                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('pimpinan2') ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_validasiInsentif) ? $active_menu_validasiInsentif : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_vi) ? $active_menu_vi : '' ?>">
                            <i class="nav-icon fa fa-graduation-cap"></i>
                            <p>Validasi Publis
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/vaji') ?>" class="nav-link <?= isset($active_menu_vaji) ? $active_menu_vaji : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/vpj') ?>" class="nav-link <?= isset($active_menu_vpj) ? $active_menu_vpj : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Pengelolaan Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/vhki') ?>" class="nav-link <?= isset($active_menu_vhki) ? $active_menu_vhki : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. HKI</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/viks') ?>" class="nav-link <?= isset($active_menu_vks) ? $active_menu_vks : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Keynote Speaker</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/vifip') ?>" class="nav-link <?= isset($active_menu_vifip) ? $active_menu_vifip : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Prosiding</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/viba') ?>" class="nav-link <?= isset($active_menu_viba) ? $active_menu_viba : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Buku Ajar</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/vuipjs') ?>" class="nav-link <?= isset($active_menu_vuipjs) ? $active_menu_vuipjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/vbbp') ?>" class="nav-link <?= isset($active_menu_vbbp) ? $active_menu_vbbp : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Biaya Publikasi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/vbfio') ?>" class="nav-link <?= isset($active_menu_vbfio) ? $active_menu_vbfio : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Seminar Inter/Nas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/vubpjs') ?>" class="nav-link <?= isset($active_menu_vubpjs) ? $active_menu_vubpjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan2/vbhki') ?>" class="nav-link <?= isset($active_menu_vbhki) ? $active_menu_vbhki : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. HKI</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('pimpinan/cuti') ?>" class="nav-link <?= isset($active_menu_cuti) ? $active_menu_cuti : '' ?>">
                            <i class="nav-icon fas fa-server"></i>
                            <p>Permohonan Izin Cuti <Span class="badge badge-success"><?php echo $permohonanCutiPimpinan->num_rows(); ?></Span> </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_track) ? $active_menu_track : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_tc) ? $active_menu_tc : '' ?>">
                            <i class="nav-icon fa fa-fax"></i>
                            <p>Tracking Publis
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('track/aji') ?>" class="nav-link <?= isset($active_menu_taji) ? $active_menu_taji : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/pj') ?>" class="nav-link <?= isset($active_menu_tpj) ? $active_menu_tpj : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Pengelolaan Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/hki') ?>" class="nav-link <?= isset($active_menu_thki) ? $active_menu_thki : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. HKI</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/ioks') ?>" class="nav-link <?= isset($active_menu_tks) ? $active_menu_tks : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Keynote Speaker</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/ifip') ?>" class="nav-link <?= isset($active_menu_tifip) ? $active_menu_tifip : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Prosiding</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/iba') ?>" class="nav-link <?= isset($active_menu_tiba) ? $active_menu_tiba : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Buku Ajar</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/uipjs') ?>" class="nav-link <?= isset($active_menu_tuipjs) ? $active_menu_tuipjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/bbp') ?>" class="nav-link <?= isset($active_menu_tbbp) ? $active_menu_tbbp : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Biaya Publikasi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/bfio') ?>" class="nav-link <?= isset($active_menu_tbfio) ? $active_menu_tbfio : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Seminar Inter/Nas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/ubpjs') ?>" class="nav-link <?= isset($active_menu_tubpjs) ? $active_menu_tubpjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('reportpublis') ?>" class="nav-link <?= isset($active_menu_rpublis) ? $active_menu_rpublis : '' ?>">
                            <i class="nav-icon fas fa-print"></i>
                            <p>Report Publis</p>
                        </a>
                    </li>



                    <!-- LPPM  -->
                <?php } elseif ($this->session->userdata('role_id') == '3') { ?>
                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('lppm') ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_validasiInsentif) ? $active_menu_validasiInsentif : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_vi) ? $active_menu_vi : '' ?>">
                            <i class="nav-icon fa fa-graduation-cap"></i>
                            <p>Validasi Insentif
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('vaji') ?>" class="nav-link <?= isset($active_menu_vaji) ? $active_menu_vaji : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('vpj') ?>" class="nav-link <?= isset($active_menu_vpj) ? $active_menu_vpj : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Pengelolaan Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('vhki') ?>" class="nav-link <?= isset($active_menu_vhki) ? $active_menu_vhki : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. HKI</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('vks') ?>" class="nav-link <?= isset($active_menu_vks) ? $active_menu_vks : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Keynote Speaker</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('vifip') ?>" class="nav-link <?= isset($active_menu_vifip) ? $active_menu_vifip : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Prosiding</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('viba') ?>" class="nav-link <?= isset($active_menu_viba) ? $active_menu_viba : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Buku Ajar</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('vuipjs') ?>" class="nav-link <?= isset($active_menu_vuipjs) ? $active_menu_vuipjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('vbbp') ?>" class="nav-link <?= isset($active_menu_vbbp) ? $active_menu_vbbp : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Biaya Publikasi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('vbfio') ?>" class="nav-link <?= isset($active_menu_vbfio) ? $active_menu_vbfio : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Seminar Inter/Nas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('vubpjs') ?>" class="nav-link <?= isset($active_menu_vubpjs) ? $active_menu_vubpjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li class="nav-item has-treeview <?= isset($active_menu_track) ? $active_menu_track : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_tc) ? $active_menu_tc : '' ?>">
                            <i class="nav-icon fa fa-fax"></i>
                            <p>Tracking Publis
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('track/aji') ?>" class="nav-link <?= isset($active_menu_taji) ? $active_menu_taji : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/pj') ?>" class="nav-link <?= isset($active_menu_tpj) ? $active_menu_tpj : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Pengelolaan Jurnal</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/hki') ?>" class="nav-link <?= isset($active_menu_thki) ? $active_menu_thki : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. HKI</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/iks') ?>" class="nav-link <?= isset($active_menu_tks) ? $active_menu_tks : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Keynote Speaker</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/ifip') ?>" class="nav-link <?= isset($active_menu_tifip) ? $active_menu_tifip : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Prosiding</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/iba') ?>" class="nav-link <?= isset($active_menu_tiba) ? $active_menu_tiba : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Buku Ajar</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/uipjs') ?>" class="nav-link <?= isset($active_menu_tuipjs) ? $active_menu_tuipjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UI. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/bbp') ?>" class="nav-link <?= isset($active_menu_tbbp) ? $active_menu_tbbp : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Biaya Publikasi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('track/bfio') ?>" class="nav-link <?= isset($active_menu_tbfio) ? $active_menu_tbfio : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Seminar Inter/Nas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('track/ubpjs') ?>" class="nav-link <?= isset($active_menu_tubpjs) ? $active_menu_tubpjs : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>UB. Publikasi Jurnal <br> (Skripsi/Thesis/Disertasi)</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('reportpublis') ?>" class="nav-link <?= isset($active_menu_rpublis) ? $active_menu_rpublis : '' ?>">
                            <i class="nav-icon fas fa-print"></i>
                            <p>Report Publis</p>
                        </a>
                    </li>
                    <!-- OPERATOR  -->
                <?php } elseif ($this->session->userdata('role_id') == '8') { ?>
                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('operator') ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_presensi) ? $active_menu_presensi : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_prs) ? $active_menu_prs : '' ?>">
                            <i class="nav-icon fa fa-fax"></i>
                            <p>Presensi Dosen
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('presensi/createPresensi') ?>" class="nav-link <?= isset($active_menu_presensi_create) ? $active_menu_presensi_create : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Input Presensi</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('presensi/listPresensi') ?>" class="nav-link <?= isset($active_menu_presensi_listData) ? $active_menu_presensi_listData : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Presensi</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('presensi/lapPresensi') ?>" class="nav-link <?= isset($active_menu_lap_presensi) ? $active_menu_lap_presensi : '' ?>">
                            <i class="nav-icon fas fa-print"></i>
                            <p>Laporan Presensi</p>
                        </a>
                    </li>

                    <!-- ADMINKEU  -->
                <?php } elseif ($this->session->userdata('role_id') == '9') { ?>
                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('adminkeu') ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview <?= isset($active_menu_presensi) ? $active_menu_presensi : '' ?>">
                        <a href="#" class="nav-link <?= isset($active_menu_prs) ? $active_menu_prs : '' ?>">
                            <i class="nav-icon fa fa-fax"></i>
                            <p>Data Presensi
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('adminkeu/listPresensi') ?>" class="nav-link <?= isset($active_menu_presensi_tetap) ? $active_menu_presensi_tetap : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Dosen Tetap</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('adminkeu/listPresensi_dt') ?>" class="nav-link <?= isset($active_menu_presensi_tidaktetap) ? $active_menu_presensi_tidaktetap : '' ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Dosen Tidak Tetap</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('adminkeu/mtarif') ?>" class="nav-link <?= isset($active_menu_mtarif) ? $active_menu_mtarif : '' ?>">
                            <i class="nav-icon fas fa-money-check"></i>
                            <p> Manajemen Tarif</p>
                        </a>
                    </li>


                    <li class="nav-item has-treeview">
                        <a href="<?= base_url('adminkeu/lapPresensi') ?>" class="nav-link <?= isset($active_menu_lap_presensi) ? $active_menu_lap_presensi : '' ?>">
                            <i class="nav-icon fas fa-print"></i>
                            <p>Laporan Presensi</p>
                        </a>
                    </li>

                <?php } ?>
                <li class="nav-item has-treeview <?= isset($active_menu_pengaturan) ? $active_menu_pengaturan : '' ?>">
                    <a href="#" class="nav-link <?= isset($active_menu_pr) ? $active_menu_pr : '' ?>">
                        <i class="nav-icon fa fa-cog"></i>
                        <p>Pengaturan
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?= base_url('ubahPassword') ?>" class="nav-link <?= isset($active_menu_ubahpass) ? $active_menu_ubahpass : '' ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ganti Password</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="<?= base_url('logout') ?>" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Logout</p>
                    </a>
                </li>



                </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>