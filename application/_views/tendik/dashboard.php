<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Track Ajuan PDD</h3>
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Informasi!</strong> Ajuan yang ditolak, Silahkan Ajukan Ulang. Terimakasi !
                    </div>
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Jenis Ajuan</th>
                                <th>Ket</th>
                                <th>Tanggal Ajuan</th>
                                <th>Umur Ajuan (Hari)</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($pdd as $row) : ?>
                                <tr>
                                    <td style="width:4%"><?= $no++; ?></td>
                                    <td style="width:30%;color:blue ;"><?= $row['jenis_ajuan']; ?></td>
                                    <td style="width:40%;color:red ;"><?= $row['komentar']; ?></td>
                                    <td style="width:10%"><?= date("d M Y", strtotime($row['tgl_ajuan'])); ?></td>
                                    <td>
                                        <?php
                                        if ($row['tgl_verifikasi'] == null) {
                                            $tgl1 = date("Y-m-d", strtotime($row['tgl_ajuan']));
                                            $tanggal1 = strtotime($tgl1);
                                            $tgl2 = date("Y-m-d");
                                            $tanggal2 = strtotime($tgl2);
                                            $perbedaan = $tanggal2 - $tanggal1;
                                            $hari = $perbedaan / 60 / 60 / 24;
                                            echo "$hari Hari";
                                        } else {
                                            $tgl1 = date("Y-m-d", strtotime($row['tgl_ajuan']));
                                            $tanggal1 = strtotime($tgl1);
                                            $tgl2 = date("Y-m-d", strtotime($row['tgl_verifikasi']));
                                            $tanggal2 = strtotime($tgl2);
                                            $perbedaan = $tanggal2 - $tanggal1;
                                            $hari = $perbedaan / 60 / 60 / 24;
                                            echo "$hari Hari";
                                        }
                                        ?>
                                    </td>
                                    <td style="width:5%">
                                        <?php
                                        if ($row['sts'] == 1) {
                                            echo '<span class="badge badge-info">Diajukan</span> ';
                                        } elseif ($row['sts'] == 2) {
                                            echo '<span class="badge badge-success">Disetujui</span>';
                                        } elseif ($row['sts'] == 3) {
                                            echo '<span class="badge badge-danger">Ditolak</span>';
                                        }
                                        ?>
                                    </td>
                                    <td style="width:5%">
                                        <?php if ($row['kode_ajuan'] == "ADPROFIL") { ?>
                                            <a href="<?= base_url('ajuan_pdd/detailProfil/') ?><?= $row['reff'] ?>/<?php echo $this->session->userdata('nidn'); ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>
                                        <?php } elseif ($row['kode_ajuan'] == "ADKDD") { ?>
                                            <a href="<?= base_url('ajuan_pdd/detailKependudukan/') ?><?= $row['reff'] ?>/<?php echo $this->session->userdata('nidn'); ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>
                                        <?php } elseif ($row['kode_ajuan'] == "ADKLG") { ?>
                                            <a href="<?= base_url('ajuan_pdd/detailKeluarga/') ?><?= $row['reff'] ?>/<?php echo $this->session->userdata('nidn'); ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>
                                        <?php } elseif ($row['kode_ajuan'] == "ADKTAM") { ?>
                                            <a href="<?= base_url('ajuan_pdd/detailKeanggotaan/') ?><?= $row['reff'] ?>/<?php echo $this->session->userdata('nidn'); ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>
                                        <?php } elseif ($row['kode_ajuan'] == "ADALAMAT") { ?>
                                            <a href="<?= base_url('ajuan_pdd/detailAlamat/') ?><?= $row['reff'] ?>/<?php echo $this->session->userdata('nidn'); ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>
                                        <?php } elseif ($row['kode_ajuan'] == "ADKPG") { ?>
                                            <a href="<?= base_url('ajuan_pdd/detailKepegawaian/') ?><?= $row['reff'] ?>/<?php echo $this->session->userdata('nidn'); ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>
                                        <?php } elseif ($row['kode_ajuan'] == "ADLLN") { ?>
                                            <a href="<?= base_url('ajuan_pdd/detailLainlain/') ?><?= $row['reff'] ?>/<?php echo $this->session->userdata('nidn'); ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Ringkasan Profil</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                                <img class="mx-auto d-block" src="<?= base_url('assets/images/profil/') ?><?php echo $this->session->userdata('image'); ?>" style="width: 200px;">
                                <br>
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>NIDN/NIDK/NITK</td>
                                        <td>:</td>
                                        <td><?= $dp['nidn']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>NPP</td>
                                        <td>:</td>
                                        <td><?= $ss['npp']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>NKTAM</td>
                                        <td>:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Name</td>
                                        <td>:</td>
                                        <td><?= $ss['name']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        <td><?= $dp['jk']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tempat Lahir</td>
                                        <td>:</td>
                                        <td><?= $dp['t_lahir']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td><?= $dp['tgl_lahir']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Ibu Kandung</td>
                                        <td>:</td>
                                        <td><?= $dp['nama_ibu']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="<?= base_url('dpribadi'); ?>">
                                <button type="button" class="btn btn-success">Lihat Detail</button>
                            </a>
                        </div>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Kesejahteraan</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div style="overflow-x:auto;">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Jenis Kesejahteraan</th>
                                            <th>Layanan Kesejahteraan</th>
                                            <th>Penyelenggara</th>
                                            <th>Tahun Mulai</th>
                                            <th>Tahun Selesai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($ksj as $row) : ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td><?= $row['jenis_kesejahteraan']; ?> </td>
                                                <td><?= $row['layanan']; ?></td>
                                                <td><?= $row['penyelenggara']; ?></td>
                                                <td><?= $row['tahun_mulai']; ?></td>
                                                <td><?= $row['tahun_selesai']; ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="<?= base_url('kesejahteraan'); ?>">
                                <button type="button" class="btn btn-success">Lihat Detail</button>
                            </a>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
                <!-- /.col (LEFT) -->


                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Pendidikan Terakhir</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div style="overflow-x:auto;">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>Jenjang Pendidikan</td>
                                            <td>:</td>
                                            <td><?= $pt['jenjang']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Bidang Studi</td>
                                            <td>:</td>
                                            <td><?= $pt['bidang_studi']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Perguruan Tinggi</td>
                                            <td>:</td>
                                            <td><?= $pt['perguruan_tinggi']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Lulus</td>
                                            <td>:</td>
                                            <td><?= $pt['tahun_lulus']; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="<?= base_url('pendidikan_formal'); ?>">
                                <button type="button" class="btn btn-success">Lihat Detail</button>
                            </a>
                        </div>
                    </div>
                    <!-- /.card -->

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tunjangan</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div style="overflow-x:auto;">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Jenis Tunjangan</th>
                                            <th>Nama Tunjangan</th>
                                            <th>Instansi Pemberi</th>
                                            <th>Sumber Dana</th>
                                            <th>Tahun Mulai</th>
                                            <th>Tahun Selesai</th>
                                            <th>nominal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($tunjangan as $row) : ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td><?= $row['jenis_tunjangan']; ?></td>
                                                <td><?= $row['nama_tunjangan']; ?></td>
                                                <td><?= $row['instansi']; ?></td>
                                                <td><?= $row['sumber_dana']; ?></td>
                                                <td><?= $row['tahun_mulai']; ?></td>
                                                <td><?= $row['tahun_selesai']; ?></td>
                                                <td><?= number_format($row['nominal']); ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="<?= base_url('tunjangan'); ?>">
                                <button type="button" class="btn btn-success">Lihat Detail</button>
                            </a>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->





                </div>


            </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            },
            pageLength: 5,
        });
    });
</script>