<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">Usulan Insentif Hki/Paten</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Permohonan Bantuan HKI</th>
                                <th>File Permohonan</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($v as $hk) : ?>
                                <tr>
                                    <th scope="row"><?= $i; ?></th>
                                    <td>
                                        <small>Tanggal Permohonan, <?= date("d F Y", strtotime($hk['date_created'])) ?></small>
                                        <br>
                                        <?= $hk['judul_hki']; ?>
                                        <br> <small>
                                            Nomor Registrasi: <?= $hk['no_reg']; ?> <br>
                                            Jenis HKI: <?= $hk['jenis_hki']; ?> <br>
                                            Pemegang HKI: <?= $hk['pemegang_hki']; ?> <br>
                                            Kategori HKI: <?= $hk['kategori_hki']; ?>
                                            <br></small>
                                        <small><i>Status : <?php
                                                            if ($hk['sts'] == 1) {
                                                                echo '<span class="badge badge-info">Proses LP2M</span> ';
                                                            } elseif ($hk['sts'] == 2) {
                                                                echo '<span class="badge badge-primary">Proses Wakil Rektor I</span>';
                                                            } elseif ($hk['sts'] == 3) {
                                                                echo '<span class="badge badge-warning">Proses Wakil Rektor II</span>';
                                                            } elseif ($hk['sts'] == 4) {
                                                                echo '<span class="badge badge-success">Di Terima</span>';
                                                            } elseif ($hk['sts'] == 6) {
                                                                echo '<span class="badge badge-danger">Berkas DiTolak</span>';
                                                            }
                                                            ?>
                                            </i></small>
                                        <br>
                                        <?php
                                        if ($akun['role_id'] >= '3') { ?>
                                            <small> <a href="<?= base_url('user/detail/'); ?><?= $hk['email']; ?>">Pemohon : <?= $hk['email']; ?></a></small> <br>
                                            Pagu Bantuan: <?= number_format($hk['pagu']); ?>
                                        <?php } ?>
                                        <br>
                                        <small style="color:red">
                                    </td>
                                    <td><a href="<?= base_url('file/') ?><?= $hk['upload_file']; ?>" target="_blank"><i class="fa fa-download"></i> Download</td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('bhki/detail/'); ?><?= $hk['id']; ?>" class="dropdown-item">Lihat Detail</a>
                                                <a href="<?= base_url('stamp/stamp_bhki/'); ?><?= $hk['id']; ?>" class="dropdown-item">Stempel</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>