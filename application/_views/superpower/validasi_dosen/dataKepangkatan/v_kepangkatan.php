<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">Beranda</li>
                        <li class="breadcrumb-item">Ajuan Pdd</li>
                        <li class="breadcrumb-item">Data Inpasing</li>
                        <li class="breadcrumb-item active">Validasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <section class="content">

        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    Daftar Ajuan Data Inpasing
                </h3>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">DIAJUKAN</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-settings-tab" data-toggle="pill" href="#custom-content-below-setujui" role="tab" aria-controls="custom-content-below-settings" aria-selected="false">DISETUJUI</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-messages-tab" data-toggle="pill" href="#custom-content-below-messages" role="tab" aria-controls="custom-content-below-messages" aria-selected="false">DITOLAK</a>
                    </li>

                </ul>
                <div class="tab-content" id="custom-content-below-tabContent">

                    <!-- DIAJUKAN -->
                    <div class="tab-pane fade  show active" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">

                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Nama</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Perubahan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($diajukan as $row2) : ?>
                                            <td><?= $row2['name']; ?></td>
                                            <td><?= date("d M Y", strtotime($row2['tgl_ajuan'])); ?></td>
                                            <td style="color:blue;"><?= $row2['jenis_ajuan']; ?></td>
                                            <td> <a href="<?= base_url('ajuan_pdd/detailKepangkatan/') ?><?= $row2['reff'] ?>/<?= $row2['nidn'] ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>

                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="custom-content-below-setujui" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Nama</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Disetujui</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Perubahan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($disetujui as $row3) : ?>
                                            <td><?= $row3['name']; ?></td>
                                            <td><?= date("d M Y", strtotime($row3['tgl_ajuan'])); ?></td>
                                            <td><?= date("d M Y", strtotime($row3['tgl_verifikasi'])); ?></td>
                                            <td><?= $row3['jenis_ajuan']; ?></td>
                                            <td><span class="badge badge-success">Disetujui</span></td>
                                            <td>
                                                <a href="<?= base_url('ajuan_pdd/detailKepangkatan/') ?><?= $row3['reff'] ?>/<?= $row3['nidn'] ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>

                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="custom-content-below-messages" role="tabpanel" aria-labelledby="custom-content-below-messages-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Nama</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Ditolak</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Perubahan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Penolakan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($ditolak as $row4) : ?>
                                            <td><?= $row4['name']; ?></td>
                                            <td><?= date("d M Y", strtotime($row4['tgl_ajuan'])); ?></td>
                                            <td><?= date("d M Y", strtotime($row4['tgl_verifikasi'])); ?></td>
                                            <td><?= $row4['jenis_ajuan']; ?></td>
                                            <td><?= $row4['komentar']; ?></td>
                                            <td><span class="badge badge-danger">Ditolak</span></td>
                                            <td>
                                                <a href="<?= base_url('ajuan_pdd/detailKepangkatan/') ?><?= $row4['reff'] ?>/<?= $row4['nidn'] ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>



                </div>
            </div>




        </div>
        <!-- /.card -->
</div>
</section>
<!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>